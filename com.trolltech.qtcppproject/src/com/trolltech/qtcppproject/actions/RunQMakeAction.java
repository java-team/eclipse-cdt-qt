/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.ui.actions.WorkspaceAction;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.ui.PlatformUI;
import org.eclipse.core.runtime.IProgressMonitor;
import java.lang.reflect.InvocationTargetException;
import java.lang.InterruptedException;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IProject;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IEditorInput;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import com.trolltech.qtcppproject.qmake.QMakeRunner;
import com.trolltech.qtcppproject.utils.QtUtils;

// derive from WorkspaceAction to have access to the selection from ProjectExplorer tree
public class RunQMakeAction extends WorkspaceAction implements IWorkbenchWindowActionDelegate {

	private Set<String> projectsForQMake; // stores paths to pro files for which we already have run qmake from single run() action
	
	public RunQMakeAction() {
		super(PlatformUI.getWorkbench().getActiveWorkbenchWindow(), new String("Run qmake"));
	
        projectsForQMake = new HashSet<String>();
	}

	@Override
	protected String getOperationMessage() {
		return new String("Running qmake");
	}

	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	public void init(IWorkbenchWindow window) {
		// TODO Auto-generated method stub
		
	}

	// called from every resource from the current selection in Project Explorer view
	protected void invokeOperation(IResource resource,
            IProgressMonitor monitor) {
		IProject project = resource.getProject();
		if (project == null) {
			return; // the given resource is not in any project
		}
		
		if (!QtUtils.isQtProject(project))
			return;
		
		String projectPath = project.getLocation().toString();
		if (projectsForQMake.contains(projectPath))
			return; // qmake was already run for project for different resource
		projectsForQMake.add(projectPath);
		QMakeRunner.runQMake(project, monitor);
	}
	
	// Returns null if the focus is outside of any editor.
	// Otherwise returns project of the active editor in case
	// it is a qt project or null in other cases.
	private IProject qtProjectOfActiveEditor() {
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		IEditorPart epart = page.getActiveEditor();
		IWorkbenchPart wpart = page.getActivePart();
		if (epart != null && epart.equals(wpart)) { // the focus is inside editor, at least one editor is opened
			IEditorInput input = epart.getEditorInput();
			return QtUtils.qtProjectOfEditorInput(input);
		}
	    return null;
	}

	private boolean isQtProjectSelected() {
		List<IResource> list = getActionResources();
		for (IResource resource : list) {
			IProject project = resource.getProject();
			if (QtUtils.isQtProject(project))
				return true; // at least one qt project is selected, don't iterate more.
		}
		return false;
	}
	
	public void run(IAction action) {
		final IProject project = qtProjectOfActiveEditor();
		if (project != null) { 
			IRunnableWithProgress op = new IRunnableWithProgress() {
		        	
		   	    // We run qmake with runnable. In this way we can pass progress monitor to runQMake()
				public void run(IProgressMonitor monitor) {
    			    QMakeRunner.runQMake(project, monitor);
				    monitor.done();
				}
			};
			try {
				PlatformUI.getWorkbench().getActiveWorkbenchWindow().run(true, false, op); // we run it here
			} catch (InterruptedException e) {
				return;
			} catch (InvocationTargetException e) {
				Throwable realException = e.getTargetException();
				MessageDialog.openError(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Error", realException.getMessage());
				return;
			}
				
		} else {
			projectsForQMake.clear();
			// run on selection, in consequence series of calls to invokeOperation() will be triggered by the eclipse
			run();
		}
	}
	
	public void selectionChanged(IAction action, ISelection selection) {
		// this method is also called when current editor changes

		// Delegate selection changed notification to the real "this" action.
		// In this way "this" action can reconstruct the resource selection.
		if (selection instanceof IStructuredSelection) 
		    selectionChanged((IStructuredSelection)selection);
		
		// Enable or disable the action.
		// Trying to mimic eclipse build project's enabling behaviour.
		// If the focus is inside the editor and the edited resource comes from qt project
		// the action will run qmake on that project.
		// Otherwise qmake will be run on all selected qt projects inside
		// project explorer. In case a project is not selected but a resource
		// from it is selected this method will threat the project as selected.
		boolean enabled = qtProjectOfActiveEditor() != null || isQtProjectSelected();
		setEnabled(enabled); // update the real action
		action.setEnabled(enabled); // update the delegate action
	}
	
}
