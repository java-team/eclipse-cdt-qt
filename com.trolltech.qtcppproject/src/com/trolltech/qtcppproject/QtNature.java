/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject;

import java.util.Vector;

import org.eclipse.core.resources.ICommand;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.runtime.CoreException;

public class QtNature implements IProjectNature {
	private IProject project;
	public static final String QT_NATURE_ID = "com.trolltech.qtcppproject.QtNature";
	
	public void configure() throws CoreException {
		// "Associate an incremental project builder with a project", taken from eclipse doc: "Incremental project builders"
		IProjectDescription desc = project.getDescription();
		ICommand[] commands = desc.getBuildSpec();
		
		Vector builders = new Vector();
		
		ICommand qmake = desc.newCommand();
		qmake.setBuilderName(QtProConstants.QTBUILDER_ID);
		
		builders.add(qmake);
		
		for (int i=0; i<commands.length; ++i) {
			builders.add(commands[i]);						
		}
		
		desc.setBuildSpec((ICommand[])builders.toArray(new ICommand[builders.size()]));		
		project.setDescription(desc, null);
	}

	public void deconfigure() throws CoreException {
		IProjectDescription desc = getProject().getDescription();
		ICommand[] commands = desc.getBuildSpec();
		
		Vector builders = new Vector();
		for (int i = 0; i < commands.length; ++i) {
			if (!commands[i].getBuilderName().equals(QtProConstants.QTBUILDER_ID))
				builders.add(commands[i]);
		}
		
		desc.setBuildSpec((ICommand[])builders.toArray(new ICommand[builders.size()]));
		project.setDescription(desc, null);		
	}

	public IProject getProject() {		
		return project;
	}

	public void setProject(IProject project)
	{
		this.project = project;
	}
}

