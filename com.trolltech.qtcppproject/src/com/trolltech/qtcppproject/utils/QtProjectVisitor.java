/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject.utils;

import java.util.Vector;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceProxy;
import org.eclipse.core.resources.IResourceProxyVisitor;
import org.eclipse.core.runtime.CoreException;

/*
 * Util class, returns the list of filenames which match the extension ext inside the project
 */

public class QtProjectVisitor implements IResourceProxyVisitor {
	private Vector result;
	private String extention;
	
	public QtProjectVisitor() {
		result = new Vector();
	}

	public boolean visit(IResourceProxy proxy) throws CoreException {
		if (proxy.getName().endsWith(extention) && proxy.getType() == IResource.FILE) {
			result.add(proxy.requestResource());
			return false;
		}
		return true;
	}
	
	public Vector findFiles(IProject project, String ext) {
		extention = "." + ext;
		try {
			result.clear();
			project.accept(this, IResource.NONE);
			return result;
		} catch (Exception e) {
			return null;
		}
	}
}