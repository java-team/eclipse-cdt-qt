/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject.preferences;

import java.util.Iterator;
import java.util.Vector;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.resources.WorkspaceJob;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.PlatformUI;

import com.trolltech.qtcppproject.QtProject;
import com.trolltech.qtcppproject.QtProjectPlugin;
import com.trolltech.qtcppproject.utils.QtUtils;
import com.trolltech.qtcppproject.wizards.QtVersionWizard;

class QtVersionListener extends SelectionAdapter
{
	public final static int ADD = 1;
	public final static int EDIT = 2;
	public final static int REMOVE = 3;
	public final static int DEFAULT = 4;
	public final static int SELECTION = 5;
	
	private QtPreferencePage m_prefpage;
	private int m_control;
	
	public QtVersionListener(QtPreferencePage prefpage, int control)
	{
		m_control = control;
		m_prefpage = prefpage;
	}
	
	public void widgetSelected(SelectionEvent e)
	{
		if (m_control == ADD) {
			QtVersionWizard versionWizard = new QtVersionWizard();
			WizardDialog versionDialog = new WizardDialog(m_prefpage.getShell(), versionWizard);
			versionDialog.create();

			versionDialog.setTitle("Add new Qt version");
			if (versionDialog.open() == WizardDialog.OK) {
				m_prefpage.addItem(versionWizard.getVersionName(), 
						versionWizard.getBinPath(),
						versionWizard.getIncludePath());
			}
		} else if (m_control == EDIT) {
			String[] current = m_prefpage.getCurrentItem();

			QtVersionWizard versionWizard = new QtVersionWizard();
			WizardDialog versionDialog = new WizardDialog(m_prefpage.getShell(), versionWizard);
			versionDialog.create();

			versionWizard.setVersionName(current[0]);
			versionWizard.setBinPath(current[1]);
			versionWizard.setIncludePath(current[2]);

			versionDialog.setTitle("Edit Qt Version");
			if (versionDialog.open() == WizardDialog.OK) {
				m_prefpage.updateItem(versionWizard.getVersionName(), 
						versionWizard.getBinPath(),
						versionWizard.getIncludePath());
			}
		} else if (m_control == REMOVE) {
			m_prefpage.removeItem();
		} else if (m_control == DEFAULT) {
			m_prefpage.setCurrentDefault();
		} else if (m_control == SELECTION) {
			Table table = (Table)e.widget;
			m_prefpage.enableButtons(table.getSelectionCount() > 0);
		}
	}
}

public class QtPreferencePage extends PreferencePage
	implements IWorkbenchPreferencePage {
	
	private Label label;
	private Table table;
	private Button removeButton;
	private Button editButton;
	private Button defaultButton;
	private Button autosetmkspec;
	private Button autosetmkcmd;
	
	public QtPreferencePage() {
		
	}
	
	public static boolean getAutoSetMkSpec() {
		IPreferenceStore store = QtProjectPlugin.getDefault().getPreferenceStore();
		
		if (store.contains(PreferenceConstants.QT_AUTOSETMKSPEC))
			return store.getBoolean(PreferenceConstants.QT_AUTOSETMKSPEC);
		else
			return true;
	}

	public static boolean getAutoSetMkCmd() {
		IPreferenceStore store = QtProjectPlugin.getDefault().getPreferenceStore();
		
		if (store.contains(PreferenceConstants.QT_AUTOSETMKCMD))
			return store.getBoolean(PreferenceConstants.QT_AUTOSETMKCMD);
		else
			return true;
	}
	
	public static String[] getQtVersions() {
		Vector versions = new Vector();

		IPreferenceStore store = QtProjectPlugin.getDefault().getPreferenceStore();
		if (!store.contains(PreferenceConstants.QTVERSION_COUNT))
			return null;
		
		int count = store.getInt(PreferenceConstants.QTVERSION_COUNT);
		for (int i=0; i<count; ++i) {
			String name = PreferenceConstants.QTVERSION_NAME + "." + Integer.toString(i);
			if (store.contains(name))
				versions.add(store.getString(name));
		}
		
		return (String[])versions.toArray(new String[versions.size()]);
		
	}
	
	private static String[] getQtPaths(String version) {
		IPreferenceStore store = QtProjectPlugin.getDefault().getPreferenceStore();
		if (!store.contains(PreferenceConstants.QTVERSION_COUNT))
			return null;
		
		String defaultVersionName = store.getString(PreferenceConstants.QTVERSION_DEFAULT);
		String defaultVersionBinPath = "";
		String defaultVersionIncludePath = "";
		int count = store.getInt(PreferenceConstants.QTVERSION_COUNT);
		for (int i=0; i<count; ++i) {
			String nameKey = PreferenceConstants.QTVERSION_NAME + "." + Integer.toString(i);
			String binpathKey = PreferenceConstants.QTVERSION_BINPATH + "." + Integer.toString(i);
			String includepathKey = PreferenceConstants.QTVERSION_INCLUDEPATH+ "." + Integer.toString(i);
			String name = "";
			String binpath = "";
			String includepath = "";
			
			if (store.contains(nameKey))
				name = store.getString(nameKey);
			if (store.contains(binpathKey))
				binpath = store.getString(binpathKey);
			if (store.contains(includepathKey))
				includepath = store.getString(includepathKey);
			
			if (name.equals(version))
				return new String[] {name, binpath, includepath};
			if (name.equals(defaultVersionName)) {
				defaultVersionBinPath = binpath;
				defaultVersionIncludePath = includepath;
			}
		}
		
		return new String[] {defaultVersionName, defaultVersionBinPath, defaultVersionIncludePath};
	}

	public static String getQtVersionBinPath(String version) {
		String[] paths = getQtPaths(version);
		if (paths != null)
			return getQtPaths(version)[1];
		return null;
	}

	public static String getQtVersionIncludePath(String version) {
		String[] paths = getQtPaths(version);
		if (paths != null)
			return getQtPaths(version)[2];
		return null;
	}

	public String[] getCurrentItem() {
		TableItem[] items = table.getSelection();
		if (items.length == 0)
			return null;
		return new String[] {items[0].getText(0), items[0].getText(1), items[0].getText(2)};
	}
	
	public void enableButtons(boolean enabled) {
		removeButton.setEnabled(enabled);
		defaultButton.setEnabled(enabled);
		editButton.setEnabled(enabled);
	}
	
	public void updateItem(String name, String binPath, String includePath) {
		TableItem[] items = table.getSelection();
		if (items.length == 0)
			return;
		items[0].setText(new String[] {name, binPath, includePath});
	}
	
	public void addItem(String name, String binPath, String includePath) {
		TableItem item = new TableItem(table, SWT.NONE);
		item.setText(new String[] {name, binPath, includePath});
	}
	
	public void removeItem() {
		if (table.getSelectionCount() > 0) {
			int removeindex = table.getSelectionIndex(); 
			int defindex = getDefaultIndex();
			table.remove(removeindex);
			
			if (removeindex == defindex)
				setDefaultIndex(0);
		}
		
		enableButtons(table.getSelectionCount() > 0);
	}
	
	public boolean performOk()
	{
		IPreferenceStore store = QtProjectPlugin.getDefault().getPreferenceStore();
		
		// Fetch all project's Qt paths
		IProject[] pros = QtUtils.getQtProjects();
		QtProject[] qtProjects = new QtProject[pros.length];
		String[] oldBinPaths = new String[pros.length];
		String[] oldIncludePaths = new String[pros.length];
		for (int i=0; i<pros.length; ++i) {
			qtProjects[i] = new QtProject(pros[i]);
			oldBinPaths[i] = qtProjects[i].getQtBinPath();
			oldIncludePaths[i] = qtProjects[i].getQtIncludePath();
		}
		String defaultVersion = getDefaultQtVersionName();
		if (defaultVersion != null)
			store.setValue(PreferenceConstants.QTVERSION_DEFAULT, defaultVersion);
		
		store.setValue(PreferenceConstants.QTVERSION_COUNT, table.getItemCount());
		for (int i=0; i<table.getItemCount(); ++i) {
			store.setValue(PreferenceConstants.QTVERSION_NAME + "." + Integer.toString(i),
					table.getItem(i).getText(0));
			store.setValue(PreferenceConstants.QTVERSION_BINPATH + "." + Integer.toString(i),
					table.getItem(i).getText(1));
			store.setValue(PreferenceConstants.QTVERSION_INCLUDEPATH+ "." + Integer.toString(i),
					table.getItem(i).getText(2));
		}
		
		store.setValue(PreferenceConstants.QT_AUTOSETMKSPEC, autosetmkspec.getSelection());
		store.setValue(PreferenceConstants.QT_AUTOSETMKCMD, autosetmkspec.getSelection());
		
		// updates all the Qt projects and collect projects that need rebuild
		Vector outdated = new Vector();
		for (int i=0; i < qtProjects.length; ++i) {
			qtProjects[i].updateQtDir(oldBinPaths[i], oldIncludePaths[i]);
			if ((qtProjects[i].getQtBinPath() == null && oldBinPaths[i] != null)
				|| (qtProjects[i].getQtBinPath() != null && !qtProjects[i].getQtBinPath().equals(oldBinPaths[i]))
				|| (qtProjects[i].getQtIncludePath() == null && oldIncludePaths[i] != null)
				|| (qtProjects[i].getQtIncludePath() != null && !qtProjects[i].getQtIncludePath().equals(oldIncludePaths[i]))) {
				outdated.add(qtProjects[i]);
			}
		}
		
		if (!outdated.isEmpty())
			askForRebuild(outdated);
		
		return true;
	}
	
	private void askForRebuild(final Vector projects) {
		Shell shell = PlatformUI.getWorkbench().getDisplay().getActiveShell();
		MessageDialog dialog = new MessageDialog(shell, "Qt Versions Changed", null,
				"Some projects' Qt versions have changed. A rebuild of the projects is required for changes to take effect. Do a full rebuild now?", 
				MessageDialog.QUESTION, 
				new String[] { IDialogConstants.YES_LABEL, IDialogConstants.NO_LABEL}, 0);
		if (dialog.open() == 0) {
			WorkspaceJob rebuild = new WorkspaceJob("Rebuild projects") {
	    		public boolean belongsTo(Object family) {
	    			return ResourcesPlugin.FAMILY_MANUAL_BUILD.equals(family);
	    		}
				public IStatus runInWorkspace(IProgressMonitor monitor) {
					Iterator i = projects.iterator();
					while (i.hasNext()) {
						QtProject project = (QtProject)i.next();
						project.scheduleRebuild();
					}
					return Status.OK_STATUS;
				}
			};
			rebuild.setRule(ResourcesPlugin.getWorkspace().getRuleFactory()
	                .buildRule());
			rebuild.setUser(true);
			rebuild.schedule();
		}
	}
	
	private void setDefaultIndex(int index) {
		for (int i=0; i<table.getItemCount(); ++i) {
			TableItem item = table.getItem(i); 
			Font fnt = item.getFont();
			FontData fntdata = fnt.getFontData()[0];
			
			if (i == index) {
				int style = fntdata.getStyle() | SWT.BOLD;
				fntdata.setStyle(style);
				item.setFont(new Font(fnt.getDevice(), fntdata));				
			} else if ((fntdata.getStyle() & SWT.BOLD) != 0) {
				int style = fntdata.getStyle() & ~SWT.BOLD;
				fntdata.setStyle(style);
				item.setFont(new Font(fnt.getDevice(), fntdata));
			}
		}
	}
	
	private int getDefaultIndex() {
		for (int i=0; i<table.getItemCount(); ++i) {
			TableItem item = table.getItem(i); 
			Font fnt = item.getFont();
			FontData fntdata = fnt.getFontData()[0];
			
			if ((fntdata.getStyle() & SWT.BOLD) != 0)
				return i; 
		}
		
		if (table.getItemCount() > 0)
			return 0;
		
		return -1;
	}
	
	private String getDefaultQtVersionName() {
		int index = getDefaultIndex();
		if (index == -1)
			return null;

		return table.getItem(index).getText(0);
	}
	
	public void setCurrentDefault() {
		setDefaultIndex(table.getSelectionIndex());
	}
	
	private void updateItems()
	{
		IPreferenceStore store = QtProjectPlugin.getDefault().getPreferenceStore();
		
		autosetmkspec.setSelection(getAutoSetMkSpec());
		autosetmkcmd.setSelection(getAutoSetMkCmd());
		
		if (!store.contains(PreferenceConstants.QTVERSION_COUNT))
			return;
		
		int defaultVersionIndex = 0;
		String defaultVersionName = store.getString(PreferenceConstants.QTVERSION_DEFAULT);
		
		int count = store.getInt(PreferenceConstants.QTVERSION_COUNT);
		for (int i=0; i<count; ++i) {
			String nameKey = PreferenceConstants.QTVERSION_NAME + "." + Integer.toString(i);
			String binpathKey = PreferenceConstants.QTVERSION_BINPATH + "." + Integer.toString(i);
			String includepathKey = PreferenceConstants.QTVERSION_INCLUDEPATH+ "." + Integer.toString(i);
			String name = "";
			String binpath = "";
			String includepath = "";
			
			if (store.contains(nameKey))
				name = store.getString(nameKey);
			if (store.contains(binpathKey))
				binpath = store.getString(binpathKey);
			if (store.contains(includepathKey))
				includepath = store.getString(includepathKey);
			addItem(name, binpath, includepath);
			
			if (name.equals(defaultVersionName))
				defaultVersionIndex = i;
		}	
		setDefaultIndex(defaultVersionIndex);
	}
	
	private void addQtBuildsSection(Composite parent) {
		GridData gridData;
	
		if (Platform.getOS().equals(Platform.OS_WIN32)) {
			Link vsWarning = new Link(parent, SWT.NONE);
			vsWarning.setText("Please note:\n" +
					"Qt versions built with MinGW offer the full functionality of the Eclipse integration.\n" +
					"Visual Studio builds bring limitations with them. " +
					"<A HREF=\"/com.trolltech.qtcppintegrationhelp/doc/eclipse-integration-managing-projects.html#basic-qt-version-management\">Read about these limitations</A>.");
			SelectionListener vsWarningSelectionListener = new SelectionListener() {
				public void widgetSelected(SelectionEvent e) {
					PlatformUI.getWorkbench().getHelpSystem().displayHelpResource(e.text);
				}
				public void widgetDefaultSelected(SelectionEvent e) {}
			};
			vsWarning.addSelectionListener(vsWarningSelectionListener);
			gridData = new GridData();
			gridData.horizontalSpan = 2;
			vsWarning.setLayoutData(gridData);
		}
		
		label = new Label(parent, SWT.CENTER);
		label.setText("Qt Versions:");
		gridData = new GridData();
		gridData.horizontalAlignment = GridData.HORIZONTAL_ALIGN_BEGINNING;
		gridData.horizontalSpan = 2;
		label.setLayoutData(gridData);

		table = new Table(parent, SWT.BORDER | SWT.SINGLE | SWT.FULL_SELECTION);
		table.setHeaderVisible(true);
		TableColumn column;
		column = new TableColumn(table, SWT.LEFT);
		column.setText("Name");
		column.setWidth(100);
		column.setResizable(true);
		column = new TableColumn(table, SWT.LEFT);
		column.setText("Bin Path");
		column.setResizable(true);
		column.setWidth(100);
		column = new TableColumn(table, SWT.LEFT);
		column.setText("Include Path");
		column.setResizable(true);
		column.setWidth(100);
		
		gridData = new GridData();
		gridData.verticalSpan = 5;
		gridData.widthHint = 250;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;
		gridData.verticalAlignment = GridData.FILL;
		
		table.setLayoutData(gridData);
		table.addSelectionListener(new QtVersionListener(this, QtVersionListener.SELECTION));
		
		Button addButton = new Button(parent, SWT.NONE);
		addButton.addSelectionListener(new QtVersionListener(this, QtVersionListener.ADD)); 
		addButton.setText("Add...");
		gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		addButton.setLayoutData(gridData);
		
		editButton = new Button(parent, SWT.NONE);
		editButton.addSelectionListener(new QtVersionListener(this, QtVersionListener.EDIT));
		editButton.setText("Edit...");
		gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		editButton.setLayoutData(gridData);
		
		removeButton = new Button(parent, SWT.NONE);
		removeButton.addSelectionListener(new QtVersionListener(this, QtVersionListener.REMOVE));
		removeButton.setText("Remove");
		gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		removeButton.setLayoutData(gridData);
		
		defaultButton = new Button(parent, SWT.NONE);
		defaultButton.addSelectionListener(new QtVersionListener(this, QtVersionListener.DEFAULT));
		defaultButton.setText("Default");
		gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		defaultButton.setLayoutData(gridData);
		
		Composite spacer = new Composite(parent, SWT.NONE);
		gridData = new GridData();
		gridData.grabExcessVerticalSpace = true;
		gridData.verticalAlignment = GridData.FILL;
		gridData.horizontalAlignment = GridData.FILL;
		spacer.setLayoutData(gridData);
		
		autosetmkspec = new Button(parent, SWT.CHECK);
		autosetmkspec.setText("Auto update QMAKESPEC when applying changes.");
		gridData = new GridData();
		gridData.horizontalSpan = 2;
		autosetmkspec.setLayoutData(gridData);
		
		autosetmkcmd = new Button(parent, SWT.CHECK);
		autosetmkcmd.setText("Auto update make command when applying changes.");
		gridData = new GridData();
		gridData.horizontalSpan = 2;
		autosetmkcmd.setLayoutData(gridData);
	}

	protected Control createContents(Composite parent) {
		
		Composite composite = new Composite(parent, SWT.NULL);
		
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		composite.setLayout(layout);
		
		addQtBuildsSection(composite);
		updateItems();
		enableButtons(table.getSelectionCount() > 0);
		
		return composite;
	}
	
	public void init(IWorkbench workbench) {
				
	}
}