/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject.preferences;

/**
 * Constant definitions for plug-in preferences
 */
public class PreferenceConstants {
	public final static String QT_AUTOSETMKSPEC = "com.trolltech.qtcppproject.qtautosetmkspec";
	public final static String QT_AUTOSETMKCMD = "com.trolltech.qtcppproject.qtautosetmkcmd";
	public final static String QTVERSION_COUNT = "com.trolltech.qtcppproject.qtversioncount";
	public final static String QTVERSION_NAME = "com.trolltech.qtcppproject.qtversionname";
	public final static String QTVERSION_DEFAULT = "com.trolltech.qtcppproject.qtversiondefault";
	public final static String QTVERSION_BINPATH = "com.trolltech.qtcppproject.qtversionbinpath";
	public final static String QTVERSION_INCLUDEPATH = "com.trolltech.qtcppproject.qtversionincludepath";
	
}
