/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.WorkspaceJob;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import com.trolltech.qtcppproject.qmake.QMakeRunner;

public class ProFileChangedListener implements IResourceChangeListener {

	public void resourceChanged(IResourceChangeEvent event) {
		// ignore project closed/deleted, pre/post build events
		if (event.getType() == IResourceChangeEvent.POST_CHANGE) {
			// resource changed events always start at the workspace root, so projects
			// are the next level down
			IResourceDelta[] projects = event.getDelta().getAffectedChildren();
			if (projects.length > 0) {
				for (IResourceDelta projectDelta : projects) {
					if (((projectDelta.getFlags() & IResourceDelta.OPEN) != 0) || projectDelta.getKind() == IResourceDelta.REMOVED) {
						// the project(s) was either just created, opened, renamed, closed or deleted.  in any
						// case we can ignore the event.
						continue;
					}
					
					// ignore marker deltas
					if ((projectDelta.getFlags() & IResourceDelta.MARKERS) != 0) {
						continue;
					}
					
					// ignore description deltas
					if ((projectDelta.getFlags() & IResourceDelta.DESCRIPTION) != 0) {
						continue;
					}

					IResource resource = projectDelta.getResource();
					if (resource != null && resource instanceof IProject) {
						// if the project is not a Qt project then ignore the change
						final IProject project = (IProject)resource;
						try {

							if (project.isAccessible() && project.hasNature(QtNature.QT_NATURE_ID)) {
								
								// check the project option
								if (new QtProject(project).runQMakeWhenProFileChanges()) {
									// visit the children
									boolean runQmake = visitChildren(projectDelta);
									if (runQmake) {
										WorkspaceJob runQmakJob = new WorkspaceJob("Invoking qmake") {

											@Override
											public IStatus runInWorkspace(IProgressMonitor monitor)
													throws CoreException {
												String errStr = QMakeRunner.runQMake(project, monitor);
												if (errStr != null) {
													return new Status(IStatus.ERROR, QtProjectPlugin.PLUGIN_ID, errStr);
												}
												return Status.OK_STATUS;
											}
											
										};
										runQmakJob.setUser(true);
										runQmakJob.setRule(project);
										runQmakJob.schedule();
									}
								}
							}
						} catch (CoreException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	private boolean visitChildren(IResourceDelta delta) {

		IResourceDelta[] changedChildren = delta.getAffectedChildren(IResourceDelta.CHANGED);
		if (changedChildren.length > 0) {
			for (IResourceDelta child : changedChildren) {
				// ignore marker deltas
				if ((child.getFlags() & IResourceDelta.MARKERS) != 0) {
					continue;
				}

				IResource resource = child.getResource();
				if (resource != null) {
					if (resource instanceof IFile) {
						String name = resource.getName().toLowerCase();
						//TODO ideally we would get the list of files included from
						// the main .pro file and only listen for changes to those files
						if (name.endsWith(".pro") || name.endsWith(".pri")) {
							return true;
						}
						
					} else if (resource instanceof IFolder) {
						visitChildren(child);
					}
				}
			}
		}
		
		return false;
	}
}
