/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject.editors;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.forms.editor.FormEditor;

import com.trolltech.qtcppcommon.editors.EditorInputWatcher;
import com.trolltech.qtcppcommon.editors.IQtEditor;
import com.trolltech.qtcppproject.pages.QrcTreePage;

public class QrcEditor extends FormEditor implements IQtEditor {
	private QrcTreePage treePage = null;
	private EditorInputWatcher listener;
	
	public QrcEditor() {
		super();
	}

	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		super.init(site, input);
		listener = new EditorInputWatcher(this);
	}

	public void reload() {
		treePage.reload();
	}

	public void doSave(IProgressMonitor monitor)
	{
		treePage.doSave(monitor);
		listener.updateTimeStamp();
	}
	
	public void doSaveAs()
	{
	
	}
	
	public boolean isSaveAsAllowed()
	{
		return false;
	}
	
	protected void addPages()
	{
		setPartName(getEditorInput().getName());
		try {
			treePage = new QrcTreePage(this);
			addPage(treePage);
		} catch(Exception e) {
			//### show warning
		}
	}
	
	public boolean isDirty() {
		return treePage.isDirty();
	}
	
	protected void pageChange(int newPageIndex) {
		super.pageChange(newPageIndex);
		if (newPageIndex == 0)
			treePage.setFocus();
	}

	public void dispose() {
		listener.dispose();
		super.dispose();
	}
	
	public void setFocus() {
		super.setFocus();
		if (getCurrentPage() == 0)
			treePage.setFocus();
	}
}
