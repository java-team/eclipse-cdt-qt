/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject.editors;

import org.eclipse.swt.graphics.RGB;

public interface IProFileColorConstants {
	RGB COMMENT = new RGB(0, 128, 0);
	RGB DEFAULT = new RGB(0, 0, 0);
	RGB FUNCTION = new RGB(128, 128, 0);
	RGB VARIABLE = new RGB(128, 0, 128);

}
