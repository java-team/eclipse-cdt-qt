/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject.editors;

import org.eclipse.jface.text.rules.*;
import org.eclipse.jface.text.*;

public class ProFileScanner extends RuleBasedScanner {
	private String [] variables = {
		    "CONFIG",
		    "DEFINES",
		    "DEF_FILE",
		    "DEPENDPATH",
		    "DESTDIR",
		    "DESTDIR_TARGET",
		    "DISTFILES",
		    "DLLDESTDIR",
		    "FORMS",
		    "HEADERS",
		    "INCLUDEPATH",
		    "LEXSOURCES",
		    "LIBS",
		    "MAKEFILE",
		    "MOC_DIR",
		    "OBJECTS",
		    "OBJECTS_DIR",
		    "OBJMOC",
		    "PKGCONFIG",
		    "POST_TARGETDEPS",
		    "PRECOMPILED_HEADER",
		    "PRE_TARGETDEPS",
		    "QMAKE",
		    "QMAKESPEC",
		    "QT",
		    "RCC_DIR",
		    "RC_FILE",
		    "REQUIRES",
		    "RESOURCES",
		    "RES_FILE",
		    "SOURCES",
		    "SRCMOC",
		    "SUBDIRS",
		    "TARGET",
		    "TARGET_EXT",
		    "TARGET_x",
		    "TARGET_x.y.z",
		    "TEMPLATE",
		    "TRANSLATIONS",
		    "UI_DIR",
		    "UI_HEADERS_DIR",
		    "UI_SOURCES_DIR",
		    "VER_MAJ",
		    "VER_MIN",
		    "VER_PAT",
		    "VERSION",
		    "VPATH",
		    "YACCSOURCES"
	};
	private String [] functions = { 
		    "basename",
		    "CONFIG",
		    "contains",
		    "count",
		    "dirname",
		    "error",
		    "exists",
		    "find",
		    "for",
		    "include",
		    "infile",
		    "isEmpty",
		    "join",
		    "member",
		    "message",
		    "prompt",
		    "quote",
		    "sprintf",
		    "system",
		    "unique",
		    "warning"
		    };
	public ProFileScanner(ColorManager manager) {
		IToken commentToken =
			new Token(
				new TextAttribute(
					manager.getColor(IProFileColorConstants.COMMENT)));
		IToken functionToken =
			new Token(
				new TextAttribute(
					manager.getColor(IProFileColorConstants.FUNCTION)));
		IToken variableToken =
			new Token(
				new TextAttribute(
					manager.getColor(IProFileColorConstants.VARIABLE)));
		IToken defaultToken =
			new Token(
				new TextAttribute(
					manager.getColor(IProFileColorConstants.DEFAULT)));

		IRule[] rules = new IRule[3];
		rules[0] = new EndOfLineRule("#", commentToken);
		IWordDetector wd = new IWordDetector() {
			public boolean isWordStart(char c) {
		  	    return Character.isLetter(c) || c == '_' || c == '.';	
			}
			public boolean isWordPart(char c) {
				return isWordStart(c); 
			}
			
		};
		// Token is UNDEFINED to allow pass to the next rule (index 2 on rules array)
		//in case of a recognised word which was not on the internal list. 
		FunctionRule fr = new FunctionRule(wd, Token.UNDEFINED); 
		for (String s : functions) {
			fr.addWord(s, functionToken);
		}
		
		// Since this is the last rule, we don't want run all rules again with scanner
		//which contains 1 char less so we return default token for the whole recognised word.
		WordRule wr = new WordRule(wd, defaultToken);
		for (String s : functions) {
			wr.addWord(s, functionToken);
		}
		for (String s : variables) {
			wr.addWord(s, variableToken);
		}
		rules[1] = fr;
		rules[2] = wr;

		setRules(rules);
	}

}

