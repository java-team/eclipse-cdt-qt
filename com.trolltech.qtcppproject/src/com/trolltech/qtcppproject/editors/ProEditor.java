/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject.editors;

import com.trolltech.qtcppproject.editors.ColorManager;
import com.trolltech.qtcppproject.editors.ProFileViewerConfiguration;

import org.eclipse.ui.editors.text.TextEditor;

public class ProEditor extends TextEditor {
	public static String ID = "com.trolltech.qtcppproject.editors.ProEditor";

	private ColorManager colorManager;

	public ProEditor() {
		super();
		colorManager = new ColorManager();
		setSourceViewerConfiguration(new ProFileViewerConfiguration(colorManager));
	}
	public void dispose() {
		colorManager.dispose();
		super.dispose();
	}
	

}
