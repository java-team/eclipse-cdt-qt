/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject.editors;

import org.eclipse.jface.text.rules.ICharacterScanner;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WordRule;
import org.eclipse.jface.text.rules.IWordDetector;

// Basically the same as WordRule, but in addition it checks whether
// there is a character '(' after matched word

public class FunctionRule extends WordRule {
	
	public FunctionRule(IWordDetector detector, IToken defaultToken) {
		super(detector, defaultToken);
	}
	
	public IToken evaluate(ICharacterScanner scanner) {
		IToken token = super.evaluate(scanner);
		if (!token.isUndefined()) {
    	    int c = scanner.read();
		    scanner.unread(); // we don't want highlight the last read character, we only check what it is 
	  	    if (c == '(') { // we only return matching token if the next character is '('
			    return token;
		    }
		    unreadBuffer(scanner); // from WordRule - we need to unread the scanner and return UNDEFINED token to enable scanning for other rules
		}
		return Token.UNDEFINED;
	}

}
