/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.IDocument;
import org.eclipse.ui.editors.text.TextEditor;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.osgi.framework.BundleContext;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IPartService;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IWindowListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IEditorReference;

import com.trolltech.qtcppproject.preferences.PreferenceConstants;
import com.trolltech.qtcppproject.qmake.IQMakeEnvironmentModifier;
import com.trolltech.qtcppproject.utils.QtUtils;

/**
 * The main plugin class to be used in the desktop.
 */

public class QtProjectPlugin extends AbstractUIPlugin {
	//The shared instance.
	private static QtProjectPlugin plugin;
	private ProFileChangedListener proFileListener = new ProFileChangedListener();
	private Map<String, Boolean> filePathToQObjectMacro;
	private Map<String, String> filePathToProjectPath;
	private Map<String, Boolean> projectPathToRunQMake;
	
	private static List<IQMakeEnvironmentModifier> envModifiers = null;

	public static final String TEMPLATE_LOCATION = "/com/trolltech/qtcppproject/wizards/templates/"; //$NON-NLS-1$
	
	public static final String PLUGIN_ID = "com.trolltech.qtcppproject"; //$NON-NLS-1$

	/**
	 * The constructor.
	 */
	public QtProjectPlugin() {
		super();
		plugin = this;
		filePathToQObjectMacro = new HashMap<String, Boolean>();
		filePathToProjectPath = new HashMap<String, String>();
		projectPathToRunQMake = new HashMap<String, Boolean>();
	}
	
	// Returns true if any source file inside a project got or lost Q_OBJECT macro.
	// For every opened file we keep the info whether it contains Q_OBJECT macro or not.
	// We track the changes for opened editors. Whenever the existence of Q_OBJECT
	// macro changes we record that info for the project it belongs to.
	// We check that info inside project builder (QtMakefileGenerator) and run qmake accordingly.
	// When we run qmake we clear the info for projects.
	public boolean isRunningQMakeRequest(String projectPath) {
		Boolean run = projectPathToRunQMake.get(projectPath);
		if (run != null && run.booleanValue() == true)
			return true;
		return false;
	}
	
	public void clearRunningQMakeRequest(String projectPath) {
		projectPathToRunQMake.remove(projectPath);
	}
	
	private boolean isEndingQuote(String contents, int quoteIndex)
	{
	    boolean endingQuote = true;
	    if (quoteIndex > 0) {
	        int previous = 1;
	        while (contents.charAt(quoteIndex - previous) == '\\') {
	            previous++;
	            endingQuote = !endingQuote;
	        }
	    }
	    return endingQuote;
	}

	private boolean hasQObjectMacro(String contents) {
		int idx = 0;
		int count = contents.length();
		while (idx < count && idx >= 0) {
			int macroIdx = contents.indexOf("Q_OBJECT", idx);
			if (macroIdx == -1) {
				return false;
			} else if (macroIdx == idx) {
				return true;
			} else if (contents.indexOf("//", idx) == idx) {
				idx = contents.indexOf('\n', idx + 2) + 1; // drop everything up to the end of line
			} else if (contents.indexOf("/*", idx) == idx) {
				idx = contents.indexOf("*/", idx + 2) + 2; // drop everything up to the nearest */
			} else if (contents.indexOf("'\\\"'", idx) == idx) {
				idx += 4; // drop it
			} else if (contents.charAt(idx) == '\"') {
	            do {
	                idx = contents.indexOf('\"', idx + 1); // drop everything up to the nearest "
	            } while (idx > 0 && !isEndingQuote(contents, idx)); // if the nearest " is preceded by \ (or by \\\ or by \\\\\, but not by \\ nor \\\\) we find next one
	            if (idx < 0)
	                return false; // no closing comment up to the end of file
	            idx++;
			} else {
				idx++;
			}
		}
		return false;
	}

	public void start(BundleContext context) throws Exception {
		super.start(context);
		ResourcesPlugin.getWorkspace().addResourceChangeListener(proFileListener);
		ResourcesPlugin.getWorkspace().addResourceChangeListener(new IResourceChangeListener() {
			private List<IFile> changedFiles = new ArrayList<IFile>();
			
			public void resourceChanged(IResourceChangeEvent event) {
				if (event.getType() != IResourceChangeEvent.POST_CHANGE)
					return;
				changedFiles.clear();
				visitChildren(event.getDelta());
				for (IFile changedFile : changedFiles) {
					String changedFilePath = changedFile.getLocation().toOSString();
					if (filePathToProjectPath.containsKey(changedFilePath)) {
						boolean hadMacro = filePathToQObjectMacro.get(changedFilePath).booleanValue();
						boolean hasMacro = hasQObjectMacro(getContents(new FileEditorInput(changedFile)));
						if (hadMacro != hasMacro) { // this change triggers running qmake
							filePathToQObjectMacro.put(changedFilePath, hasMacro);
							projectPathToRunQMake.put(filePathToProjectPath.get(changedFilePath), true);
						}
					}
				}
				changedFiles.clear();
			}
			
			private String getContents(IFileEditorInput input) {
				IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				IEditorPart part = page.findEditor(input);
	            if (part instanceof TextEditor) {
	               	TextEditor te = (TextEditor)part;
	               	IDocumentProvider provider = te.getDocumentProvider();
	               	IDocument document = provider.getDocument(te.getEditorInput());
	               	String contents = document.get();
	              	return contents;
	            }		
				return null;
			}
			
			private void visitChildren(IResourceDelta delta) {
				IResourceDelta[] changedChildren = delta.getAffectedChildren(IResourceDelta.CHANGED);
				if (changedChildren.length > 0) {
					for (IResourceDelta child : changedChildren) {
						// ignore marker deltas
						if ((child.getFlags() & IResourceDelta.MARKERS) != 0) {
							continue;
						}

						IResource resource = child.getResource();
						if (resource != null) {
							if (resource instanceof IFile) {
								changedFiles.add((IFile)resource);
							} else if (resource instanceof IFolder) {
								visitChildren(child);
							} else if (resource instanceof IProject) {
								visitChildren(child);
							}
						}
					}
				}
			}
			
			
		});
		
		IWorkbench workbench = PlatformUI.getWorkbench();

		workbench.addWindowListener(new IWindowListener() {

			public void windowActivated(IWorkbenchWindow window) {
				// TODO Auto-generated method stub
				
			}

			public void windowClosed(IWorkbenchWindow window) {
				// TODO Auto-generated method stub
				
			}

			public void windowDeactivated(IWorkbenchWindow window) {
				// TODO Auto-generated method stub
				
			}

			public void windowOpened(IWorkbenchWindow window) {
				// TODO Auto-generated method stub
				addPartListener(window);
		    } } );
		
		IWorkbenchWindow[] windows = workbench.getWorkbenchWindows();
		for (IWorkbenchWindow window : windows)
			addPartListener(window);
	}
	
	public void addPartListener(IWorkbenchWindow window) {
        IPartService service = window.getPartService();
        service.addPartListener(new IPartListener() {

			public void partActivated(IWorkbenchPart part) {
				// TODO Auto-generated method stub
			}
			
			private String getContents(IWorkbenchPart part) {
                if (part instanceof TextEditor) {
                	TextEditor te = (TextEditor)part;
                	IDocumentProvider provider = te.getDocumentProvider();
                	IDocument document = provider.getDocument(te.getEditorInput());
                	String contents = document.get();
                	return contents;
                }		
				return null;
			}
			
			private IFileEditorInput getEditorInput(IWorkbenchPart part) {
				if (part instanceof TextEditor) {
					TextEditor te = (TextEditor)part;
					IEditorInput ei = te.getEditorInput();
					// check if the file belongs to project of qt nature
					if (QtUtils.isQtProject(QtUtils.qtProjectOfEditorInput(ei))) {
						if (ei instanceof IFileEditorInput) {
							IFileEditorInput fei = (IFileEditorInput)ei;
							return fei;
						}
					}
				}
				return null;
			}
			
			private String getFilePath(IFileEditorInput fei) {
				if (fei != null)
					return fei.getFile().getLocation().toOSString();
				
				return null;
			}
			
			private String getFilePath(IWorkbenchPart part) {
				IFileEditorInput fei = getEditorInput(part);
				if (fei != null)
					return getFilePath(fei);
				
				return null;
			}
				
			private void storeQObjectMacroData(IWorkbenchPart part) {
				IFileEditorInput fei = getEditorInput(part);
				String filePath = getFilePath(fei);
				if (filePath != null) {
					String proFilePath = QtUtils.findProFile(fei.getFile().getProject()).toOSString();
					if (filePath.equals(proFilePath)) {
						return;
					}
				    // in case part was not opened before parse it and store info for it
					if (!filePathToQObjectMacro.containsKey(filePath)) {
						// add it.
						IProject project = QtUtils.qtProjectOfEditorInput(fei);
						IPath proPath = QtUtils.findProFile(project);
						if (proPath != null) {
						    filePathToProjectPath.put(filePath, proPath.toOSString());
							filePathToQObjectMacro.put(filePath, hasQObjectMacro(getContents(part)));
						}
					}
					
				}
			}

			public void partBroughtToTop(IWorkbenchPart part) {
				storeQObjectMacroData(part);
			}

			public void partClosed(IWorkbenchPart part) {
				IFileEditorInput fei = getEditorInput(part);
				if (fei != null) {
					// remove the info about Q_OBJECT macro for it.
					IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
					if (window == null)
						return;
					IWorkbenchPage page = window.getActivePage();
					if (page == null)
						return;
					IEditorReference refs[] = page.findEditors(fei, null, org.eclipse.ui.IWorkbenchPage.MATCH_INPUT);
					// only remove private data when count is 0. If count is 1 or more there still can be opened editor for the same input
					if (refs.length == 0) {
						String filePath = getFilePath(fei);
						// remove it.
						filePathToQObjectMacro.remove(filePath);
						filePathToProjectPath.remove(filePath);
					}
					
				}
			}

			public void partDeactivated(IWorkbenchPart part) {
				// TODO Auto-generated method stub
				
			}

			public void partOpened(IWorkbenchPart part) { // gets called also for views
				storeQObjectMacroData(part);
	  		}
        	
        	
        });

	}

	/**
	 * This method is called when the plug-in is stopped
	 */
	public void stop(BundleContext context) throws Exception {
		super.stop(context);
		ResourcesPlugin.getWorkspace().removeResourceChangeListener(proFileListener);
		plugin = null;
	}

	/**
	 * Returns the shared instance.
	 */
	public static QtProjectPlugin getDefault() {
		return plugin;
	}

	/**
	 * Gets the default Qt version string
	 * @return the default Qt version string, or "" if none
	 */
	public String getDefaultQtVersion() {
		return QtProjectPlugin.getDefault().getPreferenceStore().getString(PreferenceConstants.QTVERSION_DEFAULT);
	}
	
	/**
	 * Adds a new Qt version and sets it as the default
	 * @param versionName Qt version name
	 * @param binDir Qt bin directory
	 * @param incDir Qt include directory
	 */
	public void addDefaultQtVersion(String versionName, IPath binDir, IPath incDir) {
		IPreferenceStore store = getPreferenceStore();

		int count = store.getInt(PreferenceConstants.QTVERSION_COUNT);
		store.setValue(PreferenceConstants.QTVERSION_COUNT, count + 1);
		
		store.setValue(PreferenceConstants.QTVERSION_NAME + "." + Integer.toString(count), versionName); //$NON-NLS-1$
		store.setValue(PreferenceConstants.QTVERSION_BINPATH + "." + Integer.toString(count), binDir.toOSString()); //$NON-NLS-1$
		store.setValue(PreferenceConstants.QTVERSION_INCLUDEPATH+ "." + Integer.toString(count), incDir.toOSString()); //$NON-NLS-1$

		store.setValue(PreferenceConstants.QTVERSION_DEFAULT, versionName);
	}

	/**
	 * Gets the list of registered qmake environment modifiers
	 * @return the list of modifiers, may be empty
	 */
	public List<IQMakeEnvironmentModifier> getEnvironmentModifierExtensions() {
		if (envModifiers == null) {
			envModifiers = new ArrayList<IQMakeEnvironmentModifier>();

			IExtensionRegistry extensionRegistry = Platform.getExtensionRegistry();
			IExtensionPoint extensionPoint = extensionRegistry.getExtensionPoint(PLUGIN_ID + ".qmakeEnvironmentModifier"); //$NON-NLS-1$
			IExtension[] extensions = extensionPoint.getExtensions();
			
			for (int i = 0; i < extensions.length; i++) {
				IExtension extension = extensions[i];
				IConfigurationElement[] elements = extension.getConfigurationElements();
				IConfigurationElement element = elements[0];
				
				boolean failed = false;
				try {
					Object extObject = element.createExecutableExtension("class"); //$NON-NLS-1$
					if (extObject instanceof IQMakeEnvironmentModifier) {
						envModifiers.add((IQMakeEnvironmentModifier)extObject);
					} else {
						failed = true;
					}
				} 
				catch (CoreException e) {
					failed = true;
				}
				
				if (failed) {
					logErrorMessage("Unable to load qmakeEnvironmentModifier extension from " + extension.getContributor().getName()); //$NON-NLS-1$
				}
			}
		}
		
		return envModifiers;
	}
	
	/**
	 * Logs the given error string to the error log
	 * @param errMsg the error message
	 */
	public void logErrorMessage(String errMsg) {
		ILog log = plugin.getLog();
		if (log != null) {
			log.log(new Status(IStatus.ERROR, PLUGIN_ID, errMsg));
		}
	}
}
