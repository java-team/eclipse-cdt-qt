/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject;

import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.ui.IStartup;

import com.trolltech.qtcppproject.launch.QtLaunchConfig;

public class QtStartup implements IStartup {

	public void earlyStartup() {
		new QtProjectMonitor();
		
		ILaunchManager launchManager = DebugPlugin.getDefault().getLaunchManager();
		QtLaunchConfig qlc = new QtLaunchConfig();
		launchManager.addLaunchConfigurationListener(qlc);
	}

}
