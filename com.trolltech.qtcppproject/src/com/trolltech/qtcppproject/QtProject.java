/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject;

import java.io.File;
import java.io.FileFilter;
import java.util.Map;
import java.util.Vector;

import org.eclipse.cdt.core.CCorePlugin;
import org.eclipse.cdt.core.ICDescriptor;
import org.eclipse.cdt.core.ICDescriptorOperation;
import org.eclipse.cdt.core.model.CoreModel;
import org.eclipse.cdt.core.model.IIncludeEntry;
import org.eclipse.cdt.core.model.IPathEntry;
import org.eclipse.cdt.core.resources.IPathEntryStore;
import org.eclipse.cdt.make.core.IMakeBuilderInfo;
import org.eclipse.cdt.make.core.IMakeTarget;
import org.eclipse.cdt.make.core.IMakeTargetManager;
import org.eclipse.cdt.make.core.MakeBuilder;
import org.eclipse.cdt.make.core.MakeCorePlugin;
import org.eclipse.cdt.make.core.MakeProjectNature;
import org.eclipse.cdt.make.core.scannerconfig.IScannerConfigBuilderInfo2;
import org.eclipse.cdt.make.core.scannerconfig.ScannerConfigNature;
import org.eclipse.cdt.make.internal.core.scannerconfig2.ScannerConfigProfileManager;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.resources.WorkspaceJob;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;

import com.trolltech.qtcppproject.launch.QtLaunchConfig;
import com.trolltech.qtcppproject.preferences.QtPreferencePage;
import com.trolltech.qtcppproject.utils.QtUtils;

public class QtProject {
	private static final String QTVERSION = "com.trolltech.qtcppproject.properties.qtversion";
 	private static final String QTPROCHANGELISTENER = "com.trolltech.qtcppproject.properties.qtprochangelistener";
	private static final String QT_RELEASE_NAME = "Qt Release Build";
	private static final String QT_DEBUG_NAME = "Qt Debug Build";
	
	private static final String[] QT_INCLUDE_PATHS = {
		"",
		"ActiveQt",
		"phonon",
		"Qt3Support",
		"QtAssistant",
		"QtCore",
		"QtDBus",
		"QtDesigner",
		"QtGui",
		"QtHelp",
		"QtNetwork",
		"QtOpenGL",
		"QtScript",
		"QtSql",
		"QtSvg",
		"QtTest",
		"QtUiTools",
		"QtWebKit",
		"QtXml",
		"QtXmlPatterns"
	};

	private IProject wrapped;
	
	public QtProject(IProject wrappedProject) {
		wrapped = wrappedProject;
	}
	
	public IProject getProject() {
		return wrapped;
	}
	
	public String getQtBinPath() {
		try {
			String version = wrapped.getPersistentProperty(new QualifiedName("", QTVERSION));
			return QtPreferencePage.getQtVersionBinPath(version);
		} catch (CoreException e) {
			e.printStackTrace();
			return null;
		}
	}
	public String getQtIncludePath() {
		try {
			String version = wrapped.getPersistentProperty(new QualifiedName("", QTVERSION));
			return QtPreferencePage.getQtVersionIncludePath(version);
		} catch (CoreException e) {
			e.printStackTrace();
			return null;
		}
	}

	public String getQtVersion() {
		try {
			return wrapped.getPersistentProperty(new QualifiedName("", QTVERSION));
		} catch (CoreException ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public boolean setQtVersion(String version) {
		try {
			String oldBinPath = getQtBinPath();
			String oldIncludePath = getQtIncludePath();
			wrapped.setPersistentProperty(new QualifiedName("", QTVERSION), version);
			updateQtDir(oldBinPath, oldIncludePath);
		} catch (CoreException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean runQMakeWhenProFileChanges() {
		try {
			String value = wrapped.getPersistentProperty(new QualifiedName("", QTPROCHANGELISTENER));
			return (value != null && value.equals("true"));
		} catch (CoreException ex) {
			ex.printStackTrace();
		}
		return false;
	}
	
	public void setRunQMakeWhenProFileChanges(boolean enable) {
		try {
			wrapped.setPersistentProperty(new QualifiedName("", QTPROCHANGELISTENER), enable ? "true" : "false");
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}

	public void updateQtDir(String oldBinPath, String oldIncludePath) {
		try {
			if (!wrapped.hasNature(QtNature.QT_NATURE_ID))
				return;
			
			if (!wrapped.hasNature(MakeProjectNature.NATURE_ID))
				return;

			String qtBinPath = getQtBinPath();
			String qtIncludePath = getQtIncludePath();
			if (qtBinPath == null || qtIncludePath == null)
				return;
			String mkspec = QtUtils.getMakeSpec(new Path(qtBinPath).removeLastSegments(1).toOSString());
			boolean setmkcmd = QtPreferencePage.getAutoSetMkCmd();
			String mkcmd = null;
			if (setmkcmd)
				mkcmd = QtUtils.getMakeCommand(mkspec);
				
			IMakeBuilderInfo info = MakeCorePlugin.createBuildInfo(wrapped, MakeBuilder.BUILDER_ID);
			Map env = info.getEnvironment();
			
			if (QtPreferencePage.getAutoSetMkSpec() && mkspec != null)
				env.put("QMAKESPEC", mkspec);

			String path = QtUtils.createPath((String)env.get("PATH"), 
					qtBinPath, 
					oldBinPath);
			env.put("PATH", path);
			info.setEnvironment(env);
			
			if (setmkcmd) {
				info.setBuildAttribute(IMakeBuilderInfo.BUILD_COMMAND, mkcmd);
				// make debug build the default (this can only work, if qmake is called with CONFIG+=debug_and_release
				// for all platforms)
				info.setBuildAttribute(IMakeBuilderInfo.BUILD_TARGET_AUTO, "debug");
				info.setBuildAttribute(IMakeBuilderInfo.BUILD_TARGET_INCREMENTAL, "debug");
			}
			
			IMakeTargetManager targetManager = MakeCorePlugin.getDefault().getTargetManager();
			String[] builders = targetManager.getTargetBuilders(wrapped);
			for(int i=0; i<builders.length; ++i) {
				IMakeTarget target = targetManager.findTarget(wrapped, QT_RELEASE_NAME);
				if (target != null) {
					if (setmkcmd)
						target.setBuildAttribute(IMakeTarget.BUILD_COMMAND, mkcmd);											
					target.setEnvironment(env);					
				}
				target = targetManager.findTarget(wrapped, QT_DEBUG_NAME);
				if (target != null) {
					if (setmkcmd)
						target.setBuildAttribute(IMakeTarget.BUILD_COMMAND, mkcmd);											
					target.setEnvironment(env);
				}
			}
			
			// update the launch configs
			QtLaunchConfig.updateLaunchPaths(wrapped, 
					qtBinPath,
					oldBinPath);
			
			// update the include directories
			updateQtIncludeDir(qtIncludePath, oldIncludePath);
		} catch (CoreException e) {
			// ### this throws an exception if the .project file is read-only 
			e.printStackTrace();
		}
	}

	public void scheduleRebuild() {
		final IProject project = wrapped;
		// This is inspired by org.eclipse.ui.internal.ide.dialogs.CleanDialog
		// (see plugin org.eclipse.ui.ide)
		WorkspaceJob cleanJob = new WorkspaceJob("Clean "+project.getName()) {
    		public boolean belongsTo(Object family) {
    			return ResourcesPlugin.FAMILY_MANUAL_BUILD.equals(family);
    		}
			public IStatus runInWorkspace(IProgressMonitor monitor) {
				try {
					project.build(
							IncrementalProjectBuilder.CLEAN_BUILD,
							monitor);
					WorkspaceJob buildJob = new WorkspaceJob("Build "+project.getName()) {
			    		public boolean belongsTo(Object family) {
			    			return ResourcesPlugin.FAMILY_MANUAL_BUILD.equals(family);
			    		}
						public IStatus runInWorkspace(IProgressMonitor monitor) {
							try {
								project.build(
										IncrementalProjectBuilder.INCREMENTAL_BUILD,
										monitor);
							} catch (CoreException e) {
							}
							return Status.OK_STATUS;
						}
					};
					buildJob.setRule(project.getWorkspace().getRuleFactory()
					        .buildRule());
					buildJob.setUser(true);
					buildJob.schedule();
				} catch (CoreException e) {
				}
                return Status.OK_STATUS;
    		}
    	};
        cleanJob.setRule(project.getWorkspace().getRuleFactory()
                .buildRule());
        cleanJob.setUser(true);
        cleanJob.schedule();
	}	

	public void convertToQtProject(IProgressMonitor monitor) throws CoreException
	{
		if (!wrapped.hasNature(org.eclipse.cdt.make.core.MakeProjectNature.NATURE_ID))
			org.eclipse.cdt.core.CCorePlugin.getDefault().convertProjectToCC(wrapped, monitor, MakeCorePlugin.MAKE_PROJECT_ID);
		
		if (!wrapped.hasNature(MakeProjectNature.NATURE_ID))
				MakeProjectNature.addNature(wrapped, new SubProgressMonitor(monitor, 1));
		if (!wrapped.hasNature(ScannerConfigNature.NATURE_ID)) {
			ScannerConfigNature.addScannerConfigNature(wrapped);
			IScannerConfigBuilderInfo2 scannerConfig =
				ScannerConfigProfileManager.createScannerConfigBuildInfo2(wrapped, "org.eclipse.cdt.make.core.GCCStandardMakePerProjectProfile");
			scannerConfig.save();
		}
		addQtNature(monitor);
	}	

	private void addQtNature(IProgressMonitor monitor)
		throws CoreException
	{
		// "Associating the nature with a project", taken from eclipse doc: "Project natures"
		IProjectDescription description = wrapped.getDescription();
		String[] natures = description.getNatureIds();
	
		String[] newNatures = new String[natures.length + 1];
		System.arraycopy(natures, 0, newNatures, 0, natures.length);
		newNatures[natures.length + 0] = QtProConstants.QTNATURE_ID;
	
		description.setNatureIds(newNatures);
		wrapped.setDescription(description, monitor);
		configureMake();
	}

	private void updateQtIncludeDir(String newQtIncludePath, String oldQtIncludePath)
	{
		try {
			Vector v = new Vector();
			IPathEntryStore store = CoreModel.getPathEntryStore(wrapped);
			String[] includePaths;
			File newDirectory = new File(newQtIncludePath);
			if (newDirectory.canRead() && newDirectory.isDirectory()) {
				File[] subDirectories = newDirectory.listFiles(new FileFilter(){
					public boolean accept(File pathname) {
						if (pathname.canRead() && pathname.isDirectory())
							return true;
						return false;
					}
				});
				includePaths = new String[subDirectories.length+1];
				includePaths[0] = "";
				for (int i = 0; i < subDirectories.length; ++i)
					includePaths[i+1] = subDirectories[i].getName();
			} else {
				includePaths = QT_INCLUDE_PATHS;
			}
			for (int i = 0; i < includePaths.length; i++) {
				IIncludeEntry pathEntry = CoreModel.newIncludeEntry(new Path(""), 
						new Path(newQtIncludePath), 
						new Path(includePaths[i]));
				v.add(pathEntry);
			}

			//remove old path entries and duplicate paths
			IPathEntry[] paths = store.getRawPathEntries();
			for (int i=0; i<paths.length; ++i) {
				if (paths[i] instanceof IIncludeEntry) {
					String oldIndexerIncludePath = ((IIncludeEntry)paths[i]).getFullIncludePath().toOSString();
					if ((oldQtIncludePath != null 
							&& oldIndexerIncludePath.startsWith(oldQtIncludePath))
							|| oldIndexerIncludePath.startsWith(newQtIncludePath))
						continue;
				}
				v.add(paths[i]);
			}
			
			store.setRawPathEntries((IPathEntry[])v.toArray(new IPathEntry[v.size()]));
		} catch (CoreException e) {}
	}

	private void configureMake() {
		try {
			IMakeBuilderInfo info = MakeCorePlugin.createBuildInfo(wrapped, MakeBuilder.BUILDER_ID);
			info.setAppendEnvironment(true);
			info.setUseDefaultBuildCmd(false);
			info.setBuildAttribute(IMakeBuilderInfo.BUILD_TARGET_AUTO, "");
			info.setBuildAttribute(IMakeBuilderInfo.BUILD_TARGET_INCREMENTAL, "");
		} catch (CoreException e) {
			e.printStackTrace();
		}
		
		if (Platform.getOS().equals(Platform.OS_WIN32))
			setBinaryParser("org.eclipse.cdt.core.PE");

		createQtTargets();
		updateQtDir("", "");
	}

	private void setBinaryParser(String id) {
		class TmpICDescriptorOperation implements ICDescriptorOperation {
			public String m_id;
			public void execute(ICDescriptor descriptor, IProgressMonitor monitor) throws CoreException {
				descriptor.create(CCorePlugin.BINARY_PARSER_UNIQ_ID, m_id);
			}			
		}
		
		TmpICDescriptorOperation op = new TmpICDescriptorOperation();
		op.m_id = id;
		
		try {
			CCorePlugin.getDefault().getCDescriptorManager().runDescriptorOperation(wrapped, op, null);
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}

	private void createQtTargets() {
		MakeCorePlugin makecore = MakeCorePlugin.getDefault();
		IMakeTargetManager targetManager = makecore.getTargetManager();
		try {
			String[] builders = targetManager.getTargetBuilders(wrapped);
			for(int i=0; i<builders.length; ++i) {
				IMakeTarget target = targetManager.findTarget(wrapped, QT_RELEASE_NAME);
				if (target == null) {
					target = targetManager.createTarget(wrapped, QT_RELEASE_NAME, builders[i]);
					targetManager.addTarget(target);
				}
				initTarget(target, "release");
				
				target = targetManager.findTarget(wrapped, QT_DEBUG_NAME);
				if (target == null) {
					target = targetManager.createTarget(wrapped, QT_DEBUG_NAME, builders[i]);
					targetManager.addTarget(target);
				}
				initTarget(target, "debug");
			}
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}
	
	private static void initTarget(IMakeTarget target, String config) {
		try {
			target.setBuildAttribute(IMakeTarget.BUILD_TARGET, config);
			target.setUseDefaultBuildCmd(false);
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}
}
