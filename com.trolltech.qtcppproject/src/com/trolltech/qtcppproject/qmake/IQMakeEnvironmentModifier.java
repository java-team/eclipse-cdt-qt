/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject.qmake;

import java.util.Properties;

import org.eclipse.core.resources.IProject;

/**
 * Allows those using the QMakeEnvironmentModifier extension point to modify the
 * environment variables used when invoking qmake.
 *
 */
public interface IQMakeEnvironmentModifier {

	/**
	 * Take the existing environment and return the desired modified environment.
	 * @param project the project that qmake will be run against
	 * @param environment the current environment variables
	 * @return the modified environment variables
	 */
	public Properties getModifiedEnvironment(IProject project, Properties environment);

}
