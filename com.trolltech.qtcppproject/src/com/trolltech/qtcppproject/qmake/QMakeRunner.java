/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject.qmake;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.eclipse.cdt.core.CCorePlugin;
import org.eclipse.cdt.core.CommandLauncher;
import org.eclipse.cdt.core.ConsoleOutputStream;
import org.eclipse.cdt.core.resources.IConsole;
import org.eclipse.cdt.utils.spawner.EnvironmentReader;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.PreferencesUtil;

import com.trolltech.qtcppproject.QtProject;
import com.trolltech.qtcppproject.QtProjectPlugin;
import com.trolltech.qtcppproject.preferences.PreferenceConstants;
import com.trolltech.qtcppproject.preferences.QtPreferencePage;
import com.trolltech.qtcppproject.utils.QtUtils;


public class QMakeRunner {

	private static final boolean WINDOWS = java.io.File.separatorChar == '\\';

 	public static String runQMake(IProject project, IProgressMonitor monitor)
	{
		checkCancel(monitor);
 		removeQMakeErrors(project);

		try {
 			String errStr = QMakeRunner.runQMake(project, null, monitor);
 			if (errStr != null) {
 				reportQMakeError(project, errStr);
 				return errStr;
			}
 		} catch (CoreException e) {
 			e.printStackTrace();
 			reportQMakeError(project, e.getLocalizedMessage());
 			return e.getLocalizedMessage();
		}
 		return null;
	}
	
 	public static void checkCancel(IProgressMonitor monitor) {
		if (monitor != null && monitor.isCanceled())
			throw new OperationCanceledException();
	}

 	private static final String QMAKE_PROBLEM = "com.trolltech.qtcppproject.qtproblem";

 	private static void removeQMakeErrors(IProject project) {
		try {
 			project.deleteMarkers(QMAKE_PROBLEM, false, IResource.DEPTH_ZERO);
		} catch (CoreException ex) {}
	}
	
 	private static void reportQMakeError(IProject project, String message) {
		try {
 			IMarker marker = project.createMarker(QMAKE_PROBLEM);
			marker.setAttribute(IMarker.MESSAGE, message);
			marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_ERROR);
		} catch (CoreException ex) {}		
	}	
	
	/**
	 * Runs qmake for the given project
	 * @param project the project to run qmake on
	 * @param envMods name/value pairs of environment variables to set
	 * @param monitor progress monitor
	 * @return error string, or null if qmake ran without errors
	 * @throws CoreException 
	 */
	public static String runQMake(IProject project, Map<String, String> envMods, IProgressMonitor monitor) throws CoreException {

		IPath proFilePath = QtUtils.findProFile(project);
		
		if (proFilePath == null) {
			return "Unable to locate pro file for project";
		}

		IPath buildCommand = getQMakeCommand(project);
		if (buildCommand == null) {
			return "Could not execute qmake (no Qt version set). Open Preferences->Qt and set a Qt version to use.";
		}

		IConsole console = CCorePlugin.getDefault().getConsole();
		console.start(project);
		ConsoleOutputStream consoleOut = console.getOutputStream();
		ConsoleOutputStream consoleErr = console.getErrorStream();

		// create the environment
		Properties envProps = EnvironmentReader.getEnvVars();
		IPath workingDir = QtUtils.removeFileName(proFilePath);
		envProps.setProperty("CWD", workingDir.toOSString());
		envProps.setProperty("PWD", workingDir.toOSString());

		// allow registered extensions to modify the environment, e.g. EPOCROOT
		for (IQMakeEnvironmentModifier envModifier : QtProjectPlugin.getDefault().getEnvironmentModifierExtensions()) {
			envProps = envModifier.getModifiedEnvironment(project, envProps);
		}

		// now make any environment changes that were passed in
		if (envMods != null) {
			for (String name : envMods.keySet()) {
				envProps.setProperty(name, envMods.get(name));
			}
		}
		
		String errStr = runQMake(proFilePath, buildCommand, consoleOut, consoleErr, envProps, monitor);
		if (errStr == null) {
			try {
				project.refreshLocal(IResource.DEPTH_INFINITE, null);
			} catch (CoreException e) {
			}
		}
		
		markGeneratedFilesAsDerived(project);
		
		return errStr;
	}
	
	public static void markGeneratedFilesAsDerived(IProject project) {
		//TODO ideally this would use some API to get the real list
		// of generated files.  for now we'll just mark any bld.inf
		// and *.mmp files as derived.
		
		try {
			project.accept(new IResourceVisitor() {

				public boolean visit(IResource resource) throws CoreException {
					if (resource instanceof IFile) {
						String name = resource.getName().toLowerCase();
						if (name.equals("bld.inf") || name.endsWith(".mmp")) {
							resource.setDerived(true);
						}
						return false;
					}
					return true;
				}
				
			}, IResource.DEPTH_INFINITE, IContainer.EXCLUDE_DERIVED);
		} catch (CoreException e) {
			e.printStackTrace();
		}
		
	}
	
	private static String runQMake(IPath proFilePath, IPath buildCommand,
			ConsoleOutputStream stdout, ConsoleOutputStream stderr, Properties envProps,
			IProgressMonitor monitor) {

		if (monitor == null) {
			monitor = new NullProgressMonitor();
		}
		
		SubMonitor subMonitor = SubMonitor.convert(monitor, 2);
		subMonitor.newChild(1).subTask("QMake Runner - Collecting Data");

		CommandLauncher launcher = new CommandLauncher();
		launcher.showCommand(true);
		
		String errMsg = null;
		
		List<String> args = new ArrayList<String>();
		args.add("-recursive");
		if (!WINDOWS) {
			args.add("CONFIG+=debug_and_release");			
		}
		
		args.add(QtUtils.getFileName(proFilePath, false));
		IPath workingDir = QtUtils.removeFileName(proFilePath);
		
		Process p = launcher.execute(buildCommand, args.toArray(new String[args.size()]),
				createEnvStringList(envProps), workingDir);
		if (p != null) {
			try {
				p.getOutputStream().close();
			} catch (IOException e) {}
			
			subMonitor.newChild(1).subTask("QMake Runner - Starting QMake: " + launcher.getCommandLine());
						
			if (launcher.waitAndRead(stdout, stderr, new SubProgressMonitor(monitor, 0))
				!= CommandLauncher.OK)
				errMsg = launcher.getErrorMessage();
			
			if (p.exitValue() != 0) {
				String outputStr = stderr.readBuffer();
				if (outputStr != null && outputStr.length() > 0) {
					errMsg = outputStr;
				} else {
					outputStr = stdout.readBuffer();
					if (outputStr != null && outputStr.length() > 0) {
						errMsg = outputStr;
					} else {
						errMsg = "Error processing '" + proFilePath + "'";
					}
				}
			} else {
				QtProjectPlugin.getDefault().clearRunningQMakeRequest(proFilePath.toOSString());
			}

			try {
				stdout.close();
				stderr.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		} else {
			errMsg = launcher.getErrorMessage();
		}

		monitor.done();

		return errMsg;
	}

	/**
	 * Get the absolute path to the qmake to run
	 * @param project
	 * @return the absolute path to qmake, or null if none can be determined
	 */
	private static IPath getQMakeCommand(IProject project) {

		String qtBinPath = null;

		if (project != null) {
			// get the version specified in the project
			QtProject qtProject = new QtProject(project);
			qtBinPath = qtProject.getQtBinPath();
		} else {
			// no project, just get the default version
			String defaultVersion = QtProjectPlugin.getDefault().getPreferenceStore().getString(PreferenceConstants.QTVERSION_DEFAULT);
			qtBinPath = QtPreferencePage.getQtVersionBinPath(defaultVersion);
		}
		
		if (qtBinPath == null) {
			PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {
				public void run() {
					Shell shell = PlatformUI.getWorkbench().getDisplay().getActiveShell();
					MessageDialog dialog = new MessageDialog(shell, "Build Error", null,
							"No default Qt version is set. Open the Qt page in the preferences dialog and add a Qt version.", 
							MessageDialog.ERROR, 
							new String[] { "Open preferences",
			                        IDialogConstants.CANCEL_LABEL }, 0);
			        if (dialog.open() == 1)
			        	return;
					PreferencesUtil.createPreferenceDialogOn(shell, 
							"com.trolltech.qtcppproject.preferences.QtPreferencePage", 
							null, null).open();
				}
			});

			if (project != null) {
				// get the version specified in the project
				QtProject qtProject = new QtProject(project);
				qtBinPath = qtProject.getQtBinPath();
			} else {
				// no project, just get the default version
				String defaultVersion = QtProjectPlugin.getDefault().getPreferenceStore().getString(PreferenceConstants.QTVERSION_DEFAULT);
				qtBinPath = QtPreferencePage.getQtVersionBinPath(defaultVersion);
			}
			
			if (qtBinPath == null) {
				return null;
			}
		}

		return new Path(qtBinPath).append("qmake");
	}

	private static String[] createEnvStringList(Properties envProps) {
		String[] env = null;
		List<String> envList = new ArrayList<String>();
		Enumeration<?> names = envProps.propertyNames();
		if (names != null) {
			while (names.hasMoreElements()) {
				String key = (String) names.nextElement();
				envList.add(key + "=" + envProps.getProperty(key));
			}
			env = (String[]) envList.toArray(new String[envList.size()]);
		}
		return env;
	}

}
