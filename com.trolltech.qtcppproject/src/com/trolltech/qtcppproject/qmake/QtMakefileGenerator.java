/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject.qmake;

import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;

import com.trolltech.qtcppproject.QtProjectPlugin;
import com.trolltech.qtcppproject.utils.QtUtils;

public class QtMakefileGenerator extends IncrementalProjectBuilder
{
	protected IProject[] build(int kind, Map args, IProgressMonitor monitor) throws CoreException
	{
		// Only run qmake if there is no Makefile generated
		// Strictly speaking, there is QMAKE_MAKEFILE qmake variable
		// which stores the output makefile name. Basically it looks like
		// the name of the generated makefile is always "Makefile", but it can be
		// in theory changed by setting QMAKE_MAKEFILE in *.pro file. This case
		// is not handled at the moment.
		
		if (kind != AUTO_BUILD) {
			IProject project = getProject();
			IPath proFilePath = QtUtils.findProFile(project);
			
			if (proFilePath != null) {
				IPath proDir = QtUtils.removeFileName(proFilePath);
				IPath makefilePath = proDir.append(new String("Makefile"));
				if (!makefilePath.toFile().exists() || // we run automatically qmake only in case Makefile is missing
						QtProjectPlugin.getDefault().isRunningQMakeRequest(proFilePath.toOSString())) // or when Q_OBJECT macro was added/removed to project's source file
					QMakeRunner.runQMake(project, monitor);
			}

		}
		
		return null;
	}
	
	protected void clean(IProgressMonitor monitor) {
		// do nothing for now...
	}
 
}