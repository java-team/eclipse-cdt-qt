/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

public class GeneratedFilesFilter extends ViewerFilter {
	public boolean select(Viewer viewer, Object parentElement, Object element) {
		IResource resource = null;
		if (element instanceof IResource)
			resource = (IResource)element;
		else if (element instanceof IAdaptable)
			resource = (IResource)((IAdaptable)element).getAdapter(IResource.class);
		if (resource != null) {
			if (resource.getName().startsWith("moc_") && resource.getName().endsWith(".cpp"))
				return false;
			if (resource.getName().startsWith("ui_") && resource.getName().endsWith(".h"))
				return false;
			if (resource.getName().startsWith("qrc_") && resource.getName().endsWith(".cpp"))
				return false;
		}
		return true;
	}
}
