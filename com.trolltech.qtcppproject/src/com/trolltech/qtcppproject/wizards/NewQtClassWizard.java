/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject.wizards;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;

import com.trolltech.qtcppproject.wizards.pages.UiFileWizardPage;


public class NewQtClassWizard extends AbstractQtClassWizard {
	private UiFileWizardPage m_uipage;

	public NewQtClassWizard() {
		setWindowTitle("New Qt Gui Class");
	}

    public void addPages() {
        super.addPages();
        
        m_uipage = new UiFileWizardPage(m_selection);
        addPage(m_uipage);
    }

	protected void createFiles(IProgressMonitor monitor) throws InterruptedException, CoreException {
		m_uipage.createFiles(monitor);
	}
	
}
