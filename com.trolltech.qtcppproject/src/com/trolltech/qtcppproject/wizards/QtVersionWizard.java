/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject.wizards;

import org.eclipse.jface.wizard.Wizard;

import com.trolltech.qtcppproject.wizards.pages.QtVersionWizardPage;

public class QtVersionWizard extends Wizard {
	String versionName;
	String binPath;
	String includePath;

	QtVersionWizardPage wizardPage;

	public boolean performFinish() {
		versionName = wizardPage.getVersionName();
		binPath = wizardPage.getBinPath();
		includePath = wizardPage.getIncludePath();
		return true;
	}

	public void addPages() {
		wizardPage = new QtVersionWizardPage("Qt Version");
		addPage(wizardPage);
	}
	
	public String getVersionName() {
		return versionName;
	}

	public void setVersionName(String versionName) {
		wizardPage.setVersionName(versionName);
	}

	public String getBinPath() {
		return binPath;
	}

	public void setBinPath(String binPath) {
		wizardPage.setBinPath(binPath);
	}

	public String getIncludePath() {
		return includePath;
	}

	public void setIncludePath(String includePath) {
		wizardPage.setIncludePath(includePath);
	}
}
