/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject.wizards.pages;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import com.trolltech.qtcppproject.utils.QtUtils;

public class ImportQtWizardPage extends WizardPage implements SelectionListener, ModifyListener {
	private Table list;
	private Text profilelocation;
	private IPath pathToProFile = null;
	
	public ImportQtWizardPage(String pageName) {
		super(pageName);
		setTitle(pageName);
		setDescription("Import a Qt project from the local file system into the workspace");
		setPageComplete(false);
	}

	public void createControl(Composite parent) {
		Composite workArea = new Composite(parent, SWT.NONE);
		setControl(workArea);

		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		
		workArea.setLayout(layout);
		workArea.setLayoutData(new GridData(GridData.FILL_BOTH
				| GridData.GRAB_HORIZONTAL | GridData.GRAB_VERTICAL));
		
		Label label = new Label(workArea, SWT.NONE);
		label.setText("Select .pro file: ");
		
		profilelocation = new Text(workArea, SWT.BORDER);
		profilelocation.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		profilelocation.addModifyListener(this);
		
		Button browse = new Button(workArea, SWT.NONE);
		browse.setText("Browse...");
		browse.addSelectionListener(this);
		
		list = new Table(workArea, SWT.CHECK | SWT.BORDER);
		list.addSelectionListener(this);
		GridData data = new GridData(GridData.FILL_BOTH);
		data.horizontalSpan = 3;
		list.setLayoutData(data);
	}

	/**
	 * Extracts the name of the pro file from the user set path.
	 */
	public String getProjectName() {
		return QtUtils.getFileName(pathToProFile, true);
	}

	/**
	 * Extracts the directory name where the pro file is location.
	 */
	public IPath getProFileLocation() {
		return QtUtils.removeFileName(pathToProFile);
	}

	public void widgetDefaultSelected(SelectionEvent e) {
		
	}

	public void widgetSelected(SelectionEvent e) {
		if (e.widget == list) {
			updatePage();			
		} else {
			FileDialog filedlg = new FileDialog(getShell());
			
			String[] filterext = {"*.pro"}; 
			String[] filtername = {"Qt Project File"};
			filedlg.setFilterExtensions(filterext);
			filedlg.setFilterNames(filtername);
			filedlg.setText("Select Qt Project File");
			String filename = filedlg.open();
			if (filename != null)
				profilelocation.setText(filename);
		}
	}
	
	private void updatePage() {
		setErrorMessage(null);
		boolean enabled = false;
		for (int i=0; i<list.getItemCount(); ++i) {
			if (list.getItem(i).getChecked()) {
				enabled = true;
				break;
			}
		}
		if (!checkProFileLocation())
			enabled = false;
		setPageComplete(enabled);
	}

	private boolean checkProFileLocation() {
		try {
			String rootPath = ResourcesPlugin.getWorkspace().getRoot().getLocation().toFile().getCanonicalPath();
			String proPath = getProFilePath().toFile().getCanonicalPath();
			if (proPath.startsWith(rootPath)) {
				setErrorMessage("Cannot import pro-file from workspace location");
				return false;
			}
		} catch (IOException e) {
			setErrorMessage("Cannot resolve path");
			return false;
		}
		return true;
	}
	
	public void modifyText(ModifyEvent e) {
		list.removeAll();
		if (isValidProFile()) {
			TableItem item = new TableItem(list, SWT.NONE);
			item.setText(getProjectName());
			item.setChecked(true);
		}
		updatePage();		
	}
	
	private IPath getProFilePath() {
		return new Path(profilelocation.getText().trim());
	}
	
	private boolean isValidProFile() {
		pathToProFile = getProFilePath();
		if (!pathToProFile.getFileExtension().equals("pro"))
			return false;
		
		File file = pathToProFile.toFile();
		if (!file.exists())
			return false;
		
		return true;
	}
}
