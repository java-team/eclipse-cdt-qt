/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject.wizards.pages;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import com.trolltech.qtcppproject.QtProConstants;

public class QtModulesWizardPage extends WizardPage {
	private static int NUM_MODULES = 11;
	private Button[] buttons = new Button[NUM_MODULES]; // filled automatically
	private int[] buttonConstants = {
		QtProConstants.QtCore,
		QtProConstants.QtGui,
		QtProConstants.QtSql,
		QtProConstants.QtXml,
		QtProConstants.QtXmlPatterns,
		QtProConstants.QtNetwork,
		QtProConstants.QtSvg,
		QtProConstants.QtOpenGL,
		QtProConstants.QtWebKit,
		QtProConstants.QtScript,
		QtProConstants.Qt3Support
	};
	private String[] proEntries = {
		"core ",
		"gui ",
		"sql ",
		"xml ",
		"xmlpatterns ",
		"network ",
		"svg ",
		"opengl ",
		"webkit ",
		"script ",
		"qt3support "
	};
	private String[] visibleStrings = {
		"Core",
		"Gui",
		"SQL",
		"XML",
		"XMLPatterns",
		"Network",
		"SVG",
		"OpenGL",
		"WebKit",
		"Script",
		"Qt3 Support"
	};
	
	private boolean hasInitialized;
	private int selectedModules;
	private int requiredModules;
	
	public QtModulesWizardPage(String pageName) {
		super(pageName);
		setDescription("Select the Qt modules for the project.");
		setTitle("Qt Modules");
		selectedModules = 0;
		requiredModules = 0;
		hasInitialized = false;
	}
	
	public void setSelectedModules(int mods)
	{
		selectedModules = mods;
		refreshSelectedModules();
	}
	
	public void setRequiredModules(int mods)
	{
		requiredModules = mods;
		refreshSelectedModules();
	}
	
	public int getSelectedModules()
	{
		selectedModules = 0;
		for (int i = 0; i < NUM_MODULES; ++i)
			if (buttons[i].getSelection()) 
				selectedModules |= buttonConstants[i];
		return selectedModules;
	}
	
	public String getModules()
	{
		String modules = "";
		for (int i = 0; i < NUM_MODULES; ++i)
			if (buttons[i].getSelection()) 
				modules += proEntries[i];
		return modules;
	}

	public void createControl(Composite parent) {
		Composite mainComposite = new Composite(parent, SWT.NONE);
		
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		mainComposite.setLayout(layout);
		
		Group moduleGroup = new Group(mainComposite, SWT.PUSH);
		GridData gridData = new GridData();
		gridData.horizontalSpan = 2;
	    gridData.grabExcessHorizontalSpace = true;
	    gridData.horizontalAlignment = GridData.FILL;
	    gridData.verticalAlignment = GridData.BEGINNING;
	    moduleGroup.setLayoutData(gridData);		
	    moduleGroup.setLayout(new RowLayout(SWT.VERTICAL));				
	    moduleGroup.setText("Qt Modules");
		
		for (int i = 0; i < NUM_MODULES; ++i) {
			buttons[i] = new Button(moduleGroup, SWT.CHECK);
			buttons[i].setText(visibleStrings[i]);
			buttons[i].setSelection(false);
		}
	    
		hasInitialized = true;
		refreshSelectedModules();
		enableModules(requiredModules, false);
		
		setControl(mainComposite);
		setPageComplete(true);		
	}
	
	private void refreshSelectedModules()
	{
		if (!hasInitialized)
			return;
		int modules = selectedModules | requiredModules;
		for (int i = 0; i < NUM_MODULES; ++i) {
			if ((modules & buttonConstants[i]) != 0)
				buttons[i].setSelection(true);
		}
	}
	
	private void enableModules(int modules, boolean enabled)
	{
		if (!hasInitialized)
			return;
		for (int i = 0; i < NUM_MODULES; ++i) {
			if ((modules & buttonConstants[i]) != 0)
				buttons[i].setEnabled(enabled);
		}
	}
}