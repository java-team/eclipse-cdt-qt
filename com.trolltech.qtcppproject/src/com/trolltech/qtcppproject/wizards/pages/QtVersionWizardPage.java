/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject.wizards.pages;

import org.eclipse.jface.preference.DirectoryFieldEditor;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.trolltech.qtcppproject.utils.QtUtils;

public class QtVersionWizardPage extends WizardPage{
	Text versionName;
	DirectoryFieldEditor binPath;
	DirectoryFieldEditor includePath;
	
	public QtVersionWizardPage(String pageName) {
		super(pageName);
		setTitle("Qt Version");
		setDescription("Specify the Name and Bin + Include Pathes of the Qt version.");
	}

	public void createControl(Composite parent) {
		Composite composite =  new Composite(parent, SWT.NULL);

		GridLayout gl = new GridLayout();
		int ncol = 3;
		gl.numColumns = ncol;
		composite.setLayout(gl);

		Label versionNameLabel = new Label(composite, SWT.NULL);
		versionNameLabel.setText("Version Name:");
		versionName = new Text(composite, SWT.BORDER | SWT.SINGLE);
		versionName.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}});
		
		GridData versionNameGridData = new GridData(GridData.FILL_HORIZONTAL);
		versionName.setLayoutData(versionNameGridData);
		new Label(composite, SWT.NULL);
		new Label(composite, SWT.NULL);
		new Label(composite, SWT.NULL);
		new Label(composite, SWT.NULL);
		
		binPath =
			new DirectoryFieldEditor("bin", "Bin Path:", composite);
		binPath.getTextControl(composite).addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}});
		binPath.setPropertyChangeListener(new IPropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent event) {
				pathSelected(QtUtils.QT_PATH_TYPE_BIN);
			}});
		new Label(composite, SWT.NULL);
		versionName.setLayoutData(versionNameGridData);
		Label binPathDescription = new Label(composite, SWT.NULL);
		binPathDescription.setText("Path containing tools 'qmake', 'uic', 'rcc', etc.\n");
		new Label(composite, SWT.NULL);
		new Label(composite, SWT.NULL);
		new Label(composite, SWT.NULL);
		new Label(composite, SWT.NULL);
		
		includePath = 
			new DirectoryFieldEditor("include", "Include Path:", composite);
		includePath.getTextControl(composite).addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}});
		includePath.setPropertyChangeListener(new IPropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent event) {
				pathSelected(QtUtils.QT_PATH_TYPE_INCLUDE);
			}});
		new Label(composite, SWT.NULL);
		Label includePathDescription = new Label(composite, SWT.NULL);
		includePathDescription.setText("Path containing the include pathes 'QtCore', 'QtGui', etc.");
		new Label(composite, SWT.NULL);
		
		setControl(composite);
		setPageComplete(isValid());
	}

	private boolean isVersionNameValid() {
		return versionName.getText().length() > 0;
	}
	
	private boolean isBinPathValid() {
		return QtUtils.isValidQtPath(binPath.getStringValue(), QtUtils.QT_PATH_TYPE_BIN);
	}
	
	private boolean isIncludePathValid() {
		return QtUtils.isValidQtPath(includePath.getStringValue(), QtUtils.QT_PATH_TYPE_INCLUDE);
	}
	
	private boolean isValid() {
		return (isVersionNameValid() && isBinPathValid() && isIncludePathValid());
	}

	public void dialogChanged() {
		boolean isValid = isValid();

		if (!isValid) {
			String errorMessage = "";
			if (!isVersionNameValid())
				errorMessage += "Version Name is empty. ";
			if (!isBinPathValid())
				errorMessage += "Bin Path is invalid. ";
			if (!isIncludePathValid())
				errorMessage += "Include Path is invalid. ";
			setErrorMessage(errorMessage);
		} else {
			setErrorMessage(null);
		}
		
		setPageComplete(isValid);
	}
	
	public void pathSelected(int qtPathType) {
		int siblingPathType = qtPathType == QtUtils.QT_PATH_TYPE_BIN?QtUtils.QT_PATH_TYPE_INCLUDE:QtUtils.QT_PATH_TYPE_BIN;
		String selectedPathEntry = 
			(qtPathType == QtUtils.QT_PATH_TYPE_BIN?binPath:includePath).getStringValue();
		String siblingPathEntry = 
			(qtPathType == QtUtils.QT_PATH_TYPE_BIN?includePath:binPath).getStringValue();
		boolean selectedPathEntryIsValid = QtUtils.isValidQtPath(selectedPathEntry, qtPathType);
		boolean siblingPathEntryIsEmpty = siblingPathEntry.length() == 0;

		if (!selectedPathEntryIsValid) {
			String investigatedPath = QtUtils.getQtSubPathUnderQtPath(selectedPathEntry, qtPathType);
			if (investigatedPath.length() > 0) {
				selectedPathEntry = investigatedPath;
				selectedPathEntryIsValid = true;
			}
		}
		if (selectedPathEntryIsValid && siblingPathEntryIsEmpty)
			siblingPathEntry = QtUtils.getSiblingQtPath(selectedPathEntry, qtPathType, siblingPathType);
		
		(qtPathType == QtUtils.QT_PATH_TYPE_BIN?binPath:includePath).setStringValue(selectedPathEntry);
		(siblingPathType == QtUtils.QT_PATH_TYPE_BIN?binPath:includePath).setStringValue(siblingPathEntry);
	}
	
	public String getVersionName() {
		return versionName.getText();
	}

	public void setVersionName(String versionName) {
		this.versionName.setText(versionName);
		dialogChanged();
	}

	public String getBinPath() {
		return binPath.getStringValue();
	}

	public void setBinPath(String binPath) {
		this.binPath.setStringValue(binPath);
		dialogChanged();
	}

	public String getIncludePath() {
		return includePath.getStringValue();
	}

	public void setIncludePath(String includePath) {
		this.includePath.setStringValue(includePath);
		dialogChanged();
	}
}
