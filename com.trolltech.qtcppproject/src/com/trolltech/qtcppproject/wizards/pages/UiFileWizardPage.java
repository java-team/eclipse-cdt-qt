/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject.wizards.pages;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Locale;

import org.eclipse.cdt.core.CConventions;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ContainerSelectionDialog;

import com.trolltech.qtcppcommon.QtWizardUtils;
import com.trolltech.qtcppproject.QtProjectPlugin;
import com.trolltech.qtcppproject.utils.QtUtils;

public class UiFileWizardPage extends WizardPage implements KeyListener {
	private boolean uichanged = false;

	// controls
	private Group uigroup;

	private Text containerText;
	private Text classText;

	private Text uiFileName;
	private Text headerFileName;
	private Text sourceFileName;

	private Combo uiType;

	private Label uiTypeLable;
	
	private Button filesLowerCase;

	private ISelection selection;
	
	public UiFileWizardPage(ISelection selection) {
		super("");
		this.selection = selection;
		setTitle("User Interface Class");
		setDescription("Creates a user interface connected to a class.");
	}

	private String getUiFileName() {
		return uiFileName.getText();
	}
	private String getUiFileNameBase() {
		return QtUtils.getFileName(new Path(getUiFileName()), true);
	}
	private String getHeaderFileName() {
		return headerFileName.getText();
	}
	private String getHeaderFileNameBase() {
		return QtUtils.getFileName(new Path(getHeaderFileName()), true);
	}
	private String getRelativeHeaderFileName() {
		return QtUtils.makeRelative(new Path(getContainerName()+"/"+getHeaderFileName()), 
				new Path(getContainerName()+"/"+getSourceFileName())).toString();
	}
	private String getSourceFileName() {
		return sourceFileName.getText();
	}
	
	private String[] getFileNames() {
		return new String[] {getUiFileName(), getHeaderFileName(), getSourceFileName()};
	}

	private String[] getFileLocations() {
		return new String[] {getContainerName(), getContainerName(), getContainerName()};
	}
	private String getUISuperClass() {
		return uiType.getText();
	}

	public void createFiles(IProgressMonitor monitor) throws CoreException {

		String baseClass = getUISuperClass();
		String className = getClassName();
		HashMap replaceMap = new HashMap();
		replaceMap.put("%PRE_DEF%", getHeaderFileNameBase().toUpperCase(Locale.US).replace('.', '_')+"_H");
		replaceMap.put("%UI_CLASS%", baseClass);
		replaceMap.put("%CLASS%", className);
		replaceMap.put("%UI_HDR%", "ui_"+getUiFileNameBase()+".h");
		replaceMap.put("%INCLUDE%", getRelativeHeaderFileName());

		String[] templates = {baseClass+".ui", "gui.h", "gui.cpp"};
		String[] fileNames = getFileNames();
		String[] locations = getFileLocations();
		for (int i = 0; i < templates.length; i++) {
			InputStream template = getClass().getResourceAsStream(
					QtProjectPlugin.TEMPLATE_LOCATION +	"QtGui/" + templates[i]);
			IFile file = QtWizardUtils.createFile(new Path(locations[i]+"/"+fileNames[i]), monitor);
			String source = "";
			if (template != null) 
				source = QtWizardUtils.patchTemplateFile(template, replaceMap);
			file.create(new ByteArrayInputStream(source.getBytes()), true, monitor);
		}
	}

	public void createControl(Composite parent) {
		Composite mainComposite = new Composite(parent, SWT.NONE);

		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		mainComposite.setLayout(layout);

		Composite container = new Composite(mainComposite, SWT.NONE);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		container.setLayoutData(gd);
		layout = new GridLayout();
		layout.numColumns = 3;
		layout.verticalSpacing = 9;
		container.setLayout(layout);
		
		Label label = new Label(container, SWT.NULL);
		label.setText("&Source Folder:");

		containerText = new Text(container, SWT.BORDER | SWT.SINGLE);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		containerText.setLayoutData(gd);
		containerText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}
		});

		Button button = new Button(container, SWT.PUSH);
		button.setText("Browse...");
		button.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				handleBrowse();
			}
		});

		label = new Label(container, SWT.NULL);
		label.setText("&Class Name:");

		classText = new Text(container, SWT.BORDER | SWT.SINGLE);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		classText.setLayoutData(gd);
		classText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				syncFileNameBase();
				dialogChanged();
			}
		});

		label = new Label(container, SWT.NULL);
		
		uigroup = new Group(mainComposite, SWT.CHECK);
		uigroup.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL
				| GridData.HORIZONTAL_ALIGN_FILL));
		uigroup.setText("File Information");

		layout = new GridLayout();
		layout.numColumns = 2;
		uigroup.setLayout(layout);

		addFileNameLabel("UI File Name:");
		uiFileName = addFileNameEditor();
		addFileNameLabel("Header File Name:");
		headerFileName = addFileNameEditor();
		addFileNameLabel("Source File Name:");
		sourceFileName = addFileNameEditor();

		uiTypeLable = new Label(uigroup, SWT.PUSH);
		uiTypeLable.setText("UI Type:");

		uiType = new Combo(uigroup, SWT.READ_ONLY);
		uiType.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL
				| GridData.HORIZONTAL_ALIGN_FILL));
		uiType.setItems(new String[] { "QWidget", "QMainWindow", "QDialog" });
		uiType.select(0);
		
		filesLowerCase = new Button(uigroup, SWT.CHECK);
		filesLowerCase.setText("Make file names lowercase");
		filesLowerCase.setSelection(true);
		GridData data = new GridData();
		data.horizontalSpan = 2;
		filesLowerCase.setLayoutData(data);
		filesLowerCase.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {
			}
			public void widgetSelected(SelectionEvent e) {
				syncFileNameBase();
			}
		});

		initialize();
		dialogChanged();
		setControl(mainComposite);
		setPageComplete(true);
	}

	private Label addFileNameLabel(String text) {
		Label label = new Label(uigroup, SWT.PUSH);
		label.setText(text);
		return label;
	}

	private Text addFileNameEditor() {
		Text editor = new Text(uigroup, SWT.BORDER | SWT.SINGLE);
		editor.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL
				| GridData.HORIZONTAL_ALIGN_FILL));
		editor.addKeyListener(this);
		editor.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}
		});
		return editor;
	}
	public void keyPressed(KeyEvent e) {

	}

	public void keyReleased(KeyEvent e) {
		uichanged = true;
		dialogChanged();
	}

	private void syncFileNameBase() {
		if (!uichanged) {
			String className = getClassName();
			if (className.length() == 0) {
				uiFileName.setText("");
				headerFileName.setText("");
				sourceFileName.setText("");
			} else {
				uiFileName.setText("./"+getFileBaseFromClassName()+".ui");
				headerFileName.setText("./"+getFileBaseFromClassName()+".h");
				sourceFileName.setText("./"+getFileBaseFromClassName()+".cpp");
			}
			dialogChanged();
		}
	}

	private String getContainerName() {
		return containerText.getText();
	}
	
	private String getClassName() {
		return classText.getText();
	}

	private void dialogChanged() {
		IResource container = ResourcesPlugin.getWorkspace().getRoot()
				.findMember(new Path(getContainerName()));

		if (getContainerName().length() == 0) {
			updateStatus("Source folder must be specified");
			return;
		}
		if (container == null
				|| (container.getType() & (IResource.PROJECT | IResource.FOLDER)) == 0) {
			updateStatus("Source folder must exist");
			return;
		}
		if (!container.isAccessible()) {
			updateStatus("Project must be writable");
			return;
		}
		if (getClassName().length() == 0) {
			updateStatus("Class name must be specified");
			return;
		}
		if (!isValidClassName()) {
			return;
		}
		if (getUiFileName().length() == 0) {
			updateStatus("Ui file name must be specified");
			return;
		}
		if (getHeaderFileName().length() == 0) {
			updateStatus("Header file name must be specified");
			return;
		}
		if (getSourceFileName().length() == 0) {
			updateStatus("Source file name must be specified");
			return;
		}
		if (!getUiFileName().substring(getUiFileName().length()-3).equalsIgnoreCase(".ui")) {
			updateStatus("Ui file name must end with '.ui'");
			return;
		}
		String[] fileNames = getFileNames();
		String[] locations = getFileLocations();
		for (int i = 0; i < fileNames.length; i++) {
			String name = locations[i]+"/"+fileNames[i];
			IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot(); 
			if (root.findMember(name) != null) {
				updateStatus("File "+name+" already exists");
				return;
			}
			IPath path = new Path(name);
			if (path.hasTrailingSeparator() || root.getFile(new Path(name)) == null) {
				updateStatus("Invalid file resource '" + name + "'");
				return;
			}
		}
		updateStatus(null);
	}

	private boolean isValidClassName() {
		final String invalidClassname = "Class name is not valid : ";
		IStatus val = CConventions.validateClassName(getClassName());
		if (val.getSeverity() == IStatus.ERROR) {
			updateStatus(invalidClassname + val.getMessage());
			return false;
		} else if (val.getSeverity() == IStatus.WARNING) {
			setMessage(invalidClassname + val.getMessage(), WARNING);
		} else {
			setMessage(null);
		}
		setPageComplete(true);
		return true;
	}
	
	private String getFileBaseFromClassName() {
		String fileBase = getClassName();
		if (filesLowerCase.getSelection())
			fileBase = fileBase.toLowerCase(Locale.US);
		return fileBase;
	}
	
	private void updateStatus(String message) {
		setErrorMessage(message);
		setPageComplete(message == null);
	}
	
	/**
	 * Uses the standard container selection dialog to choose the new value for
	 * the container field.
	 */

	private void handleBrowse() {
		ContainerSelectionDialog dialog = new ContainerSelectionDialog(
				getShell(), ResourcesPlugin.getWorkspace().getRoot(), false,
				"Select source folder");
		if (dialog.open() == ContainerSelectionDialog.OK) {
			Object[] result = dialog.getResult();
			if (result.length == 1) {
				containerText.setText(((Path) result[0]).makeRelative().toString());
			}
		}
	}

	/**
	 * Tests if the current workbench selection is a suitable container to use.
	 */

	private void initialize() {
		if (selection != null && selection.isEmpty() == false
				&& selection instanceof IStructuredSelection) {
			IStructuredSelection ssel = (IStructuredSelection) selection;
			if (ssel.size() > 1)
				return;
			Object obj = ssel.getFirstElement();
			if (!(obj instanceof IResource) && (obj instanceof IAdaptable))
				obj = ((IAdaptable)obj).getAdapter(IResource.class);
			if (obj instanceof IResource) {
				IContainer container;
				if (obj instanceof IContainer)
					container = (IContainer) obj;
				else
					container = ((IResource) obj).getParent();
				containerText.setText(container.getFullPath().makeRelative().toString());
			}
		}
		classText.setFocus();
	}
}
