/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject.wizards;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Locale;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.wizard.IWizardPage;

import com.trolltech.qtcppcommon.QtWizardUtils;
import com.trolltech.qtcppproject.QtProConstants;
import com.trolltech.qtcppproject.QtProjectPlugin;
import com.trolltech.qtcppproject.wizards.pages.FilesWizardPage;
import com.trolltech.qtcppproject.wizards.pages.QtModulesWizardPage;

public class QtGuiProjectWizard extends QtProjectWizard {
	private FilesWizardPage filesPage;

	private QtModulesWizardPage modulesPage;

	public void addPages() {
		String title = "Qt Gui Project";
		mainPage.setTitle(title);
		mainPage.setDescription("Create a new Qt Gui Application Project.");
		addPage(mainPage);

		filesPage = new FilesWizardPage("com.trolltech.qtcppproject.FilesWizardPage");
		filesPage.setTitle(title);
		filesPage.setDescription("Setup the class and file names.");
		addPage(filesPage);

		modulesPage = new QtModulesWizardPage(
				"com.trolltech.qtcppproject.QtModulesPage");
		modulesPage.setSelectedModules(QtProConstants.QtCore
				| QtProConstants.QtGui);
		modulesPage.setRequiredModules(QtProConstants.QtCore
				| QtProConstants.QtGui);
		modulesPage.setTitle(title);
		modulesPage.setDescription("Select the Qt modules.");
		addPage(modulesPage);
	}

	public IWizardPage getNextPage(IWizardPage page) {
		if (mainPage.isPageComplete()
				&& getContainer().getCurrentPage() == mainPage) {
			filesPage.setClassName(mainPage.getProjectHandle().getName());
		}
		return super.getNextPage(page);
	}

	public void projectCreated(IProject project, IProgressMonitor monitor) {
		addFiles(project, monitor);
	}

	private static String toValidProjectName(String projectName) {
		return FilesWizardPage.toValidClassName(projectName);
	}
	
	private void addFiles(IProject pro, IProgressMonitor monitor) {
		HashMap replaceMap = new HashMap();

		String hdr = filesPage.getHeaderFileName();
		if (hdr.endsWith(".h"))
			hdr = hdr.substring(0, hdr.length() - 2);
		String preDef = hdr.toUpperCase(Locale.US) + "_H";

		String uiHdr = filesPage.getUIFileName();
		if (uiHdr.endsWith(".ui"))
			uiHdr = uiHdr.substring(0, uiHdr.length() - 3);
		uiHdr = "ui_" + uiHdr + ".h";
		
		String headerFile = filesPage.getHeaderFileName();
		String srcFile = filesPage.getSourceFileName();
		String uiFile = filesPage.getUIFileName();

		replaceMap.put("%MODULES%", modulesPage.getModules());
		
		replaceMap.put("%INCLUDE%", headerFile);
		replaceMap.put("%CLASS%", filesPage.getClassName());

		replaceMap.put("%PRE_DEF%", preDef);
		replaceMap.put("%UI_HDR%", uiHdr);

		String uiClass = filesPage.getUiClassName();
		replaceMap.put("%UI_CLASS%", uiClass);
		
		String projectName = toValidProjectName(pro.getName());
		replaceMap.put("%PROJECT%", projectName);
		
		replaceMap.put("%HEADER_FILE%", headerFile);
		replaceMap.put("%SOURCE_FILE%", srcFile);
		replaceMap.put("%UI_FILE%", uiFile);

		InputStream src = getClass().getResourceAsStream(QtProjectPlugin.TEMPLATE_LOCATION + "QtGui/gui.pro");
		File dest = new File(pro.getLocation().toOSString() + "/" + projectName + ".pro");
		QtWizardUtils.addTemplateFile(src, dest, replaceMap);

		src = getClass().getResourceAsStream(QtProjectPlugin.TEMPLATE_LOCATION + "QtGui/main.cpp");
		dest = new File(pro.getLocation().toOSString() + "/main.cpp");
		QtWizardUtils.addTemplateFile(src, dest, replaceMap);

		src = getClass().getResourceAsStream(QtProjectPlugin.TEMPLATE_LOCATION + "QtGui/gui.cpp");
		dest = new File(pro.getLocation().toOSString() + "/" + srcFile);
		QtWizardUtils.addTemplateFile(src, dest, replaceMap);

		src = getClass().getResourceAsStream(QtProjectPlugin.TEMPLATE_LOCATION + "QtGui/gui.h");
		dest = new File(pro.getLocation().toOSString() + "/" + headerFile);
		QtWizardUtils.addTemplateFile(src, dest, replaceMap);

		src = getClass().getResourceAsStream(QtProjectPlugin.TEMPLATE_LOCATION + "QtGui/" + uiClass + ".ui");
		dest = new File(pro.getLocation().toOSString() + "/" + uiFile);
		QtWizardUtils.addTemplateFile(src, dest, replaceMap);
	}
}
