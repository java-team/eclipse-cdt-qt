/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject.wizards;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExecutableExtension;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IImportWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.wizards.newresource.BasicNewProjectResourceWizard;
import org.eclipse.ui.wizards.newresource.BasicNewResourceWizard;

import com.trolltech.qtcppproject.QtProjectCreator;
import com.trolltech.qtcppproject.wizards.pages.ImportQtWizardPage;

public class ImportQtWizard extends Wizard implements IImportWizard, IExecutableExtension
{
	ImportQtWizardPage mainPage;
	IWorkbench workbench;
	IConfigurationElement configuration;

	public ImportQtWizard() {
		super();
	}

	public boolean performFinish() {
		IProject hProject = ResourcesPlugin.getWorkspace().getRoot().getProject(mainPage.getProjectName());
		QtProjectCreator procreator = new QtProjectCreator(hProject, 
				mainPage.getProFileLocation());
		procreator.create(getContainer());
		// switch perspective
		BasicNewProjectResourceWizard.updatePerspective(configuration);
		// select the new project
		BasicNewResourceWizard.selectAndReveal(hProject, workbench.getActiveWorkbenchWindow());
		
        return true;
	}

	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.workbench = workbench;
		setWindowTitle("Qt Import Wizard");
		setNeedsProgressMonitor(true);
		mainPage = new ImportQtWizardPage("Import Qt Project");
	}
	
    public void addPages() {
        super.addPages(); 
        addPage(mainPage);        
    }

	public void setInitializationData(IConfigurationElement config, String propertyName, Object data) throws CoreException {
		configuration = config;
	}

}
