/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject.wizards;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.dialogs.WizardNewProjectCreationPage;
import org.eclipse.ui.wizards.newresource.BasicNewProjectResourceWizard;

import com.trolltech.qtcppproject.QtProjectCreator;

public abstract class QtProjectWizard extends BasicNewProjectResourceWizard implements INewWizard, QtProjectCreatorListener 
{
	protected WizardNewProjectCreationPage mainPage;

	public void init(IWorkbench workbench, IStructuredSelection selection) {
		super.init(workbench, selection);
		setWindowTitle("New Qt Gui Application Project");
		mainPage = new WizardNewProjectCreationPage("com.trolltech.qtcppproject.MainWizardPage");
	}

	public boolean performFinish() {
		if (mainPage.isPageComplete()) {
			createNewProject();
			updatePerspective(); // Does this like switching to "finalPerspective"
			selectAndReveal(mainPage.getProjectHandle());
			return true;
		}
		return false;
	}

	public void createNewProject() {
		QtProjectCreator procreator = new QtProjectCreator(mainPage.getProjectHandle(), mainPage.getLocationPath());
		procreator.addQtProjectCreatorListener(this);
		procreator.create(getContainer());
	}
}
