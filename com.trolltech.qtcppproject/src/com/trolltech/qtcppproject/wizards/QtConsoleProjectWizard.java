/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject.wizards;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;

import com.trolltech.qtcppcommon.QtWizardUtils;
import com.trolltech.qtcppproject.QtProConstants;
import com.trolltech.qtcppproject.QtProjectPlugin;
import com.trolltech.qtcppproject.wizards.pages.QtModulesWizardPage;

public class QtConsoleProjectWizard extends QtProjectWizard {

	private QtModulesWizardPage modulesPage;

	public void addPages() {
		String title = "Qt Console Project";
		mainPage.setTitle(title);
		mainPage.setDescription("Create a new Qt Console Application Project.");
		addPage(mainPage);

		modulesPage = new QtModulesWizardPage(
				"com.trolltech.qtcppproject.QtModulesPage");
		modulesPage.setSelectedModules(QtProConstants.QtCore);
		modulesPage.setRequiredModules(QtProConstants.QtCore);
		
		modulesPage.setTitle(title);
		modulesPage.setDescription("Select the Qt modules.");
		addPage(modulesPage);
	}

	public void projectCreated(IProject project, IProgressMonitor monitor) {
		addFiles(project, monitor);
	}

	private void addFiles(IProject project, IProgressMonitor monitor) {
		HashMap replaceMap = new HashMap();

		String projectName = project.getName();
		replaceMap.put("%PROJECT%", projectName);
		replaceMap.put("%MODULES%", modulesPage.getModules());

		InputStream src = getClass().getResourceAsStream(QtProjectPlugin.TEMPLATE_LOCATION + "QtConsole/console.pro");
		File dest = new File(project.getLocation().toOSString() + "/" + projectName + ".pro");
		QtWizardUtils.addTemplateFile(src, dest, replaceMap);
		
		src = getClass().getResourceAsStream(QtProjectPlugin.TEMPLATE_LOCATION + "QtConsole/main.cpp");
		dest = new File(project.getLocation().toOSString() + "/main.cpp");
		QtWizardUtils.addTemplateFile(src, dest, null);
		
		
	}
}
