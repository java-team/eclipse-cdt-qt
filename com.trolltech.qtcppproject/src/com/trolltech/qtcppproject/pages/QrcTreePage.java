/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject.pages;

import java.io.File;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.part.FileEditorInput;

import com.trolltech.qtcppcommon.editors.EditorInputWatcher;
import com.trolltech.qtcppproject.editors.QrcEditor;
import com.trolltech.qtcppproject.pages.embedded.QrcTreeView;
import com.trolltech.qtcppproject.pages.embedded.QrcTreeViewListener;

public class QrcTreePage extends FormPage implements QrcTreeViewListener
{
	private QrcEditor m_editor;
	private QrcTreeView qrctreeview;

	public QrcTreePage(QrcEditor editor)
	{
		super(editor, "com.trolltech.QtProEditor.pages.QrcTreePage", "Resources");
		m_editor = editor;
	}

	public void setActive(boolean active)
	{
		super.setActive(active);
	}
	
	public void widgetSelected(SelectionEvent e)
	{

	}
	
	public void widgetDefaultSelected(SelectionEvent e)
	{
		
	}
	
	public boolean isDirty()
	{
		return (qrctreeview != null ? qrctreeview.isDirty() : false);
	}
	
	public void doSave(IProgressMonitor monitor)
	{
		if (qrctreeview == null)
			return;
		FileEditorInput fin = (FileEditorInput)getEditorInput();
		if (!qrctreeview.save()) {
    		MessageDialog.openError(getSite().getShell(),
    				"Error while saving file",
    				"Can't write to: " + fin.getFile().getLocation().toOSString()
    				+ "\nMake sure it's not write protected.");
		}
	}
	
	protected void createFormContent(IManagedForm managedForm)
	{
		Composite parent = managedForm.getForm().getBody();
		FileEditorInput fin = (FileEditorInput)getEditorInput();
		String fileName = fin.getFile().getLocation().toOSString();
		File file = new File(fileName);
		boolean readable = file.exists() && file.canRead();
		if (!readable) {
			EditorInputWatcher.createMissingFileInfo(parent, fileName);
			return;
		} 
		FormToolkit toolkit = managedForm.getToolkit();
		GridLayout layout = new GridLayout(1, false);
		parent.setLayout(layout);
		createQrcTreeView(toolkit, parent);
		qrctreeview.load(fileName);
	}

	public void reload() {
		FileEditorInput fin = (FileEditorInput)getEditorInput();
		String fileName = fin.getFile().getLocation().toOSString();
		qrctreeview.load(fileName);
	}

	private void createQrcTreeView(FormToolkit toolkit, Composite parent)
	{
		qrctreeview = new QrcTreeView(parent, SWT.EMBEDDED);
		qrctreeview.addQrcTreeViewListener(this);
		qrctreeview.setLayoutData(new GridData(500, 400));
		toolkit.adapt(qrctreeview);
		toolkit.paintBordersFor(qrctreeview);
	}
	
	public String contents() {
		return (qrctreeview != null ? qrctreeview.contents() : "");
	}

	public void dirtyChanged() {
		m_editor.editorDirtyStateChanged();
	}

	public void setFocus() {
		super.setFocus();
		if (qrctreeview != null)
			qrctreeview.setFocus();
	}
}
