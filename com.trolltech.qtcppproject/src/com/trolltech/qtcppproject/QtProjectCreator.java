/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject;

import java.io.File;
import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableContext;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.CopyFilesAndFoldersOperation;
import org.eclipse.ui.actions.WorkspaceModifyOperation;

import com.trolltech.qtcppproject.wizards.QtProjectCreatorListener;

public class QtProjectCreator {
	private IProject project;
	private IPath projectLocation = null;
	private QtProjectCreatorListener listener = null;
	private boolean copyToWorkspace = false;
	private Shell shell;

	public QtProjectCreator(IProject project, IPath projectLocation) {
		this(null, project, projectLocation, false);
	}
	public QtProjectCreator(Shell shell, IProject project, IPath projectLocation, boolean copyToWorkspace) {
		this.project = project;
		this.projectLocation = projectLocation;
		this.copyToWorkspace = copyToWorkspace;
		this.shell = shell;
	}

	public void addQtProjectCreatorListener(
			QtProjectCreatorListener listener) {
		this.listener = listener;
	}

	public void create(IRunnableContext runnableContext) {
		IPath defaultPath = Platform.getLocation();
		// if the project path is either directly the workspace path, or a direct
		// child, we may not set it but have to use 'null'... otherwise the
		// IProject.create(...) will fail with an exception.
		if (defaultPath.equals(projectLocation) 
				|| defaultPath.equals(projectLocation.removeLastSegments(1)))
			projectLocation = null;

		final IWorkspace workspace = ResourcesPlugin.getWorkspace();
		final IProjectDescription description = workspace
				.newProjectDescription(project.getName());
		final IProject newProject = project;
		if (!copyToWorkspace)
			description.setLocation(projectLocation);
		else
			description.setLocation(null); // default location

		WorkspaceModifyOperation op = new WorkspaceModifyOperation() {
			protected void execute(IProgressMonitor monitor)
					throws CoreException {
				try {
					monitor.subTask("Configuring Qt Project...");
					newProject.create(description, monitor);
					if (monitor.isCanceled())
						throw new OperationCanceledException();
					newProject.open(monitor);
					if (copyToWorkspace) {
						if (monitor.isCanceled())
							throw new OperationCanceledException();
						monitor.subTask("Copying the files...");
						CopyFilesAndFoldersOperation copyProc = new CopyFilesAndFoldersOperation(shell);
						String sourcePath = projectLocation.addTrailingSeparator().toOSString();
						File sourceDir = new File(sourcePath);
						String[] copyFileNames = sourceDir.list();
						if (copyFileNames != null) {
							for (int i = 0; i < copyFileNames.length; ++i)
								copyFileNames[i] = sourcePath + copyFileNames[i];
							copyProc.copyFilesInCurrentThread(copyFileNames, newProject, monitor);
						}
					}
					monitor.subTask("Setting up project builders.");
					if (monitor.isCanceled())
						throw new OperationCanceledException();
					if (listener != null)
						listener.projectCreated(newProject, monitor);
					newProject.refreshLocal(1, monitor);
					(new QtProject(newProject)).convertToQtProject(monitor);
					monitor.done();
				} catch (CoreException ex) {
					IStatus status = ex.getStatus();
					String message = "";
					// In our case it usually will be a multi-status... we need to find/collect
					// all the different error messages because the multi-status' message doesn't
					// do that for us.
					if (!status.isMultiStatus()) {
						message = status.getMessage();
					} else {
						IStatus[] children = status.getChildren();
						for (int i = 0; i < children.length; i++) {
							if (children[i].isOK())
								continue;
							message += children[i].getMessage();
						}
					}
					message.trim();
					Shell shell = PlatformUI.getWorkbench().getDisplay().getActiveShell();
					MessageDialog.openError(shell, "Project creation failed", message);
					monitor.done();
					return;
				}
			}
		};
		try {
			runnableContext.run(false, true, op);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}

}
