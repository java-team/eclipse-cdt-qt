/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject;

import org.eclipse.swt.graphics.RGB;

public interface QtProConstants
{
	final static String FILES_BEGIN_TAG = "# ECLIPSE_PROJECT_FILES_BEGIN";
	final static String FILES_END_TAG = "# ECLIPSE_PROJECT_FILES_END";
	final static String SETTINGS_BEGIN_TAG = "# ECLIPSE_PROJECT_SETTINGS_BEGIN";
	final static String SETTINGS_END_TAG = "# ECLIPSE_PROJECT_SETTINGS_END";
	
	final static String QTBUILDER_ID = "com.trolltech.qtcppproject.QtMakefileGenerator";
	final static String QTNATURE_ID = "com.trolltech.qtcppproject.QtNature";

	RGB PRO_SETTINGS_COLOR = new RGB(188, 188, 188);
	RGB PRO_SOURCE_COLOR = new RGB(188, 188, 188);
	RGB PRO_COMMENT_COLOR = new RGB(63, 127, 95);
	RGB PRO_DEFAULT_COLOR = new RGB(0, 0, 255);
	
	static final int NoModules		= 0x0000;
	static final int QtCore			= 0x0001;
	static final int QtGui			= 0x0002;
	static final int QtSql			= 0x0004;
	static final int QtXml			= 0x0008;
	static final int QtXmlPatterns	= 0x0010;
	static final int QtNetwork		= 0x0020;
	static final int QtSvg			= 0x0040;
	static final int QtOpenGL		= 0x0080;
	static final int QtWebKit		= 0x0100;
	static final int QtScript		= 0x0200;
	static final int QtTest			= 0x0400;
	static final int QtHelp			= 0x0800;
	static final int Qt3Support		= 0x1000;
}