/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject.dialogs;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.editors.text.TextEditor;
import org.eclipse.ui.PartInitException;
import org.eclipse.jface.text.IDocument;

import com.trolltech.qtcppproject.editors.ProEditor;
import com.trolltech.qtcppproject.pages.embedded.ScopeList;
import com.trolltech.qtcppproject.utils.QtProjectVisitor;

public class SelectScopeDialog extends Dialog implements SelectionListener {
	// Controls
	private ScopeList m_scope;
	private Label m_label;
	private Combo m_combo;
	
	// Variables
	private IProject m_pro;
	private Map m_files;
	private Vector m_profiles;
	private String m_desc = "";
	private boolean m_remove = false;
	
	public SelectScopeDialog(Shell parentShell, IProject pro) {
		super(parentShell);
		m_pro = pro;
		m_profiles = new Vector();
		m_files = new HashMap();
	}
	
	public void setDescription(String desc) {
		m_desc = desc;
	}
    
    public void addFiles(Vector files, String var) {
		m_remove = false;
		if (!m_desc.equals(""))
			m_desc = "Select where you want to insert the files in the Qt project settings.";											
				
		m_files.put(var, files);
	}

	public void removeFiles(Vector files, String var) {
    	m_remove = true;
		if (!m_desc.equals(""))
			m_desc = "The files will be removed from the following Qt project settings:";											
    	
		m_files.put(var, files);
    }
    
    public void widgetSelected(SelectionEvent e) {
		int index = m_combo.getSelectionIndex();
		setProjectFile((IFile)m_profiles.get(index));
	}
	
	public void widgetDefaultSelected(SelectionEvent e) {
		
	}
	
	public void okPressed() {
		for (int i=0; i<m_profiles.size(); ++i) {
			IFile file = (IFile)m_profiles.get(i);
			String proFileName = file.getLocation().toOSString();
			if (!m_scope.isChanged(proFileName))
				continue;

			String contents;
	    	if (m_remove)
				contents = m_scope.removeFiles(proFileName);
			else
				contents = m_scope.addFiles(proFileName);
				
			openAndModifyProjectFile(file, contents);
 		}
	
		close();
	}
	
	public void cancelPressed() {
		close();
	}

	protected void setProjectFile(IFile file) {
		m_scope.showModel(file.getLocation().toOSString(), getContents(file), !m_remove); // instructs the C++ tree view to show the contents of the pro file
	}
	
	private String getContents(IFile file) {
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		IEditorPart part = page.findEditor(new FileEditorInput(file)); // check if proper one is returned ()
		String contents = new String();
		if (part instanceof TextEditor) { // in case the editor is not opened either read the contents from file or invoke the old method
			TextEditor te = (TextEditor)part;
			IDocumentProvider provider = te.getDocumentProvider();
			IDocument document = provider.getDocument(te.getEditorInput());
			contents = document.get();
		} else {
			try {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(file.getContents()));
				String line;
				do {
					line = reader.readLine();
					if (line != null)
						contents += line + "\n";
				} while (line != null);
				reader.close();
			} catch (Exception e) {
			}
		}
        return contents;		
	}
	
	protected void openAndModifyProjectFile(IFile file, String newContents) {
		try {
			IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			IEditorPart part = page.openEditor(new FileEditorInput(file), ProEditor.ID, 
		    		true /*activate*/, 
		    		IWorkbenchPage.MATCH_ID | IWorkbenchPage.MATCH_INPUT /*reuse graphical editor if already open for provided input*/);
			if (part instanceof TextEditor) { // in case the editor is not opened either read the contents from file or invoke the old method
				TextEditor te = (TextEditor)part;
				IDocumentProvider provider = te.getDocumentProvider();
				IDocument document = provider.getDocument(te.getEditorInput());
				document.set(newContents);
			}
		} catch (WorkbenchException e) {
			e.printStackTrace();
		}
	}
	
	protected boolean findProjectFiles() {
		QtProjectVisitor provisitor = new QtProjectVisitor();
		m_profiles.addAll(provisitor.findFiles(m_pro, "pro"));
		m_profiles.addAll(provisitor.findFiles(m_pro, "pri")); // all pro and pri files inside m_pro eclipse project
		Collections.sort(m_profiles, new Comparator() {
			public int compare(Object o1, Object o2) {
				int res = ((IResource)o1).getParent().getProjectRelativePath().toOSString().compareTo(
						((IResource)o2).getParent().getProjectRelativePath().toOSString());
				if (res != 0)
					return res;
				return ((IResource)o1).getName().compareTo(((IResource)o2).getName());
			}
			public boolean equals(Object obj) { return compare(this, obj) == 0; }
		});
		return !m_profiles.isEmpty();
	}
	
	protected void addFiles() {
		Iterator keyit = m_files.keySet().iterator();
		while(keyit.hasNext()) {
			String var = (String)keyit.next();
			Vector files = (Vector)m_files.get(var);
			for (int i=0; i<files.size(); ++i) {
				m_scope.addFile(((IFile)files.get(i)).getLocation().toOSString(), var);
			}
		}
	}
	
	protected void createRemoveDialog(Composite composite) {
		m_combo = new Combo(composite, SWT.READ_ONLY);
		
		GridData data = new GridData(GridData.GRAB_HORIZONTAL
                | GridData.HORIZONTAL_ALIGN_FILL);
        data.widthHint = convertHorizontalDLUsToPixels(IDialogConstants.MINIMUM_MESSAGE_AREA_WIDTH);
        m_combo.setLayoutData(data);
        m_combo.setFont(composite.getParent().getFont());
		
		m_scope = new ScopeList(composite, SWT.EMBEDDED);
        data = new GridData(GridData.GRAB_HORIZONTAL
                | GridData.GRAB_VERTICAL | GridData.HORIZONTAL_ALIGN_FILL
                | GridData.VERTICAL_ALIGN_FILL);
        data.widthHint = convertHorizontalDLUsToPixels(IDialogConstants.MINIMUM_MESSAGE_AREA_WIDTH);
        data.heightHint = 300;
        m_scope.setLayoutData(data);
        m_scope.setFont(composite.getParent().getFont());

        addFiles();

		Vector profiles = m_profiles;
		m_profiles = new Vector();
		
		for (int i=0; i<profiles.size(); ++i) {
			IFile file = (IFile)profiles.get(i);
			if (m_scope.search(file.getLocation().toOSString(), getContents(file))) {
				m_profiles.add(file);
				m_combo.add(file.getProjectRelativePath().toOSString());
			}
		}
		
		if (!m_profiles.isEmpty()) {
			selectProperProFile();
		} else {
			m_label.setText("Could not find the removed file(s) in any Qt project file. You have to edit your pro/pri files manually.");
			m_combo.setEnabled(false);
			m_scope.showModel("", "", false);
		}
	}
	
	protected void createAddDialog(Composite composite) {
		// combo box			
		m_combo = new Combo(composite, SWT.READ_ONLY);
		
		for (int i=0; i<m_profiles.size(); ++i) {
			IResource res = (IResource)m_profiles.get(i);
			if (res.getType() == IResource.FILE) {
				IFile f = (IFile)res;
				m_combo.add(f.getProjectRelativePath().toOSString());
			}
		}
		
		GridData data = new GridData(GridData.GRAB_HORIZONTAL
                | GridData.HORIZONTAL_ALIGN_FILL);
        data.widthHint = convertHorizontalDLUsToPixels(IDialogConstants.MINIMUM_MESSAGE_AREA_WIDTH);
        m_combo.setLayoutData(data);
        m_combo.setFont(composite.getParent().getFont());
        
		// scope selection tree
        m_scope = new ScopeList(composite, SWT.EMBEDDED);
        data = new GridData(GridData.GRAB_HORIZONTAL
                | GridData.GRAB_VERTICAL | GridData.HORIZONTAL_ALIGN_FILL
                | GridData.VERTICAL_ALIGN_FILL);
        data.widthHint = convertHorizontalDLUsToPixels(IDialogConstants.MINIMUM_MESSAGE_AREA_WIDTH);
        data.heightHint = 300;
        
        m_scope.setLayoutData(data);
        m_scope.setFont(composite.getParent().getFont());
        
        addFiles();

        if (!m_profiles.isEmpty()) {
        	selectProperProFile();
			m_scope.selectFirstVariable();
        } else {
			m_label.setText("Could not find any Qt project file. You have to edit your pro/pri files manually.");
			m_combo.setEnabled(false);
			m_scope.showModel("", "", false);
        }
	}

	private void selectProperProFile() {
		int index = 0;
		if (!m_files.values().isEmpty()) {
			IFile first = (IFile)((Vector)m_files.values().toArray()[0]).firstElement();
			String path = first.getParent().getProjectRelativePath().toPortableString();
			for (int i = 0; i < m_profiles.size(); i++) {
				IPath otherPath = ((IResource)m_profiles.elementAt(i)).getParent().getProjectRelativePath();
				if (otherPath.toPortableString().equals(path)) {
					index = i;
					if ("pro".equals(otherPath.getFileExtension()))
						break;
				}
			}
		}
		m_combo.select(index);
		m_combo.addSelectionListener(this);
		setProjectFile((IFile)m_profiles.get(index));
	}
	
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);
		
		m_label = new Label(composite, SWT.WRAP);
		m_label.setText(m_desc);
		
        GridData data = new GridData(GridData.GRAB_HORIZONTAL
                | GridData.HORIZONTAL_ALIGN_FILL);
        data.widthHint = convertHorizontalDLUsToPixels(IDialogConstants.MINIMUM_MESSAGE_AREA_WIDTH);
        m_label.setLayoutData(data);
        m_label.setFont(composite.getParent().getFont());
		
        findProjectFiles();
		if (m_remove) {
			createRemoveDialog(composite);
		} else {
			createAddDialog(composite);
		}

		applyDialogFont(composite);
		return composite;
	}
	
    protected void configureShell(Shell shell) {
        super.configureShell(shell);
        if (m_remove)
        	shell.setText("Remove files from Qt project");
        else
        	shell.setText("Add files to Qt project");
    }
}
