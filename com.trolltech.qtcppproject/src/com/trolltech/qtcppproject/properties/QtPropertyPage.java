/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject.properties;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.PropertyPage;

import com.trolltech.qtcppproject.QtProject;
import com.trolltech.qtcppproject.preferences.QtPreferencePage;

public class QtPropertyPage extends PropertyPage {

	private Combo versioncombo;
	private Button proFileListenerCheck;
	private int oldVersionIndex;

	public QtPropertyPage() {
		super();
	}
	
	private void addControls(Composite parent) {
		Composite composite = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		composite.setLayout(layout);
		
		GridData data = new GridData();
		data.verticalAlignment = GridData.FILL;
		data.horizontalAlignment = GridData.FILL;
		data.grabExcessHorizontalSpace = true;
		data.grabExcessVerticalSpace = true;
		composite.setLayoutData(data);
		
		Label label = new Label(composite, SWT.NONE);
		label.setText("Use Qt Version: ");
		versioncombo = new Combo(composite, SWT.READ_ONLY);
		
		data = new GridData();
		data.horizontalAlignment = GridData.FILL;
		data.grabExcessHorizontalSpace = true;
		versioncombo.setLayoutData(data);
		
		proFileListenerCheck = new Button(composite, SWT.CHECK);
		proFileListenerCheck.setText("Run qmake when .pro file changes");
		proFileListenerCheck.setToolTipText("When enabled, qmake will be invoked whenever the .pro file or any of its includes are changed.");
		data = new GridData();
		data.horizontalAlignment = GridData.FILL;
		data.grabExcessHorizontalSpace = true;
		proFileListenerCheck.setLayoutData(data);
	}

	private void loadPersistentSettings() {
		versioncombo.add("<Default>");
		
		String[] versions = QtPreferencePage.getQtVersions();
		
		QtProject qtProject = new QtProject((IProject)getElement());
		String currentVersion = qtProject.getQtVersion();
		
		versioncombo.select(0);
		if (versions != null) {
			for (int i=0; i<versions.length; ++i) {
				versioncombo.add(versions[i]);
				if (versions[i].equals(currentVersion)) {
					versioncombo.select(i + 1);
				}
			}
		}
		setOldVersionToSelectedVersion();
		
		proFileListenerCheck.setSelection(qtProject.runQMakeWhenProFileChanges());
	}
	
	private boolean savePersistentSettings() {
		QtProject qtProject = new QtProject((IProject)getElement());
		qtProject.setRunQMakeWhenProFileChanges(proFileListenerCheck.getSelection());

		return qtProject.setQtVersion(versioncombo.getItem(versioncombo.getSelectionIndex()));
	}
	
	private boolean versionHasChanged() {
		return (versioncombo.getSelectionIndex() != oldVersionIndex);
	}
	
	private void setOldVersionToSelectedVersion() {
		oldVersionIndex = versioncombo.getSelectionIndex();
	}
	
	protected Control createContents(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		composite.setLayout(layout);
		GridData data = new GridData(GridData.FILL);
		data.grabExcessHorizontalSpace = true;
		composite.setLayoutData(data);

		addControls(composite);
		
		loadPersistentSettings();
		
		return composite;
	}

	protected void performDefaults() {
		versioncombo.select(0);
	}
	
	public boolean performOk() {
		if (savePersistentSettings()) {
			if (versionHasChanged()) {
				if (!requestFullBuild())
					return false;
				setOldVersionToSelectedVersion();
			}
		} else {
			return false;
		}
		return true;
	}

	private boolean requestFullBuild() {
		boolean accepted = false;
		Shell shell = PlatformUI.getWorkbench().getDisplay().getActiveShell();
		MessageDialog dialog = new MessageDialog(shell, "Qt Version Changed", null,
				"The project's Qt version has changed. A rebuild of the project is required for changes to take effect. Do a full rebuild now?", 
				MessageDialog.QUESTION, 
				new String[] { IDialogConstants.YES_LABEL, IDialogConstants.NO_LABEL,
                        IDialogConstants.CANCEL_LABEL }, 2);
		switch (dialog.open()) {
		case 2: 
			accepted = false; 
			break;
		case 0:
			(new QtProject((IProject)getElement())).scheduleRebuild();
			accepted = true;
			break;
		case 1: 
			accepted = true;
			break;
		}
		return accepted;
	}
}