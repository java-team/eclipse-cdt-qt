/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject.utils.tests;

import org.eclipse.core.runtime.Path;

import com.trolltech.qtcppproject.utils.QtUtils;

import junit.framework.TestCase;

public class QtUtilsTest extends TestCase {

	public void testRemoveFileName() {
		assertNull(QtUtils.removeFileName(null));
		assertEquals(new Path("../bla/./"), 
				QtUtils.removeFileName(new Path("../bla/./someFile.test")));
		assertEquals(new Path("/blue/../"), 
				QtUtils.removeFileName(new Path("/blue/../other.h")));		
		assertEquals(new Path("/bla/blue/"), 
				QtUtils.removeFileName(new Path("/bla/blue/something")));		
		assertEquals(new Path("/bla/blue/blubb"), 
				QtUtils.removeFileName(new Path("/bla/blue/blubb/")));		
	}

	public void testGetFileName() {
		assertNull(QtUtils.getFileName(null, false));
		assertEquals("someFile.test", 
				QtUtils.getFileName(new Path("../bla/./someFile.test"), false));
		assertEquals("someFile", 
				QtUtils.getFileName(new Path("../bla/./someFile.test"), true));
		assertEquals("other.h", 
				QtUtils.getFileName(new Path("/blue/../other.h"), false));		
		assertEquals("other", 
				QtUtils.getFileName(new Path("/blue/../other.h"), true));		
		assertEquals("something", 
				QtUtils.getFileName(new Path("/bla/blue/something"), false));		
		assertEquals("something", 
				QtUtils.getFileName(new Path("/bla/blue/something"), true));		
		assertEquals("", 
				QtUtils.getFileName(new Path("/bla/blue/blubb/"), false));		
		assertEquals("", 
				QtUtils.getFileName(new Path("/bla/blue/blubb/"), true));		
	}

	public void testGetCanonicalPath() {
		assertEquals(new Path("bla/someFile.test"),
				QtUtils.getCanonicalPath(new Path("./bla/someFile.test")));
		assertEquals(new Path("../bla/someFile.test"),
				QtUtils.getCanonicalPath(new Path("../bla/./someFile.test")));
		assertEquals(new Path("/test/other.h"),
				QtUtils.getCanonicalPath(new Path("/blue/../test/other.h")));
		assertEquals(new Path("test/"),
				QtUtils.getCanonicalPath(new Path("blue/../some/./../test/")));
	}

	public void testMakeRelative() {
		assertEquals(new Path("file1.txt"),
				QtUtils.makeRelative(new Path("file1.txt"), new Path("file2.txt")));
		assertEquals(new Path("../file1.txt"),
				QtUtils.makeRelative(new Path("file1.txt"), new Path("somedir/file2.txt")));
		assertEquals(new Path("file1.txt"),
				QtUtils.makeRelative(new Path("file1.txt"), new Path("./file2.txt")));
		assertEquals(new Path("../../otherdir/file1.txt"),
				QtUtils.makeRelative(new Path("dir1/dir2/otherdir/file1.txt"), new Path("dir1/dir2/dir3/dir4/file2.txt")));
		assertEquals(new Path("../file1.txt"),
				QtUtils.makeRelative(new Path("/common/./../common/file1.txt"), new Path("/common/other/./file2.txt")));
	}

}
