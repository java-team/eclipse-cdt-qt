/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject.utils.tests;

import java.io.File;

import org.eclipse.core.runtime.Path;

import com.trolltech.qtcppproject.utils.QtUtils;

import junit.framework.TestCase;

public class QtDirUtilsTest extends TestCase {
	private String tempBasePath;
	private String qtPath;
	private String binPathInQtPath;
	private String qmakeInBinPathInQtPath;
	private String qmakePathInQtPath;
	private String includePathInQtPath;
	private String qtCoreInIncludePathInQtPath;
	private static final String STANDALONEBINPATHPARENT = "somepathwithbin";
	private String standaloneBinPathParent;
	private String standaloneBinPath;
	private String qmakeInStandaloneBinPath;
	private static final String STANDALONEINCLUDEPATHPARENT = "somepathwithinclude";
	private String standaloneIncludePathParent;
	private String standaloneIncludePath;
	private String qtCoreInStandaloneIncludePath;
	
	protected void setUp() throws Exception {
		super.setUp();
		File tempFile = File.createTempFile("qtdirtest", "");
		tempBasePath = tempFile.getAbsolutePath();
		tempFile.delete();
		tempFile.mkdir();

		qtPath = new Path(tempBasePath).append("qt").toOSString();
		new File(qtPath).mkdir();
		
		binPathInQtPath = new Path(qtPath).append("bin").toOSString();
		new File(binPathInQtPath).mkdir();
		qmakeInBinPathInQtPath = new Path(binPathInQtPath).append(QtUtils.getQmakeExecutableName()).toOSString();
		new File(qmakeInBinPathInQtPath).createNewFile();

		includePathInQtPath = new Path(qtPath).append("include").toOSString();
		new File(includePathInQtPath).mkdir();
		qtCoreInIncludePathInQtPath = new Path(includePathInQtPath).append("QtCore").toOSString();
		new File(qtCoreInIncludePathInQtPath).mkdir();
		
		qmakePathInQtPath = new Path(qtPath).append("qmake").toOSString();
		new File(qmakePathInQtPath).mkdir();
		
		standaloneBinPathParent = new Path(tempBasePath).append(STANDALONEBINPATHPARENT).toOSString();
		new File(standaloneBinPathParent).mkdir();
		standaloneBinPath = new Path(standaloneBinPathParent).append("bin").toOSString();
		new File(standaloneBinPath).mkdir();
		qmakeInStandaloneBinPath = new Path(standaloneBinPath).append(QtUtils.getQmakeExecutableName()).toOSString();
		new File(qmakeInStandaloneBinPath).createNewFile();

		standaloneIncludePathParent = new Path(tempBasePath).append(STANDALONEINCLUDEPATHPARENT).toOSString();
		new File(standaloneIncludePathParent).mkdir();
		standaloneIncludePath = new Path(standaloneIncludePathParent).append("include").toOSString();
		new File(standaloneIncludePath).mkdir();
		qtCoreInStandaloneIncludePath = new Path(standaloneIncludePath).append("QtCore").toOSString();
		new File(qtCoreInStandaloneIncludePath).mkdir();
	}

	protected void tearDown() throws Exception {
 		new File(qmakePathInQtPath).delete();
 		new File(qmakeInBinPathInQtPath).delete();
		new File(binPathInQtPath).delete();
		new File(qtCoreInIncludePathInQtPath).delete();
		new File(includePathInQtPath).delete();
		new File(qtPath).delete();
		new File(qmakeInStandaloneBinPath).delete();
		new File(standaloneBinPath).delete();
		new File(standaloneBinPathParent).delete();
		new File(qtCoreInStandaloneIncludePath).delete();
		new File(standaloneIncludePath).delete();
		new File(standaloneIncludePathParent).delete();
		new File(tempBasePath).delete();
		super.tearDown();
	}

	public void testIsValidQtPath() {
		assertTrue(QtUtils.isValidQtPath(binPathInQtPath, QtUtils.QT_PATH_TYPE_BIN));
		assertTrue(QtUtils.isValidQtPath(standaloneBinPath, QtUtils.QT_PATH_TYPE_BIN));
		assertFalse(QtUtils.isValidQtPath(qtPath, QtUtils.QT_PATH_TYPE_BIN));
		assertFalse(QtUtils.isValidQtPath("/some/non/existing/qt/path/bin", QtUtils.QT_PATH_TYPE_BIN));

		assertTrue(QtUtils.isValidQtPath(includePathInQtPath, QtUtils.QT_PATH_TYPE_INCLUDE));
		assertTrue(QtUtils.isValidQtPath(standaloneIncludePath, QtUtils.QT_PATH_TYPE_INCLUDE));
		assertFalse(QtUtils.isValidQtPath(qtPath, QtUtils.QT_PATH_TYPE_INCLUDE));
		assertFalse(QtUtils.isValidQtPath("/some/non/existing/qt/path/include", QtUtils.QT_PATH_TYPE_INCLUDE));
	}
	
	public void testGetSiblingQtPath() {
		assertEquals(includePathInQtPath,
				QtUtils.getSiblingQtPath(binPathInQtPath, QtUtils.QT_PATH_TYPE_BIN, QtUtils.QT_PATH_TYPE_INCLUDE));
		assertEquals("",
				QtUtils.getSiblingQtPath(standaloneBinPath, QtUtils.QT_PATH_TYPE_BIN, QtUtils.QT_PATH_TYPE_INCLUDE));

		assertEquals(binPathInQtPath,
				QtUtils.getSiblingQtPath(includePathInQtPath, QtUtils.QT_PATH_TYPE_INCLUDE, QtUtils.QT_PATH_TYPE_BIN));
		assertEquals("",
				QtUtils.getSiblingQtPath(standaloneIncludePath, QtUtils.QT_PATH_TYPE_INCLUDE, QtUtils.QT_PATH_TYPE_BIN));
	}
	
	public void testGetQtSubPathUnderQtPath() {
		assertEquals(binPathInQtPath, QtUtils.getQtSubPathUnderQtPath(qtPath, QtUtils.QT_PATH_TYPE_BIN));
		assertEquals("", QtUtils.getQtSubPathUnderQtPath("/some/non/existing/qt/path", QtUtils.QT_PATH_TYPE_BIN));

		assertEquals(includePathInQtPath, QtUtils.getQtSubPathUnderQtPath(qtPath, QtUtils.QT_PATH_TYPE_INCLUDE));
		assertEquals("", QtUtils.getQtSubPathUnderQtPath("/some/non/existing/qt/path", QtUtils.QT_PATH_TYPE_INCLUDE));
	}
}
