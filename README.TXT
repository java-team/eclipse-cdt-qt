Build Instructions for the Qt/C++ Eclipse Integration 1.6.1 Source Package
==========================================================================
This package contains the sources for the Qt/C++ Eclipse Integration with
the necessary build files.
There is a build script provided (bash).

Prerequisites
-------------
Java Development Kit 1.5 or later (http://java.sun.com)
Eclipse 3.3.2 or later (http://www.eclipse.org)
CDT 4.0 or later (http://www.eclipse.org/cdt)
Qt 4.5.3 (http://www.qtsoftware.com)

For the build script you also need
Ant (http://ant.apache.org)

Preparations
------------
Qt must be built with glib support (look for glib in QTDIR/lib/pkgconfig/QtGui.pc).
Gtk development headers must be installed.

The build script uses the following environment variables:
JAVADIR     must point to the JDK's root directory
ECLIPSEDIR  must point to the directory where Eclipse's "plugins/" directory
            is located
QTDIR       must point to Qt's root directory

Additionally "ant" must be on the PATH.

Building
--------
Unzip the source package and move to the created directory.
Call "./build.sh"

Installing
----------
The build script creates an "eclipse/" directory in the folder where the
Qt Eclipse Integration's source code lies. This directory contains a
"plugins/" and "features/" subdirectory with the generated files.
To install it, just copy their contents to the respective directories
of you Eclipse installation (remember to remove previous installs of the
Qt/C++ Eclipse Integration first), and start Eclipse with the "-clean"
argument to ensure that Eclipse looks for new installed plugins.
Example:

cp -R eclipse/* /usr/local/eclipse/
eclipse -clean

FAQ
---
Q) Building and installing the Qt Eclipse Integration worked,
   but when I open e.g. a ".ui" file or a ".pro" file in an editor
   all I see is an empty tab where the editor should show.
A) This happens if Qt is configured without glib support. Make sure that
   glib support is compiled into your Qt installation (if you build
   Qt yourself, install the glib development headers before configuring
   Qt).
