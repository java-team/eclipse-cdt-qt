TARGET	 = qtcppproparser

CHECKARCH = $$(ECLIPSEARCH)
linux*:isEmpty(CHECKARCH) {
    error("ECLIPSEARCH environment variable is not set! Please set it to the osgi arch, like x86, x86_64");
}

win32 {
    PLUGINDIR = ../../../com.trolltech.qtcppproject.win32.x86
} else:macx {
    PLUGINDIR = ../../../com.trolltech.qtcppproject.macosx
    DESTDIR = $${PLUGINDIR}/os/macosx
} else:linux* {
    PLUGINDIR = ../../../com.trolltech.qtcppproject.linux.$$(ECLIPSEARCH)
    DESTDIR = $${PLUGINDIR}/lib
}

include(../../shared/common.pri)
include(../proparser.pri)
include(qtcppproparser_inc.pri)

# copy the .java files
JAVAFILES_SRC = java/*.java
JAVAFILES_DEST = $${PLUGINDIR}/src/com/trolltech/qtcppproject/pages/embedded

win32 {
    JAVAFILES_SRC ~= s|/|\|
    JAVAFILES_DEST ~= s|/|\|
}

QMAKE_POST_LINK += $${QMAKE_COPY} $${JAVAFILES_SRC} $${JAVAFILES_DEST}
