/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppproject.pages.embedded;

import java.io.IOException;
import java.util.*;
import org.eclipse.swt.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.events.*;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;

public class ScopeList extends Composite
{
    long handleWidget;
    static Hashtable table = new Hashtable();
    static {
        try {
            System.loadLibrary ("qtcppproparser");
        } catch (UnsatisfiedLinkError ex) { ex.printStackTrace(); }
    }

    public ScopeList(Composite parent, int style)
    {
        super (parent, style);
        handleWidget = createControl(handle, embeddedHandle);
        if (handleWidget == 0) SWT.error (SWT.ERROR_NO_HANDLES);
        table.put(new Long(handleWidget), this);
        
        setFont(getFont());
    
        addDisposeListener(new DisposeListener() {
            public void widgetDisposed(DisposeEvent e) {
                ScopeList.this.widgetDisposed(e);
            }
        });

        addControlListener(new ControlAdapter() {
            public void controlResized(ControlEvent e) {
                ScopeList.this.controlResized(e);
            }
        });

    }

    public void widgetDisposed (DisposeEvent e)
    {
        table.remove(new Long(handleWidget));
        disposeControl(handleWidget);
        handleWidget = 0;
    }
  
    public void controlResized(ControlEvent e)
    {
        Rectangle rect = getClientArea();
        resizeControl(handleWidget, rect.x, rect.y, rect.width, rect.height);
    }

    public Point computeSize(int wHint, int hHint, boolean changed)
    {
        checkWidget();
        int [] result = new int[2];
        computeSize(handleWidget, result);
        if (wHint != SWT.DEFAULT) result[0] = wHint;
        if (hHint != SWT.DEFAULT) result[1] = hHint;
        int border = getBorderWidth();
        return new Point(result[0] + border * 2, result[1] + border * 2);
    }
    
    public void setFont(Font font)
    {
        super.setFont(font);
        
        if (font == null)
            return;
        FontData[] fntlist = font.getFontData();
        setFont (handleWidget, fntlist[0].getName(), fntlist[0].getHeight());
    }
    
    public void showModel(String proFileName, String contents, boolean enabled)
    {
        checkWidget();
        showModel(handleWidget, proFileName, contents, enabled);
    }

    public void selectFirstVariable()
    {
        checkWidget();
        selectFirstVariable(handleWidget);
    }

    public boolean search(String proFileName, String contents)
    {
        checkWidget();
        return search(handleWidget, proFileName, contents);
    }

    public boolean isChanged(String proFileName)
    {
        checkWidget();
        return isChanged(handleWidget, proFileName);
    }

    public String removeFiles(String proFileName)
    {
        checkWidget();
        return removeFiles(handleWidget, proFileName);
    }

    public String addFiles(String proFileName)
    {
        checkWidget();
        return addFiles(handleWidget, proFileName);
    }

    public void addFile(String file, String var)
    {
        checkWidget();
        addFile(handleWidget, file, var);
    }

    static final native long createControl(long phandle, long socketWin);
    static final native void computeSize(long handle, int [] result);
    static final native void disposeControl(long handle);
    static final native void resizeControl(long handle, int x, int y, int width, int height);
    static final native void setFont(long handle, String family, int size);
    static final native void showModel(long handle, String proFileName, String contents, boolean enabled);
    static final native void selectFirstVariable(long handle);
    static final native boolean search(long handle, String proFileName, String contents);
    static final native boolean isChanged(long handle, String proFileName);
    static final native String removeFiles(long handle, String proFileName);
    static final native String addFiles(long handle, String proFileName);
    static final native void addFile(long handle, String file, String var);
}
