/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

#include <QObject>
#include <jni.h>
#include <pthread.h>

#ifndef QTPROPARSER_H
#define QTPROPARSER_H

extern "C" {
// ------ ScopeList ------
JNIEXPORT jlong JNICALL Java_com_trolltech_qtcppproject_pages_embedded_ScopeList_createControl(JNIEnv *, jclass, jlong, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppproject_pages_embedded_ScopeList_computeSize(JNIEnv *, jclass, jlong, jintArray);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppproject_pages_embedded_ScopeList_resizeControl(JNIEnv *, jclass, jlong, jint, jint, jint, jint);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppproject_pages_embedded_ScopeList_disposeControl(JNIEnv *, jclass, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppproject_pages_embedded_ScopeList_setFont(JNIEnv *, jclass, jlong, jstring, jint);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppproject_pages_embedded_ScopeList_showModel(JNIEnv *, jclass, jlong, jstring, jstring, jboolean);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppproject_pages_embedded_ScopeList_selectFirstVariable(JNIEnv *, jclass, jlong);
JNIEXPORT jboolean JNICALL Java_com_trolltech_qtcppproject_pages_embedded_ScopeList_search(JNIEnv *, jclass, jlong, jstring, jstring);
JNIEXPORT jboolean JNICALL Java_com_trolltech_qtcppproject_pages_embedded_ScopeList_isChanged(JNIEnv *, jclass, jlong, jstring);
JNIEXPORT jstring JNICALL Java_com_trolltech_qtcppproject_pages_embedded_ScopeList_removeFiles(JNIEnv *, jclass, jlong, jstring);
JNIEXPORT jstring JNICALL Java_com_trolltech_qtcppproject_pages_embedded_ScopeList_addFiles(JNIEnv *, jclass, jlong, jstring);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppproject_pages_embedded_ScopeList_addFile(JNIEnv *, jclass, jlong, jstring, jstring);
}
#endif //QTPROPARSER_H
