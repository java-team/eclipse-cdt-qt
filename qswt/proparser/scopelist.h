/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

#ifndef SCOPELIST_H
#define SCOPELIST_H

#include <QtCore/QSet>
#include <QtCore/QFileInfo>
#include <QtGui/QTreeView>

#include "proeditormodel.h"

namespace Qt4ProjectManager {
    namespace Internal {
        class ProScopeFilter;
        class ProEditorModel;
    }
}

class ScopeList : public QTreeView
{
    Q_OBJECT
    Q_CLASSINFO("ClassID", "{2B36A512-E024-46E3-A24B-79FDC6A1D9D3}")
    Q_CLASSINFO("InterfaceID", "{8A4F6357-A727-4A04-878A-67CC4AECA48A}")
    Q_CLASSINFO("EventsID", "{40140114-B20B-45BB-8F7E-2A6A1C551568}")
    Q_CLASSINFO("ToSuperClass", "ScopeList")

public:
    ScopeList(QWidget *parent = 0);
    ~ScopeList();

public slots:
    // displays the model and the selected variable
    void showModel(const QString &proFileName, const QString &contents, bool enabled);

    // selects the first variable in the current model
    void selectFirstVariable();

    // searches for the files (which were added before with addFile) in the model
    // creates a filter model and checks the values
    // returns true if files are found
    bool search(const QString &proFileName, const QString &contents);

    // check if a model is changed
    bool isChanged(const QString &proFileName);

    // removes the selected files
    QString removeFiles(const QString &proFileName);

    // adds the files to the specified variable
    QString addFiles(const QString &proFileName);

    // adds a file to the list of files to add or remove from
    // the specified variable
    void addFile(const QString &file, const QString &var);

private:

    static QMap<QString, Qt4ProjectManager::Internal::ProEditorModel *> &modelMap();
    static QMap<Qt4ProjectManager::Internal::ProEditorModel *, int> &modelRefCount();

    static Qt4ProjectManager::Internal::ProEditorModel *aquireModel(const QString &fileName, const QString &contents);
    static void releaseModel(Qt4ProjectManager::Internal::ProEditorModel *editorModel);

    Qt4ProjectManager::Internal::ProScopeFilter *filter(Qt4ProjectManager::Internal::ProEditorModel *model);
    void ensureVariable(const QString &variable, Qt4ProjectManager::Internal::ProEditorModel *model);
    Qt4ProjectManager::Internal::ProEditorModel *proEditorModel(const QString &fileName, const QString &contents);

    QMap<Qt4ProjectManager::Internal::ProEditorModel *,
        Qt4ProjectManager::Internal::ProScopeFilter *> m_models;
    QMap<QString, Qt4ProjectManager::Internal::ProEditorModel *> m_proFileNameToModel;

    QMultiMap<QString, QFileInfo> m_files;
    QStringList m_filenames;
};
    
#endif //SCOPELIST_H
