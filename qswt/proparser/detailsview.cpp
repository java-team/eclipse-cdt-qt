/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

#include "detailsview.h"
#include "proeditormodel.h"
#include "procommandmanager.h"

using namespace Qt4ProjectManager::Internal;

DetailsView::DetailsView(QWidget *parent)
    : Qt4ProjectManager::Internal::ProEditor(parent, false)
{
}

void DetailsView::initialize(ProEditorModel *model,
                             ProItemInfoManager *infomanager)
{
    ProEditor::initialize(model, infomanager);

    m_actions.insert(CUT_ACTION, m_cutAction);
    m_actions.insert(COPY_ACTION, m_copyAction);
    m_actions.insert(PASTE_ACTION, m_pasteAction);

    connect(m_cutAction, SIGNAL(changed()),
        this, SLOT(actionChanged()));

    connect(m_copyAction, SIGNAL(changed()),
        this, SLOT(actionChanged()));

    connect(m_pasteAction, SIGNAL(changed()),
        this, SLOT(actionChanged()));

    QAction *action = new QAction(tr("Undo"), this);
    m_actions.insert(UNDO_ACTION, action);

    connect(action, SIGNAL(changed()),
        this, SLOT(actionChanged()));

    connect(action, SIGNAL(triggered()),
        m_model->cmdManager(), SLOT(undo()));

    action = new QAction(tr("Redo"), this);
    m_actions.insert(REDO_ACTION, action);

    connect(action, SIGNAL(changed()),
        this, SLOT(actionChanged()));

    connect(action, SIGNAL(triggered()),
        m_model->cmdManager(), SLOT(redo()));

    connect(m_model->cmdManager(), SIGNAL(modified()),
        this, SLOT(commandManagerChanged()));

    commandManagerChanged();
}

DetailsView::~DetailsView()
{

}

bool DetailsView::isActionEnabled(int id)
{
    if (m_actions.count() -1 < id)
        return false;
    return m_actions.at(id)->isEnabled();
}

void DetailsView::triggerAction(int id)
{
    if (m_actions.count() -1 < id)
        return;
    m_actions.at(id)->trigger();
}

void DetailsView::actionChanged()
{
    emit actionChanged(m_actions.indexOf(qobject_cast<QAction*>(sender())));
}

void DetailsView::commandManagerChanged()
{
    m_actions.at(UNDO_ACTION)->setEnabled(m_model->cmdManager()->canUndo());
    m_actions.at(REDO_ACTION)->setEnabled(m_model->cmdManager()->canRedo());
}
