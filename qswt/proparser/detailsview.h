/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

#ifndef DETAILSVIEW_H
#define DETAILSVIEW_H

#include "proeditor.h"

class DetailsView : public Qt4ProjectManager::Internal::ProEditor
{
    Q_OBJECT

    enum {
        CUT_ACTION      = 0,
        COPY_ACTION     = 1,
        PASTE_ACTION    = 2,
        UNDO_ACTION     = 3,
        REDO_ACTION     = 4
    };

public:
    DetailsView(QWidget *parent = 0);
    ~DetailsView();

    void initialize(Qt4ProjectManager::Internal::ProEditorModel *model,
                    Qt4ProjectManager::Internal::ProItemInfoManager *infomanager);

    bool isActionEnabled(int id);
    void triggerAction(int id);

private slots:
    void actionChanged();
    void commandManagerChanged();

signals:
    void actionChanged(int id);

private:
    QList<QAction *> m_actions;
};

#endif //DETAILSVIEW_H
