/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

#include "qswt.h"

#include "scopelist.h"

QSWT_MAIN_BEGIN("qtcppproparser", "com.trolltech.qtcppproject.pages.embedded",
    "{414BB33D-822C-4685-B3BC-1E70979934F8}", "{201EDEF9-2FE7-4B63-8939-51A327D41582}")
    QSWT_CLASS(ScopeList, "scopelist.h", "scopelist.cpp")
QSWT_MAIN_END()
