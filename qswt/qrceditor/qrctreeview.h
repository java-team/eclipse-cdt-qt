/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

#ifndef QRCTREEVIEW_H
#define QRCTREEVIEW_H

#include "qrceditor.h"

#include <QtCore/QObject>

class QrcTreeView : public QWidget
{
    Q_OBJECT
    Q_CLASSINFO("ClassID", "{D4D8260E-5461-49D8-86B5-E52CAA7BAAAD}")
    Q_CLASSINFO("InterfaceID", "{650D2DFB-6535-47AE-83D9-F4E7A6C0AAAE}")
    Q_CLASSINFO("EventsID", "{F563CFD4-893B-473F-B069-5052F462AAA9}")
    Q_CLASSINFO("ToSuperClass", "QrcTreeView")

public:
    QrcTreeView(QWidget *parent = 0);
    ~QrcTreeView();

public slots:
    bool load(const QString &file);
    bool save();

    bool isDirty() const;

    QString contents() const;

signals:
    void dirtyChanged();

private:
    SharedTools::QrcEditor *m_editor;
};

#endif //EXPLORERVIEW_H
