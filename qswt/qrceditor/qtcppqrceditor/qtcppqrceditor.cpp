/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

#include "qrctreeview.h"
#include <pthread.h>
#include <stdio.h>
#include <QX11EmbedWidget>
#include <QVBoxLayout>
#include <QApplication>
#include <QClipboard>
#include <QVariant>
#include <QPalette>
#include <QColor>
#include <QFont>
#include <gdk/gdkx.h>
#include <gtk/gtkstyle.h>
#include <gtk/gtkwidget.h>
#include <gtk/gtkmain.h>

#include "qtcppqrceditor.h"

static pthread_key_t *envKey = 0;

// ------ QrcTreeView ------
JNIEXPORT jlong JNICALL Java_com_trolltech_qtcppproject_pages_embedded_QrcTreeView_createControl(JNIEnv *env, jclass that, jlong parent, jlong socketWin)
{
    if (!QApplication::instance())
    {
        Display *xdisp = XOpenDisplay(0);
        (void)new QApplication(xdisp);
        #if QT_VERSION >= 0x040400
        qApp->setAttribute(Qt::AA_NativeWindows);
        #endif
        qApp->clipboard()->setProperty("useEventLoopWhenWaiting", QVariant(true));
        GtkStyle *style = gtk_widget_get_style((GtkWidget *)parent);
        GdkColor col = style->bg[GTK_STATE_NORMAL];
        QPalette qtpal(QColor(col.red>>8, col.green>>8, col.blue>>8));
        col = style->text[GTK_STATE_SELECTED];
        qtpal.setColor(QPalette::Normal, QPalette::HighlightedText,
            QColor(col.red>>8, col.green>>8, col.blue>>8));
        col = style->base[GTK_STATE_SELECTED];
        qtpal.setColor(QPalette::Normal, QPalette::Highlight,
        QColor(col.red>>8, col.green>>8, col.blue>>8));
        QApplication::setPalette(qtpal);
        QFont::insertSubstitution("Sans", "Sans Serif");
    }
    if (!envKey) {
        envKey = new pthread_key_t;
        pthread_key_create(envKey, NULL);
    }
    pthread_setspecific(*envKey, env);
    
    QrcTreeView *obj = new QrcTreeView();

    // the first client needs to be the parent of all the other clients.
    // else focus will not work, since the other clients never becomes the
    // active window in the application. 
    QX11EmbedWidget *client = new QX11EmbedWidget();
    
    // we need to create obj first in order for the eventfilters in xembed
    // to be called first.
    obj->setParent(client);
    
    QVBoxLayout *vblayout = new QVBoxLayout(client);
    vblayout->setMargin(0);
    
    QrcTreeViewListener *lstnr = new QrcTreeViewListener(obj, envKey, that);
    QObject::connect(obj, SIGNAL(dirtyChanged()), lstnr, SLOT(slot_dirtyChanged()));
    vblayout->addWidget(obj);

    client->embedInto(socketWin);
    client->show();
    return (jlong)obj;
}

JNIEXPORT void JNICALL Java_com_trolltech_qtcppproject_pages_embedded_QrcTreeView_computeSize(JNIEnv *env, jclass that, jlong handle, jintArray result)
{
    QrcTreeView *obj = (QrcTreeView*)handle;
    jint *nresult = NULL;
    nresult = env->GetIntArrayElements(result, NULL);
    
    nresult[0] = obj->parentWidget()->sizeHint().width();
    nresult[1] = obj->parentWidget()->sizeHint().height();
    
    env->ReleaseIntArrayElements(result, nresult, 0);
    
    Q_UNUSED(env);
    Q_UNUSED(that);
}

JNIEXPORT void JNICALL Java_com_trolltech_qtcppproject_pages_embedded_QrcTreeView_resizeControl(JNIEnv *env, jclass that, jlong handle, jint x, jint y, jint width, jint height)
{
    QrcTreeView *obj = (QrcTreeView*)handle;
    obj->parentWidget()->resize(width, height);
    obj->parentWidget()->move(x, y);
    
    Q_UNUSED(env);
    Q_UNUSED(that);
}

JNIEXPORT void JNICALL Java_com_trolltech_qtcppproject_pages_embedded_QrcTreeView_disposeControl(JNIEnv *env, jclass that, jlong handle)
{
    QrcTreeView *obj = (QrcTreeView*)handle;
    QWidget *parentW = obj->topLevelWidget();
    delete parentW;
    Q_UNUSED(env);
    Q_UNUSED(that);
}

JNIEXPORT void JNICALL Java_com_trolltech_qtcppproject_pages_embedded_QrcTreeView_setFont(JNIEnv *env, jclass that, jlong handle, jstring jni_family, jint size)
{
    QrcTreeView *obj = (QrcTreeView*)handle;
    const char *utf_family = env->GetStringUTFChars(jni_family, 0);
    QString family = QString::fromUtf8(utf_family);
    env->ReleaseStringUTFChars(jni_family, utf_family);
    
    obj->setFont(QFont(family, size));
    
    Q_UNUSED(that);
}

JNIEXPORT jboolean JNICALL Java_com_trolltech_qtcppproject_pages_embedded_QrcTreeView_load(JNIEnv *env, jclass that, jlong handle, jstring jni_file)
{
    QrcTreeView *obj = (QrcTreeView*)handle;
    Q_UNUSED(that);
    const char *utf_file = env->GetStringUTFChars(jni_file, 0);
    QString file = QString::fromUtf8(utf_file);
    env->ReleaseStringUTFChars(jni_file, utf_file);
    bool res = obj->load(file);
    Q_UNUSED(env);
    return (jboolean)res;
}

JNIEXPORT jboolean JNICALL Java_com_trolltech_qtcppproject_pages_embedded_QrcTreeView_save(JNIEnv *env, jclass that, jlong handle)
{
    QrcTreeView *obj = (QrcTreeView*)handle;
    Q_UNUSED(that);
    bool res = obj->save();
    Q_UNUSED(env);
    return (jboolean)res;
}

JNIEXPORT jboolean JNICALL Java_com_trolltech_qtcppproject_pages_embedded_QrcTreeView_isDirty(JNIEnv *env, jclass that, jlong handle)
{
    QrcTreeView *obj = (QrcTreeView*)handle;
    Q_UNUSED(that);
    bool res = obj->isDirty();
    Q_UNUSED(env);
    return (jboolean)res;
}

JNIEXPORT jstring JNICALL Java_com_trolltech_qtcppproject_pages_embedded_QrcTreeView_contents(JNIEnv *env, jclass that, jlong handle)
{
    QrcTreeView *obj = (QrcTreeView*)handle;
    Q_UNUSED(that);
    QString res = obj->contents();
    return env->NewStringUTF(res.toUtf8());
}

