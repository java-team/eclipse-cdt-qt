/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

#include <QObject>
#include <jni.h>
#include <pthread.h>

#ifndef QTQRCEDITOR_H
#define QTQRCEDITOR_H

extern "C" {
// ------ QrcTreeView ------
JNIEXPORT jlong JNICALL Java_com_trolltech_qtcppproject_pages_embedded_QrcTreeView_createControl(JNIEnv *, jclass, jlong, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppproject_pages_embedded_QrcTreeView_computeSize(JNIEnv *, jclass, jlong, jintArray);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppproject_pages_embedded_QrcTreeView_resizeControl(JNIEnv *, jclass, jlong, jint, jint, jint, jint);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppproject_pages_embedded_QrcTreeView_disposeControl(JNIEnv *, jclass, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppproject_pages_embedded_QrcTreeView_setFont(JNIEnv *, jclass, jlong, jstring, jint);
JNIEXPORT jboolean JNICALL Java_com_trolltech_qtcppproject_pages_embedded_QrcTreeView_load(JNIEnv *, jclass, jlong, jstring);
JNIEXPORT jboolean JNICALL Java_com_trolltech_qtcppproject_pages_embedded_QrcTreeView_save(JNIEnv *, jclass, jlong);
JNIEXPORT jboolean JNICALL Java_com_trolltech_qtcppproject_pages_embedded_QrcTreeView_isDirty(JNIEnv *, jclass, jlong);
JNIEXPORT jstring JNICALL Java_com_trolltech_qtcppproject_pages_embedded_QrcTreeView_contents(JNIEnv *, jclass, jlong);
}

// ------ QrcTreeView ------
class QrcTreeViewListener : public QObject
{
    Q_OBJECT
public:
    QrcTreeViewListener(QObject *parent, pthread_key_t *key, jclass that)
        : QObject(parent)
    {
        envKey = key;
        JNIEnv *env = (JNIEnv *)pthread_getspecific(*envKey);
        javaClass = (jclass) env->NewGlobalRef((jobject)that);
        jmId[0] = env->GetStaticMethodID(that, "dirtyChanged", "(J)V");
    }

    ~QrcTreeViewListener() { }

public slots:
    void slot_dirtyChanged()
    {
        JNIEnv *env = (JNIEnv *)pthread_getspecific(*envKey);
        if (env == NULL) return;
        env->CallStaticVoidMethod(javaClass, jmId[0], (jlong)parent());
    }

private:
    pthread_key_t *envKey;
    jclass javaClass;
    jmethodID jmId[1];
};

#endif //QTQRCEDITOR_H
