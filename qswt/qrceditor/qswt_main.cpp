/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

#include "qswt.h"

#include "qrctreeview.h"

QSWT_MAIN_BEGIN("qtcppqrceditor", "com.trolltech.qtcppproject.pages.embedded",
    "{3186D076-3FFA-4080-B798-8813E3071679}", "{E585D458-C4CB-4212-80DC-608253CB2781}")
    QSWT_CLASS(QrcTreeView, "qrctreeview.h", "qrctreeview.cpp")
QSWT_MAIN_END()
