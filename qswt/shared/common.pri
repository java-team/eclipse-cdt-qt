macx {
} else:unix {
    VERSION = $${QT_MAJOR_VERSION}.$${QT_MINOR_VERSION}.$${QT_PATCH_VERSION}
    
    contains(DEFINES, QTJAMBI_ECLIPSE_INTEGRATION) {
        QMAKE_LFLAGS += -Wl,-z,origin \
                   \'-Wl,-rpath,\$\$ORIGIN\'
        QMAKE_RPATHDIR = 
    } else {
        QMAKE_LFLAGS += -Wl,-z,origin \
        	     \'-Wl,-rpath,\$\$ORIGIN/../../com.trolltech.qtcpp.linux.$$(ECLIPSEARCH)_$${VERSION}/lib:\$\$ORIGIN/../../com.trolltech.qtcpp.linux.$$(ECLIPSEARCH)/lib\'
        QMAKE_RPATHDIR =
    }
}

# compile on suse 9.3
INCLUDEPATH += /opt/gnome/lib/glib-2.0/include \
    /opt/gnome/lib/gtk-2.0/include \
    /opt/gnome/include/atk-1.0 \
    /opt/gnome/include/glib-2.0 \
    /opt/gnome/include/gail-1.0 \
    /opt/gnome/include/gtk-2.0 \
    /opt/gnome/include/orbit-2.0 \
    /opt/gnome/include/pango-1.0 \
    $$PWD

