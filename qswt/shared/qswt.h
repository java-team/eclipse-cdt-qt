/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

#ifndef QSWT_H
#define QSWT_H

#include <QtGlobal>

#ifdef Q_OS_WIN
#include <QAxFactory>
#define QSWT_MAIN_BEGIN(LibName, Package, axLibId, axAppId) \
    QAXFACTORY_BEGIN(axLibId, axAppId)
#define QSWT_CLASS(Class, Header, Sources) \
    QAXCLASS(Class)
#define QSWT_MAIN_END() \
    QAXFACTORY_END()
#else //Q_OS_WIN
#define QSWT_MAIN_BEGIN(LibName, Package, axLibId, axAppId)
#define QSWT_CLASS(Class, Header, Sources)
#define QSWT_MAIN_END()
#endif //Q_OS_WIN

#endif //QSWT_H
