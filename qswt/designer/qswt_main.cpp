/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

#include "qswt.h"

#include "designerbridgew.h"
#include "widgetboxw.h"
#include "propertyeditorw.h"
#include "objectinspectorw.h"
#include "formwindoww.h"
#include "actioneditorw.h"
#include "signalsloteditorw.h"
#include "resourceeditorw.h"

QSWT_MAIN_BEGIN("qtcppdesigner", "com.trolltech.qtcppdesigner.views.embedded",
    "{3186D076-3FFA-4080-B788-8815E3078679}", "{E585D458-14CB-42A2-80CC-608252CB2781}")
    QSWT_CLASS(DesignerBridgeW, "designerbridgew.h", "designerbridgew.cpp")
    QSWT_CLASS(WidgetBoxW, "widgetboxw.h", "widgetboxw.cpp")
    QSWT_CLASS(PropertyEditorW, "propertyeditorw.h", "propertyeditorw.cpp")
    QSWT_CLASS(ObjectInspectorW, "objectinspectorw.h", "objectinspectorw.cpp")
    QSWT_CLASS(FormWindowW, "formwindoww.h", "formwindoww.cpp")
    QSWT_CLASS(ActionEditorW, "actioneditorw.h", "actioneditorw.cpp")
    QSWT_CLASS(SignalSlotEditorW, "signalsloteditorw.h", "signalsloteditorw.cpp")
    QSWT_CLASS(ResourceEditorW, "resourceeditorw.h", "resourceeditorw.cpp")
QSWT_MAIN_END()
