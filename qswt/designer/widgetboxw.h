/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

#ifndef WIDGETBOXW_H
#define WIDGETBOXW_H

#include <QWidget>

QT_FORWARD_DECLARE_CLASS(QDesignerWidgetBoxInterface)

class WidgetBoxW : public QWidget
{
    Q_OBJECT
    Q_CLASSINFO("ClassID", "{D9D8240E-5461-49D8-86B5-E52CAA7BAAAD}")
    Q_CLASSINFO("InterfaceID", "{660D2DFB-6525-47AE-81D9-F4E7A6C0AAAE}")
    Q_CLASSINFO("EventsID", "{F562CFD4-893B-471F-B068-5052F462AAA9}")
    Q_CLASSINFO("ToSuperClass", "WidgetBoxW")

public:
    WidgetBoxW(QWidget *parent = 0);
    ~WidgetBoxW();

    QSize minimumSize();
    static WidgetBoxW *instance();

public slots:
    QString pluginFailureString() const;
    void updateCustomWidgetLocation(const QString &location);
    void setBoxEnabled(bool state);
    void updateCustomWidgets();
    bool initializeJambiPlugins(const QString &jambiBase, const QString &jambiPluginPath,
        const QString &customWidgetClassPath, const QString &resourcePath, const QString &jvm);
    void initialize();

protected:
    void resizeEvent(QResizeEvent *event);
    bool event(QEvent *event);

private:
    uint m_initialized : 1;
    static WidgetBoxW *m_self;
};

#endif //WIDGETBOXW_H
