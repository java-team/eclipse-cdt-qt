/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

#ifndef OBJECTINSPECTORW_H
#define OBJECTINSPECTORW_H

#include <QWidget>

QT_FORWARD_DECLARE_CLASS(QDesignerObjectInspectorInterface)

class ObjectInspectorW : public QWidget
{
    Q_OBJECT
    Q_CLASSINFO("ClassID", "{D8647F38-ACB0-43BD-9C6A-42EC8898AAA2}")
    Q_CLASSINFO("InterfaceID", "{FD8C3F28-1EC1-41BD-B64B-0CD4EA14AAA8}")
    Q_CLASSINFO("EventsID", "{30740FA1-BE77-4BDD-8F9C-A4A67086AAA4}")
    Q_CLASSINFO("ToSuperClass", "ObjectInspectorW")

public:
    ObjectInspectorW(QWidget *parent = 0);
    ~ObjectInspectorW();

    QSize minimumSize();

    static ObjectInspectorW *instance();

public slots:
    QString pluginFailureString() const;
    bool initializeJambiPlugins(const QString &jambiBase, const QString &jambiPluginPath,
        const QString &customWidgetClassPath, const QString &resourcePath, const QString &jvm);
    void initialize();
    void updateCustomWidgetLocation(const QString &path);


protected:
    void resizeEvent(QResizeEvent *event);

private:
    QDesignerObjectInspectorInterface *m_editor;
    uint m_initialized : 1;
    static ObjectInspectorW *m_self;
};

#endif //OBJECTINSPECTORW_H
