/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

#ifndef ACTIONEDITORW_H
#define ACTIONEDITORW_H

#include <QWidget>

QT_FORWARD_DECLARE_CLASS(QDesignerActionEditorInterface)

class ActionEditorW : public QWidget
{
    Q_OBJECT
    Q_CLASSINFO("ClassID", "{E22724DC-B592-47A2-8ED2-83B690BFAAA0}")
    Q_CLASSINFO("InterfaceID", "{5277C2A2-C326-49E6-A4FC-AEBAA8AFAAA4}")
    Q_CLASSINFO("EventsID", "{85F2AE23-730D-41CE-B98D-F168FCC8AAA5}")
    Q_CLASSINFO("ToSuperClass", "ActionEditorW")

public:
    ActionEditorW(QWidget *parent = 0);
    ~ActionEditorW();

    QSize minimumSize();
    static ActionEditorW *instance();

public slots:
    QString pluginFailureString() const;
    bool initializeJambiPlugins(const QString &jambiBase, const QString &jambiPluginPath,
        const QString &customWidgetClassPath, const QString &resourcePath, const QString &jvm);
    void initialize();
    void updateCustomWidgetLocation(const QString &path);

protected:
    void resizeEvent(QResizeEvent *event);

private:
    QDesignerActionEditorInterface *m_editor;
    uint m_initialized : 1;
    static ActionEditorW *m_self;
};

#endif //ACTIONEDITORW_H
