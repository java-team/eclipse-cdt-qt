/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

#ifndef ECLIPSEINTEGRATION_H
#define ECLIPSEINTEGRATION_H

#include <QtDesigner/private/qdesigner_integration_p.h>

class FormEditorW;

class EclipseIntegration : public qdesigner_internal::QDesignerIntegration {
    Q_OBJECT

public:
    EclipseIntegration(QDesignerFormEditorInterface *core, FormEditorW *parent = 0);

    QWidget *containerWindow(QWidget *widget) const;

public slots:
    void updateSelection();

private:
    FormEditorW *m_formEditorW;
};

#endif // ECLIPSEINTEGRATION_H
