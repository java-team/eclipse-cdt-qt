/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

#ifndef FORMWINDOWW_H
#define FORMWINDOWW_H

#include <QtGui/QWidget>
#include <QtCore/QObject>
#include <QtCore/QPoint>

#include "widgethost.h"

QT_FORWARD_DECLARE_CLASS(QDesignerFormEditorInterface)
QT_FORWARD_DECLARE_CLASS(QDesignerFormWindowInterface)
QT_FORWARD_DECLARE_CLASS(QDesignerFormWindowManagerInterface)
QT_FORWARD_DECLARE_CLASS(QFile)

class FormWindowW : public SharedTools::WidgetHost
{
    Q_OBJECT
    Q_CLASSINFO("ClassID", "{3B33923D-9423-4662-A2D2-35CA6009AAAE}")
    Q_CLASSINFO("InterfaceID", "{147566D3-D7EC-4745-BA9B-C7904FB8AAAF}")
    Q_CLASSINFO("EventsID", "{E4A3B9F5-DEBE-4CD5-83ED-34694204AAAE}")
    Q_CLASSINFO("ToSuperClass", "FormWindowW")

public:
    FormWindowW(QWidget *parent = 0);
    ~FormWindowW();

    void signalChange(int id)
        { emit actionChanged(id); }

    bool eventFilter(QObject *watched, QEvent *e);

    //do nothing, use default always
    void setFont(const QFont &) { }

public slots:
    QString pluginFailureString() const;
    void open(QString fileName);
    bool save();
    bool saveAs(QString fileName);
    bool isDirty();
    void close();
    void initialize();

    void setActiveFormWindow();
    void setObjectName(const QString &objectName);
    bool initializeJambiPlugins(const QString &jambiBase, const QString &jambiPluginPath,
        const QString &customWidgetClassPath, const QString &resourcePath, const QString &jvm);
    void updateJambiResourcePath(const QString &resourcePath);
    void updateCustomWidgetLocation(const QString &path);

    // actions
    int actionCount();
    int staticActionCount();
    QString actionName(int id);
    QString actionToolTip(int id);
    void actionTrigger(int id);
    bool isEnabled(int id);

    // tools
    int toolCount();
    QString toolName(int index);
    QString toolToolTip(int index);
    int currentTool();
    void setCurrentTool(int index);

/*protected:
    void mouseReleaseEvent ( QMouseEvent * e );*/

signals:
    void actionChanged(int id);
    void checkActiveWindow();
    void resourceFilesChanged();
    void updateDirtyFlag();

private slots:
    void formSizeChanged(int w, int h);
    void formSelectionChanged();
    void formChanged();

private:
    bool save(QString fileName);
    QDesignerFormWindowManagerInterface *m_fwm;
    QDesignerFormEditorInterface *m_core;

    uint m_lastDirtyFlag : 1;
    uint m_initialized : 1;
};

#endif //FORMWINDOWW_H
