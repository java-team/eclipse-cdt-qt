/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

#include "propertyeditorw.h"
#include "formeditorw.h"
#include "widgetboxw.h"
#include "abstractpropertyeditor.h"

#include <QResizeEvent>

PropertyEditorW *PropertyEditorW::m_self = 0;

PropertyEditorW::PropertyEditorW(QWidget *parent)
    : QWidget(parent), m_editor(0), m_initialized(false)
{
    if (m_self != 0)
        delete m_self;
    m_self = this;
    FormEditorW::instance()->setPropertyEditor(this);
}

PropertyEditorW::~PropertyEditorW()
{
    m_self = 0;
    FormEditorW::instance()->setPropertyEditor(0);

    if (m_editor != 0) {
        m_editor->hide();
        m_editor->setParent(0);
    }
}

void PropertyEditorW::initialize()
{
    if (!m_initialized) {
        m_initialized = true;
        if (!FormEditorW::instance()->formEditor()->propertyEditor()) {
            m_editor = QDesignerComponents::createPropertyEditor(FormEditorW::instance()->formEditor(), this);
            FormEditorW::instance()->formEditor()->setPropertyEditor(m_editor);
        } else {
            m_editor = FormEditorW::instance()->formEditor()->propertyEditor();
            m_editor->setParent(this);
        }
        // initialize formeditor after the property editor has been created
        // otherwise the connect to it done in the EclipseIntegration will fail
        FormEditorW::instance()->initialize();
        m_editor->setGeometry(geometry());
        m_editor->show();
    }
}

PropertyEditorW *PropertyEditorW::instance()
{
    if (m_self == 0)
        m_self = new PropertyEditorW();
    m_self->initialize();
    return m_self;
}

void PropertyEditorW::updateCustomWidgetLocation(const QString &path)
{
    WidgetBoxW *w = WidgetBoxW::instance();
    if (w != 0) {
        w->updateCustomWidgetLocation(path);
    }
}


void PropertyEditorW::resizeEvent(QResizeEvent *event)
{
    if (m_editor != 0)
        m_editor->resize(event->size());
    QWidget::resizeEvent(event);
}

QSize PropertyEditorW::minimumSize()
{
    if (m_editor != 0)
        return m_editor->minimumSize();
    else
        return QWidget::minimumSize();
}

void PropertyEditorW::clearSheet()
{
    FormEditorW::instance()->formEditor()->formWindowManager()->setActiveFormWindow(0);
}

bool PropertyEditorW::initializeJambiPlugins(const QString &jambiBase, const QString &jambiPluginPath,
                                         const QString &customWidgetClassPath, const QString &resourcePath, const QString &jvm) {
    return FormEditorW::instance()->initializeJambiPlugins(jambiBase, jambiPluginPath, customWidgetClassPath, resourcePath, jvm);
}

QString PropertyEditorW::pluginFailureString() const
{
    return FormEditorW::instance()->pluginFailureString();
}

