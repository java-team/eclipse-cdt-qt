/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

#ifndef RESOURCEEDITORW_H
#define RESOURCEEDITORW_H

#include <QWidget>

class ResourceEditorW : public QWidget
{
    Q_OBJECT
    Q_CLASSINFO("ClassID", "{C5DC3150-BB15-4FF2-90A7-390C6324AAA8}")
    Q_CLASSINFO("InterfaceID", "{771D2E1D-C95D-43B4-83D2-46175DA7AAA5}")
    Q_CLASSINFO("EventsID", "{45F7CC7C-A09F-4767-B879-672E2F4CAAA7}")
    Q_CLASSINFO("ToSuperClass", "ResourceEditorW")

public:
    ResourceEditorW(QWidget *parent = 0);
    ~ResourceEditorW();

    QSize minimumSize();

    static ResourceEditorW *instance();

public slots:
    QString pluginFailureString() const;
    void updateResources(const QString &sourcePaths);
    bool initializeJambiPlugins(const QString &jambiBase, const QString &jambiPluginPath,
        const QString &customWidgetClassPath, const QString &resourcePath, const QString &jvm);
    void initialize();
    void updateCustomWidgetLocation(const QString &path);

protected:
    void resizeEvent(QResizeEvent *event);

private:
    QWidget *m_editor;
    uint m_initialized : 1;
    static ResourceEditorW *m_self;
};

#endif //RESOURCEEDITORW_H
