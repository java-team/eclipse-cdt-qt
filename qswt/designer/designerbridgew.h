/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

#ifndef DESIGNERBRIDGEW_H
#define DESIGNERBRIDGEW_H

#include <QtGui/QWidget>

class DesignerBridgeW : public QWidget
{
    Q_OBJECT
    Q_CLASSINFO("ClassID", "{3C43923A-9323-4672-A4A2-35F96029AAAE}")
    Q_CLASSINFO("InterfaceID", "{147586A3-D9EC-4645-BC9B-C1F04F08AAAF}")
    Q_CLASSINFO("EventsID", "{E4A3C2F5-A0BE-43A5-D39D-34993202AAAE}")
public:
    DesignerBridgeW(QWidget *parent = 0);


public slots:
    QString pluginFailureString() const;
    void setPluginPath(const QString &pluginPath);
    bool initializeJambiPlugins(const QString &jambiBase, const QString &jambiPluginPath,
        const QString &customWidgetClassPath, const QString &resourcePath, const QString &jvm);
    void initialize();
    void updateCustomWidgetLocation(const QString &path);

private:
    bool m_initialized;
};

#endif //DESIGNERBRIDGEW_H
