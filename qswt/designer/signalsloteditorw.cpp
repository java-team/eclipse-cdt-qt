/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

#include "signalsloteditorw.h"
#include "formeditorw.h"
#include "widgetboxw.h"

#include <QResizeEvent>
#include <QtCore/QDebug>

SignalSlotEditorW *SignalSlotEditorW::m_self = 0;

SignalSlotEditorW::SignalSlotEditorW(QWidget *parent)
    : QWidget(parent), m_editor(0), m_initialized(false)
{
    if (m_self != 0)
        delete m_self;
    m_self = this;
    FormEditorW::instance()->setSignalSlotEditor(this);
}

SignalSlotEditorW::~SignalSlotEditorW()
{
    m_self = 0;
    FormEditorW::instance()->setSignalSlotEditor(0);

    if (m_editor != 0) {
        m_editor->hide();
        m_editor->setParent(0);
    }
}


void SignalSlotEditorW::initialize()
{
    if (!m_initialized) {
        m_initialized = true;
        FormEditorW::instance()->initialize();

        if (!FormEditorW::instance()->signalSlotEditor()) {
            m_editor = QDesignerComponents::createSignalSlotEditor(FormEditorW::instance()->formEditor(), this);
            FormEditorW::instance()->setDesignerSignalSlotEditor(m_editor);
        } else {
            m_editor = FormEditorW::instance()->signalSlotEditor();
            m_editor->setParent(this);
        }
        m_editor->setGeometry(geometry());
        m_editor->show();
    }
}

SignalSlotEditorW *SignalSlotEditorW::instance()
{
    if (m_self == 0)
        m_self = new SignalSlotEditorW();
    m_self->initialize();

    return m_self;
}

void SignalSlotEditorW::updateCustomWidgetLocation(const QString &path)
{
    WidgetBoxW *w = WidgetBoxW::instance();
    if (w != 0) {
        w->updateCustomWidgetLocation(path);
    }
}

bool SignalSlotEditorW::initializeJambiPlugins(const QString &jambiBase, const QString &jambiPluginPath,
                                         const QString &customWidgetClassPath, const QString &resourcePath, const QString &jvm) {
    return FormEditorW::instance()->initializeJambiPlugins(jambiBase, jambiPluginPath, customWidgetClassPath, resourcePath, jvm);
}


void SignalSlotEditorW::resizeEvent(QResizeEvent *event)
{
    if (m_editor != 0)
        m_editor->resize(event->size());
    QWidget::resizeEvent(event);
}

QSize SignalSlotEditorW::minimumSize()
{
    if (m_editor != 0)
        return m_editor->minimumSize();
    else
        return QWidget::minimumSize();
}

QString SignalSlotEditorW::pluginFailureString() const
{
    return FormEditorW::instance()->pluginFailureString();
}

