/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppdesigner.views.embedded;

import java.io.IOException;
import java.util.*;
import org.eclipse.swt.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.events.*;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;

public class SignalSlotEditorW extends Composite
    implements com.trolltech.qtcppcommon.JambiCompatible
{
    long handleWidget;
    static Hashtable table = new Hashtable();
    public SignalSlotEditorW(Composite parent, int style)
    {
        super (parent, style);
        try {
            org.osgi.framework.Bundle bundle = org.eclipse.core.runtime.Platform.getBundle("com.trolltech.qtcppdesigner");
            if (bundle != null) {
                Class cls = bundle.loadClass("com.trolltech.qtcppdesigner.qtcppjambi.QtJambiConnection");
                java.lang.reflect.Method m = cls.getMethod("validateJambi", new Class[] { com.trolltech.qtcppcommon.JambiCompatible.class });
                m.invoke(null, new Object[] { this });
            }
        } catch (Throwable t) {
            Bundle pluginBundle = Platform.getBundle("com.trolltech.qtcppdesigner."+Platform.getOS()+"."+Platform.getOSArch());
            if (pluginBundle != null) {
                try {
                    String path = FileLocator.toFileURL(FileLocator.find(pluginBundle, new org.eclipse.core.runtime.Path("lib/libqtcppdesigner.so"), null)).getPath();
                    System.load(path);
                } catch (IOException ex) {
                    ex.printStackTrace();
                } catch (UnsatisfiedLinkError ex) {
                    ex.printStackTrace();
                }
            }
        }

        handleWidget = createControl(handle, embeddedHandle);
        if (handleWidget == 0) SWT.error (SWT.ERROR_NO_HANDLES);
        table.put(new Long(handleWidget), this);
        
        setFont(getFont());
    
        addDisposeListener(new DisposeListener() {
            public void widgetDisposed(DisposeEvent e) {
                SignalSlotEditorW.this.widgetDisposed(e);
            }
        });

        addControlListener(new ControlAdapter() {
            public void controlResized(ControlEvent e) {
                SignalSlotEditorW.this.controlResized(e);
            }
        });

        try {
            org.osgi.framework.Bundle bundle = org.eclipse.core.runtime.Platform.getBundle("com.trolltech.qtcppdesigner");
            if (bundle != null) {
                Class cls = bundle.loadClass("com.trolltech.qtcppdesigner.qtcppjambi.QtJambiConnection");
                java.lang.reflect.Method m = cls.getMethod("loadPlugins", new Class[] { com.trolltech.qtcppcommon.JambiCompatible.class });
                m.invoke(null, new Object[] { this });
            }
        } catch (Throwable t) { /* not jambi */ }

        initialize();
    }

    // Needs to be invoked in a class loaded by the 
    // same class loader which will be resolving
    // the native methods.
    public void loadLibrary(String path) {
        System.load(path);
    }
    public void widgetDisposed (DisposeEvent e)
    {
        table.remove(new Long(handleWidget));
        disposeControl(handleWidget);
        handleWidget = 0;
    }
  
    public void controlResized(ControlEvent e)
    {
        Rectangle rect = getClientArea();
        resizeControl(handleWidget, rect.x, rect.y, rect.width, rect.height);
    }

    public Point computeSize(int wHint, int hHint, boolean changed)
    {
        checkWidget();
        int [] result = new int[2];
        computeSize(handleWidget, result);
        if (wHint != SWT.DEFAULT) result[0] = wHint;
        if (hHint != SWT.DEFAULT) result[1] = hHint;
        int border = getBorderWidth();
        return new Point(result[0] + border * 2, result[1] + border * 2);
    }
    
    public void setFont(Font font)
    {
        super.setFont(font);
        
        if (font == null)
            return;
        FontData[] fntlist = font.getFontData();
        setFont (handleWidget, fntlist[0].getName(), fntlist[0].getHeight());
    }
    
    public String pluginFailureString()
    {
        checkWidget();
        return pluginFailureString(handleWidget);
    }

    public boolean initializeJambiPlugins(String jambiBase, String jambiPluginPath, String customWidgetClassPath, String resourcePath, String jvm)
    {
        checkWidget();
        return initializeJambiPlugins(handleWidget, jambiBase, jambiPluginPath, customWidgetClassPath, resourcePath, jvm);
    }

    public void initialize()
    {
        checkWidget();
        initialize(handleWidget);
    }

    public void updateCustomWidgetLocation(String path)
    {
        checkWidget();
        updateCustomWidgetLocation(handleWidget, path);
    }

    static final native long createControl(long phandle, long socketWin);
    static final native void computeSize(long handle, int [] result);
    static final native void disposeControl(long handle);
    static final native void resizeControl(long handle, int x, int y, int width, int height);
    static final native void setFont(long handle, String family, int size);
    static final native String pluginFailureString(long handle);
    static final native boolean initializeJambiPlugins(long handle, String jambiBase, String jambiPluginPath, String customWidgetClassPath, String resourcePath, String jvm);
    static final native void initialize(long handle);
    static final native void updateCustomWidgetLocation(long handle, String path);
}
