CHECKARCH = $$(ECLIPSEARCH)
linux*:isEmpty(CHECKARCH) {
    error("ECLIPSEARCH environment variable is not set! Please set it to the osgi arch, like x86, x86_64");
}

TARGET   = qtcppdesigner

win32 {
    PLUGINDIR = ../../../com.trolltech.qtcppdesigner.win32.x86
} else:macx {
    PLUGINDIR = ../../../com.trolltech.qtcppdesigner.macosx
    DESTDIR = $${PLUGINDIR}/os/macosx
} else:linux* {
    PLUGINDIR = ../../../com.trolltech.qtcppdesigner.linux.$$(ECLIPSEARCH)
    DESTDIR = $${PLUGINDIR}/lib
}
include(../../shared/common.pri)

contains(CONFIG, static) {
    DEFINES += QT_DESIGNER_STATIC
    DEFINES += QT_NODLL
}
INCLUDEPATH += $$QMAKE_INCDIR_QT/QtDesigner

CONFIG(debug, debug|release) {
    macx { LIBS += -lQtDesigner_debug -lQtDesignerComponents_debug -lQtScript_debug }
    else:linux* { LIBS += -lQtDesigner$${QT_LIBINFIX} -lQtDesignerComponents$${QT_LIBINFIX} -lQtScript$${QT_LIBINFIX} }
    else:win32 { LIBS += -lQtDesignerd -lQtDesignerComponentsd -lQtScriptd }
} else {
    LIBS += -lQtDesignerComponents$${QT_LIBINFIX} -lQtDesigner$${QT_LIBINFIX} -lQtScript$${QT_LIBINFIX}
}


include(qtcppdesigner_inc.pri)

HEADERS += \
    ../formeditorw.h \
    ../eclipseintegration.h


SOURCES += \
    ../formeditorw.cpp \
    ../eclipseintegration.cpp \
    ../qswt_main.cpp

# copy the .java files
JAVAFILES_SRC = java/*.java
JAVAFILES_DEST = $${PLUGINDIR}/src/com/trolltech/qtcppdesigner/views/embedded

win32 {
    JAVAFILES_SRC ~= s|/|\|
    JAVAFILES_DEST ~= s|/|\|
}

QMAKE_POST_LINK += $${QMAKE_COPY} $${JAVAFILES_SRC} $${JAVAFILES_DEST}

include(../../shared/designerintegrationv2/designerintegration.pri)|error("designerintegration.pri not found! check out //depot/ide/...")
