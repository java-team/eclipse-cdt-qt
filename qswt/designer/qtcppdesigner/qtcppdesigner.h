/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

#include <QObject>
#include <jni.h>
#include <pthread.h>

#ifndef QTDESIGNER_H
#define QTDESIGNER_H

extern "C" {
// ------ DesignerBridgeW ------
JNIEXPORT jlong JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_DesignerBridgeW_createControl(JNIEnv *, jclass, jlong, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_DesignerBridgeW_computeSize(JNIEnv *, jclass, jlong, jintArray);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_DesignerBridgeW_resizeControl(JNIEnv *, jclass, jlong, jint, jint, jint, jint);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_DesignerBridgeW_disposeControl(JNIEnv *, jclass, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_DesignerBridgeW_setFont(JNIEnv *, jclass, jlong, jstring, jint);
JNIEXPORT jstring JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_DesignerBridgeW_pluginFailureString(JNIEnv *, jclass, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_DesignerBridgeW_setPluginPath(JNIEnv *, jclass, jlong, jstring);
JNIEXPORT jboolean JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_DesignerBridgeW_initializeJambiPlugins(JNIEnv *, jclass, jlong, jstring, jstring, jstring, jstring, jstring);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_DesignerBridgeW_initialize(JNIEnv *, jclass, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_DesignerBridgeW_updateCustomWidgetLocation(JNIEnv *, jclass, jlong, jstring);

// ------ WidgetBoxW ------
JNIEXPORT jlong JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_WidgetBoxW_createControl(JNIEnv *, jclass, jlong, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_WidgetBoxW_computeSize(JNIEnv *, jclass, jlong, jintArray);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_WidgetBoxW_resizeControl(JNIEnv *, jclass, jlong, jint, jint, jint, jint);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_WidgetBoxW_disposeControl(JNIEnv *, jclass, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_WidgetBoxW_setFont(JNIEnv *, jclass, jlong, jstring, jint);
JNIEXPORT jstring JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_WidgetBoxW_pluginFailureString(JNIEnv *, jclass, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_WidgetBoxW_updateCustomWidgetLocation(JNIEnv *, jclass, jlong, jstring);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_WidgetBoxW_setBoxEnabled(JNIEnv *, jclass, jlong, jboolean);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_WidgetBoxW_updateCustomWidgets(JNIEnv *, jclass, jlong);
JNIEXPORT jboolean JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_WidgetBoxW_initializeJambiPlugins(JNIEnv *, jclass, jlong, jstring, jstring, jstring, jstring, jstring);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_WidgetBoxW_initialize(JNIEnv *, jclass, jlong);

// ------ PropertyEditorW ------
JNIEXPORT jlong JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_PropertyEditorW_createControl(JNIEnv *, jclass, jlong, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_PropertyEditorW_computeSize(JNIEnv *, jclass, jlong, jintArray);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_PropertyEditorW_resizeControl(JNIEnv *, jclass, jlong, jint, jint, jint, jint);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_PropertyEditorW_disposeControl(JNIEnv *, jclass, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_PropertyEditorW_setFont(JNIEnv *, jclass, jlong, jstring, jint);
JNIEXPORT jstring JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_PropertyEditorW_pluginFailureString(JNIEnv *, jclass, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_PropertyEditorW_clearSheet(JNIEnv *, jclass, jlong);
JNIEXPORT jboolean JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_PropertyEditorW_initializeJambiPlugins(JNIEnv *, jclass, jlong, jstring, jstring, jstring, jstring, jstring);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_PropertyEditorW_initialize(JNIEnv *, jclass, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_PropertyEditorW_updateCustomWidgetLocation(JNIEnv *, jclass, jlong, jstring);

// ------ ObjectInspectorW ------
JNIEXPORT jlong JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_ObjectInspectorW_createControl(JNIEnv *, jclass, jlong, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_ObjectInspectorW_computeSize(JNIEnv *, jclass, jlong, jintArray);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_ObjectInspectorW_resizeControl(JNIEnv *, jclass, jlong, jint, jint, jint, jint);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_ObjectInspectorW_disposeControl(JNIEnv *, jclass, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_ObjectInspectorW_setFont(JNIEnv *, jclass, jlong, jstring, jint);
JNIEXPORT jstring JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_ObjectInspectorW_pluginFailureString(JNIEnv *, jclass, jlong);
JNIEXPORT jboolean JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_ObjectInspectorW_initializeJambiPlugins(JNIEnv *, jclass, jlong, jstring, jstring, jstring, jstring, jstring);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_ObjectInspectorW_initialize(JNIEnv *, jclass, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_ObjectInspectorW_updateCustomWidgetLocation(JNIEnv *, jclass, jlong, jstring);

// ------ FormWindowW ------
JNIEXPORT jlong JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_FormWindowW_createControl(JNIEnv *, jclass, jlong, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_FormWindowW_computeSize(JNIEnv *, jclass, jlong, jintArray);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_FormWindowW_resizeControl(JNIEnv *, jclass, jlong, jint, jint, jint, jint);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_FormWindowW_disposeControl(JNIEnv *, jclass, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_FormWindowW_setFont(JNIEnv *, jclass, jlong, jstring, jint);
JNIEXPORT jstring JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_FormWindowW_pluginFailureString(JNIEnv *, jclass, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_FormWindowW_open(JNIEnv *, jclass, jlong, jstring);
JNIEXPORT jboolean JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_FormWindowW_save(JNIEnv *, jclass, jlong);
JNIEXPORT jboolean JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_FormWindowW_saveAs(JNIEnv *, jclass, jlong, jstring);
JNIEXPORT jboolean JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_FormWindowW_isDirty(JNIEnv *, jclass, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_FormWindowW_close(JNIEnv *, jclass, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_FormWindowW_initialize(JNIEnv *, jclass, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_FormWindowW_setActiveFormWindow(JNIEnv *, jclass, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_FormWindowW_setObjectName(JNIEnv *, jclass, jlong, jstring);
JNIEXPORT jboolean JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_FormWindowW_initializeJambiPlugins(JNIEnv *, jclass, jlong, jstring, jstring, jstring, jstring, jstring);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_FormWindowW_updateJambiResourcePath(JNIEnv *, jclass, jlong, jstring);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_FormWindowW_updateCustomWidgetLocation(JNIEnv *, jclass, jlong, jstring);
JNIEXPORT jint JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_FormWindowW_actionCount(JNIEnv *, jclass, jlong);
JNIEXPORT jint JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_FormWindowW_staticActionCount(JNIEnv *, jclass, jlong);
JNIEXPORT jstring JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_FormWindowW_actionName(JNIEnv *, jclass, jlong, jint);
JNIEXPORT jstring JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_FormWindowW_actionToolTip(JNIEnv *, jclass, jlong, jint);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_FormWindowW_actionTrigger(JNIEnv *, jclass, jlong, jint);
JNIEXPORT jboolean JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_FormWindowW_isEnabled(JNIEnv *, jclass, jlong, jint);
JNIEXPORT jint JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_FormWindowW_toolCount(JNIEnv *, jclass, jlong);
JNIEXPORT jstring JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_FormWindowW_toolName(JNIEnv *, jclass, jlong, jint);
JNIEXPORT jstring JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_FormWindowW_toolToolTip(JNIEnv *, jclass, jlong, jint);
JNIEXPORT jint JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_FormWindowW_currentTool(JNIEnv *, jclass, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_FormWindowW_setCurrentTool(JNIEnv *, jclass, jlong, jint);

// ------ ActionEditorW ------
JNIEXPORT jlong JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_ActionEditorW_createControl(JNIEnv *, jclass, jlong, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_ActionEditorW_computeSize(JNIEnv *, jclass, jlong, jintArray);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_ActionEditorW_resizeControl(JNIEnv *, jclass, jlong, jint, jint, jint, jint);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_ActionEditorW_disposeControl(JNIEnv *, jclass, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_ActionEditorW_setFont(JNIEnv *, jclass, jlong, jstring, jint);
JNIEXPORT jstring JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_ActionEditorW_pluginFailureString(JNIEnv *, jclass, jlong);
JNIEXPORT jboolean JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_ActionEditorW_initializeJambiPlugins(JNIEnv *, jclass, jlong, jstring, jstring, jstring, jstring, jstring);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_ActionEditorW_initialize(JNIEnv *, jclass, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_ActionEditorW_updateCustomWidgetLocation(JNIEnv *, jclass, jlong, jstring);

// ------ SignalSlotEditorW ------
JNIEXPORT jlong JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_SignalSlotEditorW_createControl(JNIEnv *, jclass, jlong, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_SignalSlotEditorW_computeSize(JNIEnv *, jclass, jlong, jintArray);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_SignalSlotEditorW_resizeControl(JNIEnv *, jclass, jlong, jint, jint, jint, jint);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_SignalSlotEditorW_disposeControl(JNIEnv *, jclass, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_SignalSlotEditorW_setFont(JNIEnv *, jclass, jlong, jstring, jint);
JNIEXPORT jstring JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_SignalSlotEditorW_pluginFailureString(JNIEnv *, jclass, jlong);
JNIEXPORT jboolean JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_SignalSlotEditorW_initializeJambiPlugins(JNIEnv *, jclass, jlong, jstring, jstring, jstring, jstring, jstring);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_SignalSlotEditorW_initialize(JNIEnv *, jclass, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_SignalSlotEditorW_updateCustomWidgetLocation(JNIEnv *, jclass, jlong, jstring);

// ------ ResourceEditorW ------
JNIEXPORT jlong JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_ResourceEditorW_createControl(JNIEnv *, jclass, jlong, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_ResourceEditorW_computeSize(JNIEnv *, jclass, jlong, jintArray);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_ResourceEditorW_resizeControl(JNIEnv *, jclass, jlong, jint, jint, jint, jint);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_ResourceEditorW_disposeControl(JNIEnv *, jclass, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_ResourceEditorW_setFont(JNIEnv *, jclass, jlong, jstring, jint);
JNIEXPORT jstring JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_ResourceEditorW_pluginFailureString(JNIEnv *, jclass, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_ResourceEditorW_updateResources(JNIEnv *, jclass, jlong, jstring);
JNIEXPORT jboolean JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_ResourceEditorW_initializeJambiPlugins(JNIEnv *, jclass, jlong, jstring, jstring, jstring, jstring, jstring);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_ResourceEditorW_initialize(JNIEnv *, jclass, jlong);
JNIEXPORT void JNICALL Java_com_trolltech_qtcppdesigner_views_embedded_ResourceEditorW_updateCustomWidgetLocation(JNIEnv *, jclass, jlong, jstring);
}

// ------ FormWindowW ------
class FormWindowWListener : public QObject
{
    Q_OBJECT
public:
    FormWindowWListener(QObject *parent, pthread_key_t *key, jclass that)
        : QObject(parent)
    {
        envKey = key;
        JNIEnv *env = (JNIEnv *)pthread_getspecific(*envKey);
        javaClass = (jclass) env->NewGlobalRef((jobject)that);
        jmId[0] = env->GetStaticMethodID(that, "actionChanged", "(JI)V");
        jmId[1] = env->GetStaticMethodID(that, "checkActiveWindow", "(J)V");
        jmId[2] = env->GetStaticMethodID(that, "resourceFilesChanged", "(J)V");
        jmId[3] = env->GetStaticMethodID(that, "updateDirtyFlag", "(J)V");
    }

    ~FormWindowWListener() { }

public slots:
    void slot_actionChanged(int id)
    {
        JNIEnv *env = (JNIEnv *)pthread_getspecific(*envKey);
        if (env == NULL) return;
        env->CallStaticVoidMethod(javaClass, jmId[0], (jlong)parent(), id);
    }

    void slot_checkActiveWindow()
    {
        JNIEnv *env = (JNIEnv *)pthread_getspecific(*envKey);
        if (env == NULL) return;
        env->CallStaticVoidMethod(javaClass, jmId[1], (jlong)parent());
    }

    void slot_resourceFilesChanged()
    {
        JNIEnv *env = (JNIEnv *)pthread_getspecific(*envKey);
        if (env == NULL) return;
        env->CallStaticVoidMethod(javaClass, jmId[2], (jlong)parent());
    }

    void slot_updateDirtyFlag()
    {
        JNIEnv *env = (JNIEnv *)pthread_getspecific(*envKey);
        if (env == NULL) return;
        env->CallStaticVoidMethod(javaClass, jmId[3], (jlong)parent());
    }

private:
    pthread_key_t *envKey;
    jclass javaClass;
    jmethodID jmId[4];
};

#endif //QTDESIGNER_H
