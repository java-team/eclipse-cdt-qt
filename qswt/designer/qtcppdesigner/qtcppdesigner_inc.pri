TEMPLATE = lib

CONFIG += core gui x11

QT += xml

INCLUDEPATH += ./ \
    ../ \
    ../../shared \
    $$(JAVADIR)/include \
    $$(JAVADIR)/include/linux \
    /usr/X11R6/include \
    /usr/include/gtk-2.0 \
    /usr/include/glib-2.0 \
    /usr/lib/glib-2.0/include \
    /usr/lib/gtk-2.0/include \
    /usr/include/cairo \
    /usr/include/pango-1.0 \
    /usr/include/atk-1.0 \
    $$(QTDIR)/src/gui/kernel

LIBS += -L/usr/X11R6/lib \
    -L/usr/lib \
    -lX11 \
    -lXt \
    -lgdk-x11-2.0 \
    -lgtk-x11-2.0

HEADERS += qtcppdesigner.h\
    ../designerbridgew.h\
    ../widgetboxw.h\
    ../propertyeditorw.h\
    ../objectinspectorw.h\
    ../formwindoww.h\
    ../actioneditorw.h\
    ../signalsloteditorw.h\
    ../resourceeditorw.h

SOURCES += qtcppdesigner.cpp\
    ../designerbridgew.cpp\
    ../widgetboxw.cpp\
    ../propertyeditorw.cpp\
    ../objectinspectorw.cpp\
    ../formwindoww.cpp\
    ../actioneditorw.cpp\
    ../signalsloteditorw.cpp\
    ../resourceeditorw.cpp