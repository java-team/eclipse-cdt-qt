/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

#include "objectinspectorw.h"
#include "formeditorw.h"
#include "widgetboxw.h"

#include "abstractobjectinspector.h"

#include <QResizeEvent>

ObjectInspectorW *ObjectInspectorW::m_self = 0;

ObjectInspectorW::ObjectInspectorW(QWidget *parent)
    : QWidget(parent), m_editor(0), m_initialized(false)
{
    if (m_self != 0)
        delete m_self;

    m_self = this;
    FormEditorW::instance()->setObjectInspector(this);
}

ObjectInspectorW::~ObjectInspectorW()
{
    m_self = 0;
    FormEditorW::instance()->setObjectInspector(0);

    if (m_editor != 0) {
        m_editor->hide();
        m_editor->setParent(0);
    }
}


void ObjectInspectorW::initialize()
{
    if (!m_initialized) {
        m_initialized = true;
        FormEditorW::instance()->initialize();

        if (!FormEditorW::instance()->formEditor()->objectInspector()) {
            m_editor = QDesignerComponents::createObjectInspector(FormEditorW::instance()->formEditor(), this);
            FormEditorW::instance()->formEditor()->setObjectInspector(m_editor);
        } else {
            m_editor = FormEditorW::instance()->formEditor()->objectInspector();
            m_editor->setParent(this);
        }
        m_editor->setGeometry(geometry());
        m_editor->show();
    }
}

void ObjectInspectorW::updateCustomWidgetLocation(const QString &path)
{
    WidgetBoxW *w = WidgetBoxW::instance();
    if (w != 0) {
        w->updateCustomWidgetLocation(path);
    }
}

bool ObjectInspectorW::initializeJambiPlugins(const QString &jambiBase, const QString &jambiPluginPath,
                                         const QString &customWidgetClassPath, const QString &resourcePath, const QString &jvm)
{
    return FormEditorW::instance()->initializeJambiPlugins(jambiBase, jambiPluginPath, customWidgetClassPath, resourcePath, jvm);
}

ObjectInspectorW *ObjectInspectorW::instance()
{
    if (m_self == 0)
        m_self = new ObjectInspectorW();
    m_self->initialize();

    return m_self;
}

void ObjectInspectorW::resizeEvent(QResizeEvent *event)
{
    if (m_editor != 0)
        m_editor->resize(event->size());
    QWidget::resizeEvent(event);
}

QSize ObjectInspectorW::minimumSize()
{
    if (m_editor != 0)
        return m_editor->minimumSize();
    else
        return QWidget::minimumSize();
}

QString ObjectInspectorW::pluginFailureString() const
{
    return FormEditorW::instance()->pluginFailureString();
}

