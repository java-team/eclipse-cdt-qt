/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

#ifndef FORMEDITORW_H
#define FORMEDITORW_H

// Designer includes
#include <qdesigner_components.h>
#include <QtDesigner/private/qdesigner_integration_p.h>
#include <abstractformwindowmanager.h>
#include <abstractformeditor.h>
#include <abstractpropertyeditor.h>
#include <abstractwidgetbox.h>

class ObjectInspectorW;
class PropertyEditorW;
class WidgetBoxW;
class ActionEditorW;
class SignalSlotEditorW;
class ResourceEditorW;
class FormWindowW;

class FormEditorW : public QObject
{
    Q_OBJECT
public:
    enum DesignerAction {
        // integrated ones
        ActionCut = 0,
        ActionCopy = 1,
        ActionPaste = 2,
        ActionDelete = 3,
        ActionSelectAll = 4,
        ActionUndo = 5,
        ActionRedo = 6,

        // additional ones
        ActionLower = 7,
        ActionRaise = 8,
        ActionHorizontalLayout = 9,
        ActionVerticalLayout = 10,
        ActionSplitHorizontal = 11,
        ActionSplitVertical = 12,
        ActionGridLayout = 13,
        ActionBreakLayout = 14,
        ActionAdjustSize = 15,
        LastStaticAction = 15
    };

    FormEditorW(QObject *parent = 0);
    virtual ~FormEditorW();
    static FormEditorW *instance();
    QAction *idToAction(int id);

    bool updateTopLevel(QWidget *delWidget = 0);
    inline QDesignerFormEditorInterface *formEditor() const { return m_formeditor; }

    inline QWidget *signalSlotEditor() const { return m_designerSignalSlotEditor; }
    inline void setDesignerSignalSlotEditor(QWidget *w) { m_designerSignalSlotEditor = w; }

    inline int lastAction() { return LastStaticAction + m_previewActions.size(); }

    bool eventFilter(QObject *, QEvent *);

    void initPlugins();
    QString pluginFailureString() const;
    bool initializeJambiPlugins(const QString &jambiBase, const QString &jambiPluginPath,
        const QString &customWidgetClassPath, const QString &resourcePath, const QString &jvm);
    void setPluginPath(const QString &pluginPath);

    void setResourceEditor(ResourceEditorW *resourceEditor) {
        m_resourceEditor = resourceEditor;
    }

    void setObjectInspector(ObjectInspectorW *objectInspector) {
        m_objectInspector = objectInspector;
    }

    void setPropertyEditor(PropertyEditorW *propertyEditor) {
        m_propertyEditor = propertyEditor;
    }

    void setWidgetBox(WidgetBoxW *widgetBox) {
        m_widgetBox = widgetBox;
    }

    void setActionEditor(ActionEditorW *actionEditor) {
        m_actionEditor = actionEditor;
    }

    void setSignalSlotEditor(SignalSlotEditorW *signalSlotEditor) {
        m_signalSlotEditor = signalSlotEditor;
    }

    void initialize();

    void addFormWindowW(FormWindowW *formWindowW);
    void removeFormWindowW(FormWindowW *formWindowW);
    FormWindowW *activeFormWindowW() const;

signals:
    void updateCustomWidgetPlugins();

public slots:
    void activeFormWindowChanged(QDesignerFormWindowInterface *);

private slots:
    void preview(const QString &);

private:
    void initializeCorePlugins();
    void setupPreviewActions();
    static FormEditorW *m_self;
    QDesignerFormEditorInterface *m_formeditor;
    QWidget *m_designerSignalSlotEditor;
    QList<QAction *> m_previewActions;

    QString m_jambiPluginPath;

    ObjectInspectorW *m_objectInspector;
    PropertyEditorW *m_propertyEditor;
    WidgetBoxW *m_widgetBox;
    ActionEditorW *m_actionEditor;
    SignalSlotEditorW *m_signalSlotEditor;
    ResourceEditorW *m_resourceEditor;

    QList<FormWindowW *> m_formWindows;

    QString m_pluginFailureString;

    uint m_initialized : 1;
};

class ActionChangedNotifier : public QObject
{
    Q_OBJECT
public:
    ActionChangedNotifier(QObject *parent, int id);

public slots:
    void actionChanged();

private:
    int actId;
};

#endif //FORMEDITORW_H
