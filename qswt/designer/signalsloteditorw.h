/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

#ifndef SIGNALSLOTEDITORW_H
#define SIGNALSLOTEDITORW_H

#include <QWidget>

class SignalSlotEditorW : public QWidget
{
    Q_OBJECT
    Q_CLASSINFO("ClassID", "{72BD12D6-9599-4171-A76A-FB01DBE1AAAC}")
    Q_CLASSINFO("InterfaceID", "{0B3F6E54-4B88-4B76-B87F-98F0CA07AAA3}")
    Q_CLASSINFO("EventsID", "{25E5BEC7-FD60-4326-8718-785B8C92AAAF}")
    Q_CLASSINFO("ToSuperClass", "SignalSlotEditorW")

public:
    SignalSlotEditorW(QWidget *parent = 0);
    ~SignalSlotEditorW();

    QSize minimumSize();
    static SignalSlotEditorW *instance();

public slots:
    QString pluginFailureString() const;
    bool initializeJambiPlugins(const QString &jambiBase, const QString &jambiPluginPath,
        const QString &customWidgetClassPath, const QString &resourcePath, const QString &jvm);
    void initialize();
    void updateCustomWidgetLocation(const QString &path);

protected:
    void resizeEvent(QResizeEvent *event);

private:
    QWidget *m_editor;
    uint m_initialized : 1;
    static SignalSlotEditorW *m_self;
};

#endif //SIGNALSLOTEDITORW_H
