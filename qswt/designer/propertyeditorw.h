/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

#ifndef PROPERTYEDITORW_H
#define PROPERTYEDITORW_H

#include <QWidget>

QT_FORWARD_DECLARE_CLASS(QDesignerPropertyEditorInterface)

class PropertyEditorW : public QWidget
{
    Q_OBJECT
    Q_CLASSINFO("ClassID", "{29B6C1B8-5163-4FFC-B762-ABD202FCAAA0}")
    Q_CLASSINFO("InterfaceID", "{2EAF3EA8-438B-46EC-BDA9-1A3A8CE2AAA9}")
    Q_CLASSINFO("EventsID", "{ACE7282C-6385-49F1-9192-316EB889AAA5}")
    Q_CLASSINFO("ToSuperClass", "PropertyEditorW")

public:
    PropertyEditorW(QWidget *parent = 0);
    ~PropertyEditorW();

    QSize minimumSize();
    static PropertyEditorW *instance();

public slots:
    QString pluginFailureString() const;
    void clearSheet();
    bool initializeJambiPlugins(const QString &jambiBase, const QString &jambiPluginPath,
        const QString &customWidgetClassPath, const QString &resourcePath, const QString &jvm);
    void initialize();
    void updateCustomWidgetLocation(const QString &path);

protected:
    void resizeEvent(QResizeEvent *event);

private:
    QDesignerPropertyEditorInterface *m_editor;
    uint m_initialized : 1;
    static PropertyEditorW *m_self;
};

#endif //PROPERTYEDITORW_H
