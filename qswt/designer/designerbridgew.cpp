/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

#include "designerbridgew.h"
#include "formeditorw.h"
#include "widgetboxw.h"

QString DesignerBridgeW::pluginFailureString() const
{
    return FormEditorW::instance()->pluginFailureString();
}

void DesignerBridgeW::setPluginPath(const QString &pluginPath)
{
   FormEditorW::instance()->setPluginPath(pluginPath);
}

DesignerBridgeW::DesignerBridgeW(QWidget *parent) : QWidget(parent), m_initialized(false)
{
}

bool DesignerBridgeW::initializeJambiPlugins(const QString &jambiBase, const QString &jambiPluginPath,
                                             const QString &customWidgetClassPath, const QString &resourcePath, const QString &jvm)
{
    return FormEditorW::instance()->initializeJambiPlugins(jambiBase, jambiPluginPath, customWidgetClassPath, resourcePath, jvm);
}

void DesignerBridgeW::initialize()
{
    if (!m_initialized) {
        m_initialized = true;
        FormEditorW::instance()->initialize();
    }
}

void DesignerBridgeW::updateCustomWidgetLocation(const QString &path)
{
    WidgetBoxW *w = WidgetBoxW::instance();
    if (w != 0) {
        w->updateCustomWidgetLocation(path);
    }
}

