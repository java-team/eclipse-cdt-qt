/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppdesigner.qtcpp.wizards.pages;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.dialogs.ContainerSelectionDialog;

import com.trolltech.qtcppdesigner.qtcpp.wizards.QtDesignerFormsWizard;

/**
 * The "New" wizard page allows setting the container for the new file as well
 * as the file name. The page will only accept file name without the extension
 * OR with the extension that matches the expected one (ui).
 */

public class QtDesignerFormsWizardPage extends WizardPage {
	private Text containerText;
	private Text fileText;
	private List templateList;
	private Label preview;

	private ISelection selection;

	/**
	 * Constructor for SampleNewWizardPage.
	 * 
	 * @param pageName
	 */
	public QtDesignerFormsWizardPage(ISelection selection) {
		super("wizardPage");
		setTitle("Qt Designer Templates");
		this.selection = selection;
	}

	/**
	 * @see IDialogPage#createControl(Composite)
	 */
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 3;
		layout.verticalSpacing = 9;
		Label label = new Label(container, SWT.NULL);
		label.setText("&Source Folder:");

		containerText = new Text(container, SWT.BORDER | SWT.SINGLE);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		containerText.setLayoutData(gd);
		containerText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}
		});
		
		Button button = new Button(container, SWT.PUSH);
		button.setText("Browse...");
		button.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				handleBrowse();
			}
		});
				
		Composite templateComposite = new Composite(container, SWT.NULL);
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		gridLayout.horizontalSpacing = 10;
		gridLayout.marginWidth = 0;
		templateComposite.setLayout(gridLayout);
		
		gd = new GridData(GridData.FILL_BOTH);
		gd.horizontalSpan = 3;
		templateComposite.setLayoutData(gd);
		
		label = new Label(templateComposite, SWT.NULL);
		label.setText("Template:");
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = 2;
		label.setLayoutData(gd);
		
		templateList = new List(templateComposite, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL | SWT.SINGLE);
		templateList.addSelectionListener(new SelectionListener() {
		      public void widgetSelected(SelectionEvent event) {
		        updateTemplate();
		      }
		      
		      public void widgetDefaultSelected(SelectionEvent event) {
		          updateTemplate();
		        }
		});
		templateList.setSize(80, 150);
		gd = new GridData();
		gd.verticalAlignment = SWT.FILL;
		gd.grabExcessVerticalSpace = true;
		templateList.setLayoutData(gd);
				
		preview = new Label(templateComposite, SWT.NONE);
		preview.setSize(150, 150);
		gd = new GridData();
		gd.verticalAlignment = SWT.FILL;
		gd.grabExcessVerticalSpace = true;
		preview.setLayoutData(gd);
		
		label = new Label(container, SWT.NULL);
		label.setText("&File name:");

		fileText = new Text(container, SWT.BORDER | SWT.SINGLE);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		fileText.setLayoutData(gd);
		fileText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}
		});
		
		insertTemplates();
		initialize();
		dialogChanged();
		setControl(container);
		updateTemplate();
	}
	
	private void insertTemplates()
	{
		InputStream stream = getClass().getResourceAsStream(QtDesignerFormsWizard.TEMPLATE_LOCATION + "/templates.txt");
        if (stream != null) {
            BufferedReader r = new BufferedReader(new InputStreamReader(stream));

            try {
                String s = null;
                while ((s = r.readLine()) != null) {
                	templateList.add(s.replace('_', ' '));
                    if (s.equals("Widget")) {
                    	templateList.setSelection(templateList.getItemCount()-1);                        
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Can't find ui templates.");
        }
	}
	
	private void updateTemplate()
	{
		Image image = null;
        try {
        	String name = QtDesignerFormsWizard.TEMPLATE_LOCATION + "/" + templateList.getSelection()[0] + ".png";
            name = name.replace(' ', '_');
            InputStream stream = getClass().getResourceAsStream(name);
            image = new Image(preview.getDisplay(), stream);
        } catch (RuntimeException e) {
        }

        preview.setImage(image);
        preview.pack();        
	}

	/**
	 * Tests if the current workbench selection is a suitable container to use.
	 */

	private void initialize() {
		if (selection != null && selection.isEmpty() == false
				&& selection instanceof IStructuredSelection) {
			IStructuredSelection ssel = (IStructuredSelection) selection;
			if (ssel.size() > 1)
				return;
			Object obj = ssel.getFirstElement();
			if (!(obj instanceof IResource) && (obj instanceof IAdaptable))
				obj = ((IAdaptable)obj).getAdapter(IResource.class);
			if (obj instanceof IResource) {
				IContainer container;
				if (obj instanceof IContainer)
					container = (IContainer) obj;
				else
					container = ((IResource) obj).getParent();
				containerText.setText(container.getFullPath().makeRelative().toString());
			}
		}	
		fileText.setFocus();
	}

	/**
	 * Uses the standard container selection dialog to choose the new value for
	 * the container field.
	 */

	private void handleBrowse() {
		ContainerSelectionDialog dialog = new ContainerSelectionDialog(
				getShell(), ResourcesPlugin.getWorkspace().getRoot(), false,
				"Select source folder");
		if (dialog.open() == ContainerSelectionDialog.OK) {
			Object[] result = dialog.getResult();
			if (result.length == 1) {
				containerText.setText(((Path) result[0]).makeRelative().toString());
			}
		}
	}

	/**
	 * Ensures that both text fields are set.
	 */

	private void dialogChanged() {
		IResource container = ResourcesPlugin.getWorkspace().getRoot()
				.findMember(new Path(getContainerName()));
		String fileName = getFileName();

		if (getContainerName().length() == 0) {
			updateStatus("Source folder must be specified");
			return;
		}
		if (container == null
				|| (container.getType() & (IResource.PROJECT | IResource.FOLDER)) == 0) {
			updateStatus("Source folder must exist");
			return;
		}
		if (!container.isAccessible()) {
			updateStatus("Project must be writable");
			return;
		}
		if (fileName.length() == 0) {
			updateStatus("File name must be specified");
			return;
		}
		if (fileName.replace('\\', '/').indexOf('/', 1) > 0) {
			updateStatus("File name must be valid");
			return;
		}
		
		final IFile file = ((IContainer)container).getFile(new Path(fileName));
		updateStatus(file.toString());
		if (file.exists()) {
			updateStatus("File already exists");
			return;
		}
		
		String ext = "";
		int dotLoc = fileName.lastIndexOf('.');
		if (dotLoc != -1) {
			ext = fileName.substring(dotLoc + 1);
		}
		if (ext.equalsIgnoreCase("ui") == false) {
			updateStatus("File extension must be \"ui\"");
			return;
		}
		updateStatus(null);
	}

	private void updateStatus(String message) {
		setErrorMessage(message);
		setPageComplete(message == null);
	}

	public String getContainerName() {
		return containerText.getText();
	}

	public String getFileName() {
		return fileText.getText();
	}
	
	public String getTemplateName()
	{
		String name = QtDesignerFormsWizard.TEMPLATE_LOCATION + "/" + templateList.getSelection()[0] + ".ui";
		return name.replace(' ', '_');
	}
}