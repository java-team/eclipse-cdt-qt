/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppcommon.editors;

import java.io.File;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IWindowListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.texteditor.InfoForm;

public class EditorInputWatcher implements IPartListener, IWindowListener, IResourceChangeListener {
    private IQtEditor part;
    private long timestamp;
    private IResource resource;

    public EditorInputWatcher(IQtEditor part) {
        this.part = part;
        this.resource = (IResource)part.getEditorInput().getAdapter(IResource.class);
        this.part.getSite().getWorkbenchWindow().getPartService().addPartListener(this);
        PlatformUI.getWorkbench().addWindowListener(this);
        ResourcesPlugin.getWorkspace().addResourceChangeListener(this);
        updateTimeStamp();
    }
    public void dispose() {
        this.part.getSite().getWorkbenchWindow().getPartService().removePartListener(this);
        PlatformUI.getWorkbench().removeWindowListener(this);
        ResourcesPlugin.getWorkspace().removeResourceChangeListener(this);
    }
    private String fileName() {
        FileEditorInput fin = (FileEditorInput)part.getEditorInput();
        return fin.getFile().getLocation().toOSString();
    }
    private long getCurrentTimeStamp() {
        return (new File(fileName())).lastModified();
    }
    private boolean readable() {
        File file = new File(fileName());
        return file.exists() && file.canRead();
    }
    public void updateTimeStamp() {
        if (!readable())
            timestamp = -1;
        else
            timestamp = getCurrentTimeStamp();
    }
    public void partActivated(IWorkbenchPart partArg) {
        // explanations to the logic:
        // i) timestamp > -1 means the editor is not already showing the missing file info
        // ii) the resource checking is used to prevent the check in case the resource was deleted from eclipse's resource system,
        //     except if the editor is dirty.
        //     resource deletions are handled in the resource change listener
        // ### this can probably be simplified when this becomes the resource change listener
        if (partArg == this.part && timestamp > -1 && (resource == null || resource.exists() || part.isDirty())) {
            if (!readable()) {
                String[] buttons = {"Save", "Close"};
                MessageDialog dialog = new MessageDialog(part.getSite().getShell(),
                        "File not accessible", null,
                        "The file has been deleted or is not accessible. "+
                        "Do you want to save your changes or close the editor without saving?",
                        MessageDialog.QUESTION, buttons, 0);
                if (dialog.open() == 0) {
                    this.part.doSave(new NullProgressMonitor());
                }
                part.getSite().getShell().getDisplay().asyncExec(new Runnable() {
                    public void run() {
                            part.getSite().getPage().closeEditor(part, false);
                        }
                    });
            } else {
                long newtimestamp = getCurrentTimeStamp();
                if (newtimestamp != timestamp) {
                    if (MessageDialog.openQuestion(part.getSite().getShell(),
                            "File changed",
                            "The file has been changed on the file system. "+
                            "Do you want to replace the editor contents with these changes?")) {
                        this.part.reload();
                    }
                }
            }
            updateTimeStamp();
        }
    }
    public void partBroughtToTop(IWorkbenchPart part) {
    }
    public void partClosed(IWorkbenchPart part) {
    }
    public void partDeactivated(IWorkbenchPart part) {
    }
    public void partOpened(IWorkbenchPart part) {
    }

    public void windowActivated(IWorkbenchWindow window) {
        if (window == part.getEditorSite().getWorkbenchWindow()) {
            // otherwise the dialog might not be shown:
            window.getShell().getDisplay().asyncExec(new Runnable() {
                public void run() {
                    partActivated(part);
                }
            });
        }
    }
    public void windowClosed(IWorkbenchWindow window) {
    }
    public void windowDeactivated(IWorkbenchWindow window) {
    }
    public void windowOpened(IWorkbenchWindow window) {
    }

    public void resourceChanged(IResourceChangeEvent event) {
        handleResourceDelta(event.getDelta());
    }

    public static Control createMissingFileInfo(Composite parent, String fileName) {
        parent.setLayout(new FillLayout(SWT.VERTICAL));
        InfoForm info = new InfoForm(parent);
        info.setBannerText("");
        info.setHeaderText("");
        info.setInfo("File "+fileName+" does not exist or is not readable.");
        return info.getControl();
    }

    private boolean handleResourceDelta(IResourceDelta delta) {
        if (delta == null) // there are events that have no delta
            return false;
        if (delta.getKind() == IResourceDelta.REMOVED) {
            if (delta.getResource().equals(resource)) {
                part.getSite().getShell().getDisplay().syncExec(new Runnable() {
                    public void run() {
                        part.getSite().getPage().closeEditor(part, false);
                    }
                });
                return true;
            }
        }

        IResourceDelta deltas[] = delta.getAffectedChildren();
        for (int i=0; deltas != null && i<deltas.length; ++i) {
            if (handleResourceDelta(deltas[i]))
                return true;
        }
        return false;
    }
}
