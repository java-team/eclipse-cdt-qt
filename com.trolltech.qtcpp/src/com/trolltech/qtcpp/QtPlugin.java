/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcpp;

import java.io.*;
import java.util.StringTokenizer;
import java.util.Vector;

import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class QtPlugin extends Plugin {

    // The plug-in ID
    public static final String PLUGIN_ID = "com.trolltech.qtcpp";

    // Whether we already asked about gtk-qt-engine
    private static boolean alreadyAskedAboutGtkQtEngine = false;
    // False if we have Qt3 loaded and the user answered "don't load integration plugins"
    private static boolean validQt = true;

    // The shared instance
    private static QtPlugin plugin;

    /**
     * The constructor
     *
     * Never called
     */
    public QtPlugin() {
        plugin = this;
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.core.runtime.Plugins#start(org.osgi.framework.BundleContext)
     *
     * Never called
     */
    public void start(BundleContext context) throws Exception {
        super.start(context);
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.core.runtime.Plugin#stop(org.osgi.framework.BundleContext)
     *
     * Never called
     */
    public void stop(BundleContext context) throws Exception {
        plugin = null;
        super.stop(context);
    }

    /**
     * Returns the shared instance
     *
     * @return the shared instance
     */
    public static QtPlugin getDefault() {
        return plugin;
    }

    public static Vector resolveLinuxLibrary(String libName) {
        File file = new File("/proc/self/maps");

        Vector res = new Vector();
        try {
            BufferedReader dis = new BufferedReader(new InputStreamReader(new FileInputStream(file)));

            String line = dis.readLine();
            while (line != null) {
                StringTokenizer st = new StringTokenizer(line);
                while (st.hasMoreTokens()) {
                    String part = st.nextToken();
                    if (part.startsWith(File.separator)) {
                        File lib = new File(part);
                        if (lib.getName().startsWith(libName))
                            res.add(lib);
                        break;
                    }
                }

                line = dis.readLine();
            }
            dis.close();

        } catch (Throwable e) {
            e.printStackTrace();
        }
        return res;
    }

    public static boolean checkQtValid() {
        if (!alreadyAskedAboutGtkQtEngine && Platform.getOS().equals(Platform.OS_LINUX)){
            Vector libs = resolveLinuxLibrary("libqt-mt.so.3");
            if (!libs.isEmpty()) {
                Display.getDefault().syncExec(new Runnable() {
                    public void run() {
                        MessageBox box = new MessageBox(new Shell(), SWT.ICON_ERROR | SWT.YES | SWT.NO);
                        box.setMessage("Your Eclipse installations appears to be binding against the Qt 3 " +
                                       "libraries. Since the Qt Eclipse Integration plugin is based on Qt 4, this may cause " +
                                       "the application to crash.\n\nThe most common reason for this problem is " +
                                       "that you are running KDE with the KDE GTK style active.\n\n" +
                                       "(On many systems you can open KDE System Settings->Appearance->GTK Styles and Fonts " +
                                       " and change to a style like 'Clearlooks', " +
                                       "or you have to remove the package gtk-qt-engine from your system.)\n\n" +
                                       "Do you still want to continue loading the Qt Eclipse Integration?");

                        validQt = (box.open() == SWT.YES);
                        alreadyAskedAboutGtkQtEngine = true;
                    }
                });
            }
        }
        return validQt;
    }
}
