/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppintegrationhelp;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.intro.IIntroSite;
import org.eclipse.ui.intro.config.IIntroAction;
import org.osgi.framework.Bundle;

import com.trolltech.qtcppproject.QtProjectCreator;

public class ShowAddressBookExampleAction implements IIntroAction {

	private static String EXAMPLESSYMBOLICNAME = "com.trolltech.qtcppintegrationhelp.examples";
	public void run(IIntroSite site, Properties params) {
		try {
			Bundle pluginBundle = Platform.getBundle(EXAMPLESSYMBOLICNAME);
			URL pluginURL = FileLocator.find(pluginBundle, new Path("AddressBook"), null);
			IPath examplePath = new Path(FileLocator.toFileURL(pluginURL).getPath());
			IProject project = null;
			int count = 1;
			while ((project == null || project.exists()) && count <= 5) {
				project = ResourcesPlugin.getWorkspace().getRoot().getProject("AddressBook"+(count==1?"":String.valueOf(count)));
				++count;
			}
			QtProjectCreator creator = new QtProjectCreator(site.getShell(), project, examplePath, true);
			creator.create(site.getWorkbenchWindow());
			IWorkbenchPage page = site.getPage();
			IWorkbenchPartReference part = page.getReference(page.getActivePart());
			if (part != null) {
				page.setPartState(part, IWorkbenchPage.STATE_RESTORED);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
