/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppdesigner.views;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.swt.*;
import com.trolltech.qtcppdesigner.views.embedded.SignalSlotEditorW;

public class SignalSlotEditorView extends ViewPart {
    private SignalSlotEditorW signalsloteditor;

    public void createPartControl(Composite parent) {
        signalsloteditor = new SignalSlotEditorW(parent, SWT.EMBEDDED);
    }

    public void setFocus() {
        signalsloteditor.setFocus();
    }
}
