/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppdesigner.views;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.ViewPart;
import com.trolltech.qtcppdesigner.views.embedded.PropertyEditorW;

import com.trolltech.qtcppdesigner.editors.UiEditor;

public class PropertyEditorView extends ViewPart implements IPartListener {
    private PropertyEditorW propertyeditor;

    public void createPartControl(Composite parent) {
        propertyeditor = new PropertyEditorW(parent, SWT.EMBEDDED);
        getSite().getPage().addPartListener(this);
    }

    public void setFocus() {
        propertyeditor.setFocus();
    }

    public void dispose() {
        getSite().getPage().removePartListener(this);
        super.dispose();
    }

    public void partActivated(IWorkbenchPart part) {
        IEditorPart editor = getSite().getPage().getActiveEditor();
        if (editor instanceof UiEditor) {
            return;
        }
        propertyeditor.clearSheet();
    }
    public void partBroughtToTop(IWorkbenchPart part) {
    }
    public void partClosed(IWorkbenchPart part) {
    }
    public void partDeactivated(IWorkbenchPart part) {
    }
    public void partOpened(IWorkbenchPart part) {
    }
}
