/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppdesigner.editors;

import java.util.ArrayList;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorActionBarContributor;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.actions.ActionFactory;

import com.trolltech.qtcppdesigner.views.embedded.FormWindowW;

public class DesignerActionBarContributor implements IEditorActionBarContributor
{
    private ArrayList actions = null;
    private ArrayList tools = null;
    private IActionBars actionbars;
    private UiEditor editor;

    public void setActiveEditor(IEditorPart targetEditor)
    {
        if (targetEditor instanceof UiEditor)
        {
            editor = ((UiEditor)targetEditor);

            if (editor.formWindow() != null)
            {
                editor.formWindow().setActiveFormWindow();
                if (actions == null)
                    setupActions();

                updateTools();
            }
        }


    }

    public UiEditor activeEditor()
    {
        return editor;
    }

    public void init(IActionBars bars, IWorkbenchPage page)
    {
        actionbars = bars;
    }

    public void dispose()
    {
    }

    private void setupActions()
    {
        actions = new ArrayList();
        IToolBarManager toolbar = actionbars.getToolBarManager();
        IMenuManager menubar = actionbars.getMenuManager();
        FormWindowW formwindow = activeEditor().formWindow();

        //the first 7 actions are hard coded because they are integrated
        DesignerAction act = new DesignerAction(this, 0, false);
        actionbars.setGlobalActionHandler(ActionFactory.CUT.getId(), act);
        actionbars.setGlobalActionHandler(IWorkbenchActionConstants.CUT_EXT, act);
        actions.add(act);

        act = new DesignerAction(this, 1, false);
        actionbars.setGlobalActionHandler(ActionFactory.COPY.getId(), act);
        actions.add(act);

        act = new DesignerAction(this, 2, false);
        actionbars.setGlobalActionHandler(ActionFactory.PASTE.getId(), act);
        actions.add(act);

        act = new DesignerAction(this, 3, false);
        actionbars.setGlobalActionHandler(ActionFactory.DELETE.getId(), act);
        actions.add(act);

        act = new DesignerAction(this, 4, false);
        actionbars.setGlobalActionHandler(ActionFactory.SELECT_ALL.getId(), act);
        actions.add(act);

        act = new DesignerAction(this, 5, false);
        actionbars.setGlobalActionHandler(ActionFactory.UNDO.getId(), act);
        actionbars.setGlobalActionHandler(IWorkbenchActionConstants.UNDO_EXT, act);
        actions.add(act);

        act = new DesignerAction(this, 6, false);
        actionbars.setGlobalActionHandler(ActionFactory.REDO.getId(), act);
        actions.add(act);

        MenuManager qtmenu = new MenuManager("QtDesigner");
        menubar.insertBefore(IWorkbenchActionConstants.M_WINDOW, (qtmenu));

        // the rest of the actions are dynamic
        for(int actId=7; actId<formwindow.staticActionCount()+1; actId++)
        {
            act = new DesignerAction(this, actId, false);
            actions.add(act);
            if (act.hasIcon())
                toolbar.add(act);
            qtmenu.add(act);
        }
        qtmenu.insert(formwindow.staticActionCount()-7, new Separator());

        MenuManager qtpreviewmenu = new MenuManager("Preview in");
        qtmenu.add(qtpreviewmenu);

        for (int actId=formwindow.staticActionCount()+1; actId<formwindow.actionCount(); actId++) {
            act = new DesignerAction(this, actId, false);
            actions.add(act);
            if (act.hasIcon())
                toolbar.add(act);
            qtpreviewmenu.add(act);
        }

        tools = new ArrayList();
        toolbar.add(new Separator());
        qtmenu.add(new Separator());

        MenuManager qteditmenu = new MenuManager("Editor Mode");
        qtmenu.add(qteditmenu);

        // the tools
        for (int toolId=0; toolId<formwindow.toolCount(); toolId++)
        {
            act = new DesignerAction(this, toolId, true);
            tools.add(act);
            if (act.hasIcon())
                toolbar.add(act);
            qteditmenu.add(act);
        }
    }

    public IActionBars actionBars()
    {
        return actionbars;
    }

    public void updateAction(int actId)
    {
        if (actions != null)
        {
            DesignerAction act = (DesignerAction)actions.get(actId);
            FormWindowW formwindow = activeEditor().formWindow();

            String name = formwindow.actionName(actId);
            if (name != act.getText())
                act.setText(name);
            act.setEnabled(formwindow.isEnabled(actId));
        }
    }

    public void updateTools()
    {
        FormWindowW formwindow = activeEditor().formWindow();

        for (int toolId=0; toolId<tools.size(); toolId++)
        {
            if(formwindow.currentTool() == toolId)
                ((DesignerAction)tools.get(toolId)).setChecked(true);
            else
                ((DesignerAction)tools.get(toolId)).setChecked(false);
        }
    }
}
