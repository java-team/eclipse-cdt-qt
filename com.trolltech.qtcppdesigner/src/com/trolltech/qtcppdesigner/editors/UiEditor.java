/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppdesigner.editors;

import java.io.File;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorActionBarContributor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;
import org.eclipse.ui.part.FileEditorInput;

import com.trolltech.qtcppcommon.editors.EditorInputWatcher;
import com.trolltech.qtcppcommon.editors.IQtEditor;
import com.trolltech.qtcppdesigner.views.embedded.FormWindowW;
import com.trolltech.qtcppdesigner.views.embedded.FormWindowWListener;

public class UiEditor extends EditorPart implements IQtEditor {
    protected FormWindowW formwindow = null;
    protected String uifile;
    protected IResource resource_handle = null;
    private EditorInputWatcher listener;

    public void doSave(IProgressMonitor monitor)
    {
        if (formwindow == null)
            return;

        formwindow.save();

        if (resource_handle != null) {
            try {
                resource_handle.touch(null);
            } catch (CoreException e) {
                throw new RuntimeException("Failed to touch resource", e);
            }
        }

        firePropertyChange(IEditorPart.PROP_DIRTY);
        listener.updateTimeStamp();
    }

    public void doSaveAs()
    {
        //### implement me later
    }

    public String getTitleToolTip()
    {
        return uifile;
    }

    public IResource resource() {
        return resource_handle;
    }

    public void init(IEditorSite site, IEditorInput input)
        throws PartInitException
    {
        setSite(site);
        setInput(input);
        FileEditorInput fin = (FileEditorInput)input;
        uifile = fin.getFile().getLocation().toOSString();
        resource_handle = (IResource)input.getAdapter(IResource.class);

        setContentDescription("Qt Designer Editor");
        setPartName(input.getName());

        listener = new EditorInputWatcher(this);
    }

    public void createPartControl(Composite parent)
    {
        File file = new File(uifile);
        boolean readable = file.exists() && file.canRead();
        if (!readable) {
            EditorInputWatcher.createMissingFileInfo(parent, uifile);
            return;
        }
        formwindow = new FormWindowW(parent, SWT.EMBEDDED);

        formwindow.addFormWindowWListener(new FormWindowWListener() {
            public void actionChanged(int actId)
            {
                IEditorActionBarContributor cont = UiEditor.this.getEditorSite().getActionBarContributor();
                if (cont instanceof DesignerActionBarContributor) {
                    DesignerActionBarContributor dcont = (DesignerActionBarContributor)cont;
                    dcont.updateAction(actId);
                }
            }

            public void checkActiveWindow()
            {
                try {
                    UiEditor.this.getEditorSite().getPage().activate(UiEditor.this);
                } catch (Exception e) {

                }
            }

            public void resourceFilesChanged()
            {
                try {
                    IEditorInput ein = UiEditor.this.getEditorInput();
                    if (ein instanceof FileEditorInput) {
                        FileEditorInput fin = (FileEditorInput)ein;
                        if (fin.getFile().getProject() != null)
                            fin.getFile().getProject().refreshLocal(IResource.DEPTH_INFINITE, null);
                    }
                } catch(Exception e) {
                    //### do nothing...
                }
            }

            public void updateDirtyFlag() {
                UiEditor.this.firePropertyChange(IEditorPart.PROP_DIRTY);
            }
        });

        formwindow.initialize();
        formwindow.open(uifile);
    }

    public void setFocus()
    {
        if (formwindow == null)
            return;

        formwindow.setFocus();
    }

    public boolean isDirty()
    {
        if (formwindow == null)
            return false;

        return formwindow.isDirty();
    }

    public boolean isSaveAsAllowed()
    {
        return false;
    }

    protected void closeFormWindow() {
        // Overridden in subclasses
    }

    public void dispose()
    {
        listener.dispose();
        closeFormWindow();
        super.dispose();
    }

    public FormWindowW formWindow()
    {
        return formwindow;
    }

    public void reload() {
        formwindow.open(uifile);
    }
}
