/*********************************************************************************
**
** This file is part of Qt Eclipse Integration
**
** Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** Windows(R) users may use this file under the terms of the Qt Eclipse
** Plug In License Agreement Version 1.0 as attached in the LICENSE.TXT file.
**
** Linux(R) users may use this file under the terms of the GNU Lesser
** General Public License Agreement version 2.1 as shown in the LGPL-2_1.TXT file.
**
**********************************************************************************/

package com.trolltech.qtcppdesigner;

import java.io.IOException;
import java.net.URL;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.util.ILogger;
import org.eclipse.jface.util.Policy;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

import com.trolltech.qtcpp.QtPlugin;
import com.trolltech.qtcppdesigner.views.embedded.DesignerBridgeW;

/**
 * The main plugin class to be used in the desktop.
 */
public class QtDesignerPlugin extends AbstractUIPlugin {
    //The shared instance.
    private static QtDesignerPlugin plugin;
    //Resource bundle.
    private ResourceBundle resourceBundle;
    private DesignerBridgeW designer;
    private static final String BUNDLESYMBOLICNAME = "com.trolltech.qtcppdesigner";
    private static final String PLUGINSSYMBOLICNAME = "com.trolltech.qtcppdesignerplugins" + "." + Platform.getOS() + "." + Platform.getOSArch();

    /**
     * The constructor.
     */
    public QtDesignerPlugin() {
        super();
        plugin = this;
        try {
            resourceBundle = ResourceBundle.getBundle("com.trolltech.qtcppdesigner.QtDesignerPluginResources");
        } catch (MissingResourceException x) {
            resourceBundle = null;
        }

    }

    /**
     * This method is called upon plug-in activation
     */
    public void start(BundleContext context) throws Exception {
        super.start(context);

        // Make sure that this code gets called from the GUI thread
        // Otherwise may break on Linux
        Display.getDefault().asyncExec(new Runnable() {
            public void run() {
                Shell shell = new Shell();
                designer = new DesignerBridgeW(shell, SWT.EMBEDDED);
                loadPlugins();
            }
        });
    }

    /**
     * This method is called when the plug-in is stopped
     */
    public void stop(BundleContext context) throws Exception {
        super.stop(context);
    }

    /**
     * Returns the shared instance.
     */
    public static QtDesignerPlugin getDefault() {
        return plugin;
    }

    /**
     * Returns the string from the plugin's resource bundle,
     * or 'key' if not found.
     */
    public static String getResourceString(String key) {
        ResourceBundle bundle = QtDesignerPlugin.getDefault().getResourceBundle();
        try {
            return (bundle != null) ? bundle.getString(key) : key;
        } catch (MissingResourceException e) {
            return key;
        }
    }

    /**
     * Returns the plugin's resource bundle,
     */
    public ResourceBundle getResourceBundle() {
        return resourceBundle;
    }

    protected static String getPluginsPathByBundle() throws IOException
    {
        Bundle pluginBundle = Platform.getBundle(PLUGINSSYMBOLICNAME);
        URL pluginURL = FileLocator.find(pluginBundle, new Path(""), null);
        IPath pluginIPath = new Path(FileLocator.toFileURL(pluginURL).getPath());
        return pluginIPath.toOSString();
    }

    protected static String getPluginsPathByInstallLocation()
    {
        IPath installLocationPath = new Path(Platform.getInstallLocation().getURL().getPath());
        installLocationPath = installLocationPath.append("plugins");
        installLocationPath = installLocationPath.append(PLUGINSSYMBOLICNAME);
        String pluginPath = installLocationPath.toOSString();
        return pluginPath;
    }

    protected static String getErrorStringWithoutShippedDlls(String error)
    {
        String result = error;
        if (Platform.getOS().equals(Platform.OS_WIN32)) {
            // We ship some Qt runtimes with the designer plugins.
            // These should not be reported as invalid plugins.
            result = "";
            String[] errors = error.split("\n");
            String[] dllsToIgnore = {"Qt3Support", "QtNetwork", "QtSql"};
            for (int i = 0; i < errors.length; i++) {
                boolean dllsToIgnoreInError = false;

                for (int j = 0; j < dllsToIgnore.length; j++) {
                    if (errors[i].indexOf(dllsToIgnore[j]) > -1) {
                        dllsToIgnoreInError = true;
                        break;
                    }
                }

                if (!dllsToIgnoreInError) {
                    if (result.length() > 0)
                        result += '\n';
                    result += errors[i];
                }
            }
        }
        return result;
    }

    public void loadPlugins()
    {
        String pluginPath = null;

        try {
            try {
                pluginPath = getPluginsPathByBundle();
            } catch (IOException e1) {
                pluginPath = getPluginsPathByInstallLocation();
            }
            designer.setPluginPath(pluginPath);
            String failure = getErrorStringWithoutShippedDlls(designer.pluginFailureString());

            if (failure.length() != 0) {
                ILogger logger = Policy.getLog();
                logger.log(new Status(
                             Status.WARNING,
                             BUNDLESYMBOLICNAME,
                             Status.WARNING,
                             "Some QtDesigner plugins could not be loaded.",
                             new Exception(failure)));
            }
        } catch (Exception e2) {
            // Ignore
        }
    }

}
