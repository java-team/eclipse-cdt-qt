#!/bin/bash

cd `dirname $0`
export ECLIPSEARCH=x86

echo Checking environment...
which ant > /dev/null || { echo Couldn\'t find \'ant\' in the PATH ; exit ; }
test -x ${QTDIR}/bin/qmake || { echo Couldn\'t find \$\{QTDIR\}/bin/qmake - wrong QTDIR variable? ; exit ; }
${QTDIR}/bin/qmake -version | grep -F 4.6.1 > /dev/null || { echo Need Qt version 4.6.1 to build this package - wrong QTDIR variable? ; exit ; }
grep glib ${QTDIR}/lib/pkgconfig/QtGui*.pc > /dev/null || { echo Qt seems to be configured without glib support, but glib is needed for the Eclipse Integration ; exit ; }
test -r ${JAVADIR}/include/jni.h || { echo Couldn\'t find \$\{JAVADIR\}/include/jni.h - wrong JAVADIR variable? ; exit ; }
test -r ${JAVADIR}/lib/tools.jar || { echo Couldn\'t find \$\{JAVADIR\}/lib/tools.jar - wrong JAVADIR variable? ; exit ; }
ls -1 ${ECLIPSEDIR}/plugins/ |grep "org[.]eclipse[.]cdt.*[.]jar" > /dev/null || { echo Couldn\'t find the CDT jar files in \$\{ECLIPSEDIR\}/plugins/ - wrong ECLIPSEDIR variable or CDT missing? ; exit ; }

echo Building native components...
cd qswt/
${QTDIR}/bin/qmake -r > /dev/null || exit
make > /dev/null || exit

echo Building plugins ...
cd ..
export JAVA_HOME=$JAVADIR
ant -q -f com.trolltech.qtcpp/build.xml
ant -q -f com.trolltech.qtcppdesigner.linux.x86/build.xml
ant -q -f com.trolltech.qtcppdesigner/build.xml
ant -q -f com.trolltech.qtcppdesigner.qtcpp/build.xml
ant -q -f com.trolltech.qtcppproject.linux.x86/build.xml
ant -q -f com.trolltech.qtcppproject/build.xml
ant -q -f com.trolltech.qtcppintegrationhelp/build.xml

echo Collecting files to `pwd`/eclipse/ ...
mkdir -p eclipse/plugins
mkdir -p eclipse/features
cp com.trolltech.qtcpp/*.jar eclipse/plugins || exit
cp com.trolltech.qtcppdesigner/*.jar eclipse/plugins || exit
unzip -q -o com.trolltech.qtcppdesigner.linux.x86/*.zip -d eclipse/plugins || exit
cp -r com.trolltech.qtcppdesignerplugins.linux.x86 eclipse/plugins || exit
cp com.trolltech.qtcppdesigner.qtcpp/*.jar eclipse/plugins || exit
cp -r com.trolltech.qtcpp.feature eclipse/features/com.trolltech.qtcpp.feature_1.6.1 || exit
cp com.trolltech.qtcppintegrationhelp/*.jar eclipse/plugins || exit
cp -r com.trolltech.qtcppintegrationhelp.examples eclipse/plugins/com.trolltech.qtcppintegrationhelp.examples_1.6.1 || exit
cp -r com.trolltech.qtcpp.linux.x86 eclipse/plugins/com.trolltech.qtcpp.linux.x86_4.6.1 || exit
cp com.trolltech.qtcppproject/*.jar eclipse/plugins || exit
unzip -q -o com.trolltech.qtcppproject.linux.x86/*.zip -d eclipse/plugins || exit

mkdir -p eclipse/plugins/com.trolltech.qtcpp.linux.x86_4.6.1/lib || exit
ln -sf $QTDIR/lib/*.so* eclipse/plugins/com.trolltech.qtcpp.linux.x86_4.6.1/lib || exit

